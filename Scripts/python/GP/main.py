import cvxpy as cp

# X = 5
# Y = 5
# R = lambda a, b:   [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
#                     [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
#                     [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
#                     [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
#                     [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
#                     [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
#                     [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
#                     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#                     [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
#                     [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
# dist = [2,2,2,2,2,3,3,3,3,3]
X = 1
Y = 4
R = lambda a, b: 1.0 if a == b else -1.0
dist = [2,2]

def get_ind(p, c):
    return numb_crops * p + c

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < X and n[1] < Y:
                neighbors.append(n)
    return neighbors


numb_crops = len(dist)
pixels = [(i, j) for i in range(X) for j in range(Y)]

def main():

    x = cp.Variable(shape=(X * Y * numb_crops,), pos=True, name="x")

    constraints = []

    # constrain crop amounts in a pixel
    for p in range(X * Y):
        constr = 0
        for c in range(numb_crops):
            constr += x[p * numb_crops + c]
            constraints.append(x[p * numb_crops + c] <= 1)
        constraints.append(constr <= 1)
    
    # constrain global crop amounts
    for c in range(numb_crops):
        constr = 0
        for p in range(X * Y):
            constr += x[p * numb_crops + c]
        constraints.append(constr <= dist[c])
    
    # objective function
    obj = 0
    for i, pA in enumerate(pixels):
        neighbors = get_neighbors(pA)
        for pB in neighbors:
            j = neighbors.index(pB)
            for cA in range(numb_crops):
                for cB in range(numb_crops):
                    obj += x[get_ind(i, cA)] * x[get_ind(j, cB)] * (R(cA, cB) + 2)
    
    
    problem = cp.Problem(cp.Minimize(obj), constraints)
    assert not problem.is_dcp()
    assert problem.is_dgp()
    problem.solve(solver=cp.MOSEK, gp=True)
    print("Optimal value:", problem.value)
    print(x, ":", x.value)

    

main()