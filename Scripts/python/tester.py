from bruteForce import solver
from greedy import solver_greedy
from scorer import score, maxScore, maxScoreAlt, maxScoreGaertner
# from grower import growA, growA_alt, growB, fix
# from simulatedAnealing import simulatedAnnealing, simulatedAnnealingMultiple, simulatedAnnealingField
# import numpy as np
# import copy
# import random
# from benchmark import genProblemRandom, genProblemStriped

# Cotrini Data Ideal Sol: [[5, 0, 1, 4, 2], [6, 3, 8, 9, 7]], score: 32, 2 * 5
# Agropol Data Ideal Sol: [[1, 8, 6, 0, 7], [3, 9, 2, 5, 4]], score: 14.5, 2 * 5

def print_field(field):
    for row in field:
        for p in row:
            print(p, end="\t")
        print("")

def main():

    # Cotrini Data (Sum = 26)
    x = 15
    y = 15
    R = lambda a, b: [
        [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
        [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
        [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
        [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
        [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
        [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
        [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
        [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
        [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
        [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
    dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

    # Agropol Data (Sum = 8)
    # x = 15
    # y = 15
    # R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    #     [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    #     [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    #     [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    #     [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    #     [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #     [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    #     [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
    # dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

    sol = [
        [5, 9, 5, 4, 9, 4, 5, 9, 9, 4, 4, 4, 9, 4, 2],
        [9, 1, 8, 6, 8, 4, 0, 3, 8, 7, 2, 1, 1, 2, 9],
        [4, 3, 8, 0, 6, 3, 8, 7, 7, 2, 8, 0, 1, 1, 9],
        [4, 5, 0, 5, 5, 2, 6, 2, 0, 8, 0, 5, 1, 3, 5],
        [9, 0, 7, 0, 7, 8, 3, 1, 8, 7, 1, 8, 7, 6, 4],
        [9, 6, 0, 6, 3, 7, 0, 0, 3, 3, 5, 7, 6, 2, 9],
        [4, 6, 5, 1, 0, 7, 0, 7, 8, 1, 0, 3, 2, 7, 4],
        [9, 0, 2, 8, 6, 8, 1, 8, 3, 5, 6, 3, 0, 8, 5],
        [4, 2, 1, 6, 0, 3, 0, 7, 1, 7, 2, 6, 3, 8, 9],
        [4, 0, 7, 1, 2, 0, 1, 5, 2, 8, 6, 6, 2, 7, 5],
        [9, 3, 3, 5, 6, 1, 7, 3, 2, 6, 7, 1, 3, 8, 4],
        [5, 1, 6, 1, 0, 5, 2, 7, 8, 5, 0, 1, 7, 1, 9],
        [4, 8, 5, 2, 8, 6, 2, 2, 5, 6, 8, 3, 2, 7, 9],
        [4, 3, 6, 3, 1, 6, 1, 2, 3, 7, 3, 3, 6, 0, 4],
        [2, 4, 5, 9, 5, 9, 4, 4, 9, 4, 9, 9, 4, 9, 2]
    ]

    print("LP Bound Score:", score(R, sol))
    # R = genProblemRandom(x, y, dist, 0.4)
    # R = genProblemStriped(x, y, dist)

    # print("Maxscore Gärtner Agropol Data:")
    # print(maxScoreGaertner(R, field))
    # print("Maxscore Traditional Agropol Data:")
    # print(maxScore(R, field))

    sol = solver_greedy(x, y, R, dist)
    print("Gärtner MaxScore: ", maxScoreGaertner(R, sol))

    sol = simulatedAnnealing(R, sol, 2, 0.001, 0.999)
    print_field(sol)
    print("Score Greedy: ", score(R, sol) / maxScoreGaertner(R, sol))


if __name__ == "__main__":
    main()
