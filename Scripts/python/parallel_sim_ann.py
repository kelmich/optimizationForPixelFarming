import multiprocessing as mp
import random
import copy
from simulatedAnealing import simulatedAnnealing
from scorer import score, maxScore, maxScoreAlt, maxScoreGaertner

def R(a, b):
    return [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
        [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
        [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
        [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
        [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
        [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
        [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]


def main():

    # initialize a problem
    # Agropol Data
    x = 15
    y = 15
    dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

    # generate random 1d field
    fieldStretched = []
    for i, d in enumerate(dist):
        fieldStretched += [i] * d
    random.shuffle(fieldStretched)

    # generate 2d field from 1d field
    field = []
    for i in range(x):
        field.append([])
        for j in range(y):
            field[i].append(fieldStretched[i * x + j])

    # initialize pool
    pool = mp.Pool(mp.cpu_count())

    # do the parallel work
    for rnd in range(20):

        # progress
        print("Round ", rnd, ", Score: ", score(R, field) / maxScore(R, field))

        # have the threads work on their own
        results = [pool.apply(simulatedAnnealing, args=(R, field, 16, 0.0001, 0.999)) for _ in range(mp.cpu_count())]

        # find the best thread and set all to it's value
        new_field = copy.deepcopy(field)

        for f in results:
            if score(R, new_field) < score(R, f):
                new_field = copy.deepcopy(f)
        field = copy.deepcopy(new_field)

    print(field)

    # clean up
    pool.close()

main()