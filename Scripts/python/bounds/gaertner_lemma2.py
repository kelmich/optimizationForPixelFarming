from itertools import count
import gurobipy as gp
from gurobipy import GRB
import itertools

# specify the problem
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

#
# HELPERS
#

# i is x-coord, j is y coord, c is the crop
def get_ind(i, j, c):
    if i < 0 or j < 0:
        return -1
    ind = int((i * y + j) * len(dist) + c)
    if ind >= x * y * numb_crops:
        return -1
    return ind

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors

dp = {}
def get_rel_ind(c1, c2):
    return dp.get(str(c1) + "|" + str(c2))

#
# END HELPERS
#

# Create a new model
m = gp.Model("lp")

# Create variables
obj = 0
rel_vars = []
for cA in range(numb_crops):
    for cB in range(numb_crops):
        rel_vars.append(m.addVar(lb=0.0, name="crop_pair_{}_{}".format(cA, cB)))
        obj += -R(cA, cB) * rel_vars[-1]
        dp[str(cA) + "|" + str(cB)] = len(rel_vars) - 1
m.setObjective(obj)


# constrain relationship variables
for c in range(numb_crops):
    constr = 0
    counter = 0
    for cA in range(numb_crops):
        for cB in range(numb_crops):
            if cA == c:
                constr += rel_vars[counter]
            counter += 1
                
    m.addConstr(constr <= 8 * dist[c], "crop_rel_{}".format(c))         

# lemma 2 constraint
for i in range(len(dist)+1):
    for K in itertools.combinations([a for a in range(len(dist))], i):
        sum_left = 0
        sum_right = 0
        for cA in K:
            for cB in K:
                if cB >= cA:
                    continue
                sum_left += rel_vars[get_rel_ind(cA, cB)]
        for c in K:
            sum_right += dist[c]
        m.addConstr(sum_left <= 4 * sum_right, "lemma2_{}".format(i))

# constrain that x_{ij} == x_{ji}
for cA in range(numb_crops):
    for cB in range(cA + 1):
        m.addConstr(rel_vars[get_rel_ind(cA, cB)] == rel_vars[get_rel_ind(cB, cA)], "rel_eq_{}_{}".format(cA, cB))



# constrain total relationship sum
total_rel_sum = 0
for v in rel_vars:
    total_rel_sum += v
m.addConstr(total_rel_sum == (8 * x * y - 6 * x - 6 * y  + 4), "total_rel_sum")

m.optimize()

s = 0
for v in m.getVars():
    print('%s %g' % (v.VarName, v.X), end="")
    cA = int(v.VarName[-3])
    cB = int(v.VarName[-1])
    print("\t\t Relationship:" + str((R(cA, cB) + R(cB, cA)) /2))

    s += v.X
print(s)

print("The score is bounded by:", obj.getValue() * -1)