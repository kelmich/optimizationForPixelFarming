from itertools import count
import gurobipy as gp
from gurobipy import GRB

# specify the problem
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

#
# HELPERS
#

# i is x-coord, j is y coord, c is the crop
def get_ind(i, j, c):
    if i < 0 or j < 0:
        return -1
    ind = int((i * y + j) * len(dist) + c)
    if ind >= x * y * numb_crops:
        return -1
    return ind

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors

dp = {}
def get_rel_ind(c1, c2):
    return dp.get(str(c1) + "|" + str(c2))

#
# END HELPERS
#

# Create a new model
m = gp.Model("lp")

# Create variables
obj = 0
pixel_vars = []
rel_vars = []
for i in range(x):
    for j in range(y):
        for c in range(numb_crops):
            pixel_vars.append(m.addVar(lb=0.0, ub=1.0, name="pixel_crop_var_{}".format(get_ind(i, j, c))))
for cA in range(numb_crops):
    for cB in range(numb_crops):
        rel_vars.append(m.addVar(lb=0.0, name="crop_pair_{}_{}".format(cA, cB)))
        obj += -R(cA, cB) * rel_vars[-1]
        dp[str(cA) + "|" + str(cB)] = len(rel_vars) - 1
m.setObjective(obj)

# constrain the pixel crop variables
for i in range(x):
    for j in range(y):
        pixel_crop_sum = 0
        for c in range(numb_crops):
            pixel_crop_sum += pixel_vars[get_ind(i, j, c)]
        m.addConstr(pixel_crop_sum == 1, "pixel_crop_sum_{}_{}".format(i, j))
for c in range(numb_crops):
    crop_sum = 0
    for i in range(x):
        for j in range(y):
            crop_sum += pixel_vars[get_ind(i, j, c)]
    m.addConstr(crop_sum == dist[c], "crop_sum_{}".format(c))   


# constrain relationship variables
for c in range(numb_crops):
    constrL = 0
    constrR = 0
    counter = 0
    for cA in range(numb_crops):
        for cB in range(numb_crops):
            if cA == c:
                if cB == c:
                    constrL += rel_vars[counter]
                constrL += rel_vars[counter]
            counter += 1
    for p in pixels:
        constrR += pixel_vars[get_ind(p[0], p[1], c)] * len(get_neighbors(p))
    m.addConstr(constrL == constrR, "crop_rel_{}".format(c))            

# constrain that x_{ij} == x_{ji}
for cA in range(numb_crops):
    for cB in range(cA + 1):
        m.addConstr(rel_vars[get_rel_ind(cA, cB)] == rel_vars[get_rel_ind(cB, cA)], "rel_eq_{}_{}".format(cA, cB))



# constrain total relationship sum
total_rel_sum = 0
for v in rel_vars:
    total_rel_sum += v
m.addConstr(total_rel_sum == (8 * x * y - 6 * x - 6 * y  + 4), "total_rel_sum")

m.optimize()

sol = m.getVars()

for i in range(x):
    for j in range(y):
        for c in range(numb_crops):
            if sol[get_ind(i, j, c)].x > 0.5:
                print(c, end=" ")
    print("")

for v in m.getVars():
    print(v.X, end=" ")

print("The score is bounded by:", obj.getValue() * -1)