from itertools import count
import gurobipy as gp
from gurobipy import GRB

# Toy Example
# x = 1
# y = 10
# R = lambda a, b: 1 if abs(a - b) <= 1 else 0
# dist = [1,1,1,1,1,1,1,1,1,1]

# larger example
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

# helpers
dp = {}
def get_ind(pA, pB, c1, c2):
    return dp.get(str(pA) + "|" + str(pB) + "|" + str(c1) + "|" + str(c2))

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors

# Create a new model
m = gp.Model("lp")

# Create variables and objective funtion
obj = 0
rel_vars = []
ind = 0
for p in pixels:
    neighbors = get_neighbors(p)
    for n in neighbors:
        for c1 in range(numb_crops):
            for c2 in range(numb_crops):
                if get_ind(p, n, c1, c2) is None:
                    rel_vars.append(m.addVar(lb=0.0, ub=1.0, name="pixel_crop_pixel_crop_var_{}".format(ind)))
                    obj += -(R(c1, c2) + R(c2, c1)) * rel_vars[-1] # the solver minimizes

                    # speeds up index queries
                    dp[str(p) + "|" + str(n) + "|" + str(c1) + "|" + str(c2)] = ind
                    dp[str(n) + "|" + str(p) + "|" + str(c2) + "|" + str(c1)] = ind
                    ind += 1
m.setObjective(obj)

# constrain that all neighborhood relationships sum to 1
for p in pixels:
    neighbors = get_neighbors(p)
    for n in neighbors:
        constr = 0
        for c1 in range(numb_crops):
            for c2 in range(c1 + 1):
                constr += rel_vars[get_ind(p, n, c1, c2)]
                constr += rel_vars[get_ind(p, n, c2, c1)]
        m.addConstr(constr == 1, "neigh_is_1_{}_{}".format(p, n))

# constrain that all neighbors imply the same pixel
for p in pixels:
    neighbors = get_neighbors(p)
    n0 = neighbors.pop()
    for n in neighbors:
        for c1 in range(numb_crops):
            constr = 0
            for c2 in range(c1 + 1):
                constr -= rel_vars[get_ind(p, n, c1, c2)]
                constr -= rel_vars[get_ind(p, n, c2, c1)]
                constr += rel_vars[get_ind(p, n0, c1, c2)]
                constr += rel_vars[get_ind(p, n0, c2, c1)]
            m.addConstr(constr == 0, "same_pixel_{}_{}_{}".format(p, n0, n))

# constrain the global crop distribution (Version A)
for cTest in range(numb_crops):
    constr = 0
    for p in pixels:
        n0 = get_neighbors(p).pop()
        for c2 in range(numb_crops):
            constr += rel_vars[get_ind(p, n0, cTest, c2)]
    m.addConstr(constr == dist[cTest], "global_cps_{}".format(cTest))

# constrain the global crop distribution (Version B)
# for cTest in range(numb_crops):
#     constr = 0
#     for p in pixels:
#         neighbors = get_neighbors(p)
#         for n in neighbors:
#             for c2 in range(numb_crops):
#                 constr += rel_vars[get_ind(p, n, cTest, c2)]
#     m.addConstr(constr <= 8 * dist[cTest], "global_cps_{}".format(cTest))


m.optimize()

# for v in m.getVars():
#     print('%s %g' % (v.VarName, v.X))

print("The score is bounded by:", obj.getValue() * -1)