# Import packages.
import cvxpy as cp
import numpy as np

# specify the problem
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]
n = numb_crops + len(pixels) * numb_crops


C = np.zeros([n, n])
for i in range(numb_crops):
    for j in range(numb_crops):
        if i == j:
            continue
        C[i, j] = R(i, j)
A_leq = []
b_leq = []
A_geq = []
b_geq = []
A_eq = []
b_eq = []

for i in range(numb_crops):
    for j in range(numb_crops):
        A_geq.append(np.zeros([n, n]))
        A_geq[-1][i][j] = 1
        b_geq.append(0)

for i in range(numb_crops):
    A_leq.append(np.zeros([n, n]))
    A_geq.append(np.zeros([n, n]))
    A_leq[-1][i][i] = 1.0
    A_geq[-1][i][i] = 1.0
    b_leq.append(8 * dist[i])
    b_geq.append(3 * dist[i])
for i in range(numb_crops):
    A_eq.append(np.zeros([n, n]))
    for j in range(numb_crops):
        A_eq[-1][i][j] = -1.0 if i == j else 1.0
    b_eq.append(0)
A_eq.append(np.zeros([n, n]))
for i in range(numb_crops):
    for j in range(i):
        if i == j:
            continue
        else:
            A_eq[-1][i][j] = 1.0
b_eq.append(1624 / 2)
    


# Define and solve the CVXPY problem.
# Create a symmetric matrix variable.
X = cp.Variable((numb_crops,numb_crops), symmetric=True)
constraints = [
    cp.trace(A_leq[i] @ X) <= b_leq[i] for i in range(len(A_leq))
]
constraints += [
    cp.trace(A_geq[i] @ X) >= b_geq[i] for i in range(len(A_geq))
]
constraints += [
    cp.trace(A_eq[i] @ X) == b_eq[i] for i in range(len(A_eq))
]
prob = cp.Problem(cp.Maximize(cp.trace(C @ X)),
                  constraints)
prob.solve()

# Print result.
print("The optimal value is", prob.value)
print("A solution X is")
solution = [ round(v, 1) for tmp in X.value for v in tmp ]
for i in range(numb_crops):
    for j in range(numb_crops):
        print(solution[i * numb_crops + j], end='\t')
    print()
print(sum(solution))


'''
from itertools import count
from textwrap import indent
import gurobipy as gp
from gurobipy import GRB

# specify the problem
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

#
# HELPERS
#
def get_pix_ind(p, c):
    return pixels.index(p) * numb_crops + c

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors

dp = {}
def get_rel_ind(pA, pB, c):
    return dp.get(str(pA) + "|" + str(pB) + "|" + str(c))

#
# END HELPERS
#

# Create a new model
m = gp.Model("qcp")
m.params.NonConvex = 2

# Create variables
pix_vars = []
rel_vars = []
for c in range(numb_crops):
    for pA in pixels:
        pix_vars.append(m.addVar(lb=0.0, ub=1.0, name="pixel_{}_crop_{}".format(pA, c)))
        neighbors = get_neighbors(pA)
        for pB in neighbors:
            if pixels.index(pA) > pixels.index(pB):
                continue
            rel_vars.append(m.addVar(lb=0.0, ub=1.0, name="pixel_{}_to_pixel{}_for_crop_{}".format(pA, pB, c)))
            dp[str(pA) + "|" + str(pB) + "|" + str(c)] = len(rel_vars) - 1
            dp[str(pB) + "|" + str(pA) + "|" + str(c)] = len(rel_vars) - 1


# constrain relationship variables
for c in range(numb_crops):
    crop_sum = 0
    for pA in pixels:
        crop_sum += pix_vars[get_pix_ind(pA, c)]
        neighbors = get_neighbors(pA)
        for pB in neighbors:
            m.addConstr(pix_vars[get_pix_ind(pA, c)] <= rel_vars[get_rel_ind(pA, pB, c)])
            m.addConstr(pix_vars[get_pix_ind(pB, c)] <= rel_vars[get_rel_ind(pA, pB, c)])
    m.addConstr(crop_sum == dist[c])
for p in pixels:
    pixel_sum = 0
    for c in range(numb_crops):
        pixel_sum += pix_vars[get_pix_ind(p, c)]
    m.addConstr(pixel_sum == 1)

# sdp constraint
sdp_vars = []
for cA in range(numb_crops):
    for cB in range(numb_crops):
        if cA < cB:
            continue
        sdp_vars.append(m.addVar(lb=0.0, name="sdp_{}_{}".format(cA, cB)))
        var = 0
        for pA in pixels:
            neighbors = get_neighbors(pA)
            for pB in neighbors:
                if pixels.index(pA) > pixels.index(pB):
                    continue
                var += rel_vars[get_rel_ind(pA, pB, cA)] * rel_vars[get_rel_ind(pA, pB, cB)]
        m.addConstr(sdp_vars[-1] == var)

# build the opjective function
obj = 0
for cA in range(numb_crops):
    for cB in range(numb_crops):
        obj += ((R(cA, cB) + R(cB, cA)) / 2.0) * sdp_vars[cA * numb_crops + cB]

m.setObjective(obj, GRB.MAXIMIZE)

m.optimize()

# for v in m.getVars():
#     print('%s %g' % (v.VarName, v.X))

print("The score is bounded by:", obj.getValue())
'''