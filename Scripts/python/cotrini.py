import numpy as np
from read_rel_matrix import read_relationship_matrix, julianna_cost_function, optimal_value, print_results


def simple_transform(cost_function, relationship_matrix, field, cost_field, temp, propose_fun):
    new_field = propose_fun(field)
    cost_candidate = cost_function(new_field, relationship_matrix)
    if cost_candidate < cost_field:
        return new_field, cost_candidate
    elif np.random.rand() > np.exp(- (cost_candidate - cost_field) / temp):
        return new_field, cost_candidate
    else:
        return field, cost_field


def propose_by_swapping_cells(field):
    new_field = np.copy(field)
    num_rows = new_field.shape[0]
    num_cols = new_field.shape[1]
    i1 = np.random.randint(0, num_rows)
    i2 = np.random.randint(0, num_rows)
    j1 = np.random.randint(0, num_cols)
    j2 = np.random.randint(0, num_cols)
    new_field[i1, j1] = field[i2, j2]
    new_field[i2, j2] = field[i1, j1]
    return new_field


def propose_by_swapping_rows(field):
    new_field = np.copy(field)
    num_rows = new_field.shape[0]
    i1 = np.random.randint(0, num_rows)
    i2 = np.random.randint(0, num_rows)
    new_field[i1] = field[i2]
    new_field[i2] = field[i1]
    return new_field


def simulated_annealing(cost_function, relationship_matrix, starting_field, starting_temp, stopping_temp,
                        rate, propose_fun):
    temp = starting_temp
    field = np.copy(starting_field)
    cost = cost_function(field, relationship_matrix)
    min_field = np.copy(field)
    min_cost = cost
    while temp > stopping_temp:
        field, cost = simple_transform(cost_function, relationship_matrix, field,
                                       cost, temp, propose_fun=propose_fun)
        if cost < min_cost:
            min_cost = cost
            min_field = np.copy(field)
        temp = temp * rate
    return min_field


def create_strip_field(num_rows, num_cols):
    strip_field = np.zeros((num_rows, num_cols)).astype(int)
    for i in range(num_rows):
        for j in range(num_cols):
            strip_field[i, j] = i % number_of_crops
    return strip_field


def brute_force_strip(num_rows, num_cols):
    strip_field = np.zeros((num_rows, num_cols)).astype(int)


if __name__ == '__main__':
    # Build field

    # Read the relationship matrix
    relation_matrix = read_relationship_matrix("relationship_matrix.csv")
    number_of_crops = relation_matrix.shape[0]

    num_rows = int(input("Enter number of rows: "))
    num_cols = int(input("Enter number of columns: "))
    num_iterations = int(input("Enter number of iterations: "))

    random_field_values = np.zeros(num_iterations)
    sa_random_field_values = np.zeros(num_iterations)
    sa_strip_field_values = np.zeros(num_iterations)
    sa_ranstrip_field_values = np.zeros(num_iterations)
    last_field = None

    for i in range(num_iterations):
        print("iteration " + str(i))
        # field = (np.random.rand(num_rows, num_cols) * number_of_crops).astype(int)
        sa_strip_field = create_strip_field(num_rows, num_cols)
        random_field = (np.random.rand(num_rows, num_cols) * number_of_crops).astype(int)
        sa_random_field = (np.random.rand(num_rows, num_cols) * number_of_crops).astype(int)

        sa_strip_field = simulated_annealing(julianna_cost_function, relation_matrix,
                                             starting_field=sa_strip_field, starting_temp=10., stopping_temp=0.0001,
                                             rate=0.999, propose_fun=propose_by_swapping_rows)

        sa_random_field = simulated_annealing(julianna_cost_function, relation_matrix,
                                              starting_field=sa_random_field, starting_temp=10., stopping_temp=0.0001,
                                              rate=0.999, propose_fun=propose_by_swapping_cells)

        sa_ranstrip_field = simulated_annealing(julianna_cost_function, relation_matrix,
                                                starting_field=np.copy(sa_strip_field), starting_temp=10.,
                                                stopping_temp=0.0001,
                                                rate=0.999, propose_fun=propose_by_swapping_cells)

        random_field_values[i] = - julianna_cost_function(random_field, relation_matrix)

        sa_strip_field_values[i] = - julianna_cost_function(sa_strip_field, relation_matrix)
        sa_random_field_values[i] = - julianna_cost_function(sa_random_field, relation_matrix)
        sa_ranstrip_field_values[i] = - julianna_cost_function(sa_ranstrip_field, relation_matrix)

    optimal_val = optimal_value(num_rows, num_cols)
    print_results("random field", random_field_values * optimal_val, optimal_val)
    print_results("simulated annealing strip field", sa_strip_field_values * optimal_val, optimal_val)
    print_results("simulated annealing random field", sa_random_field_values * optimal_val, optimal_val)
    print_results("simulated annealing ran-strip field", sa_ranstrip_field_values * optimal_val, optimal_val)

    # strip_field = create_strip_field(num_rows, num_cols)
    # strip_value = - julianna_cost_function(strip_field, relation_matrix)
    # print("strip crop value: " + "{:.2f}".format(strip_value))
    # print("optimal possible value: " + str(- julianna_cost_function(strip_field, np.ones(strip_field.shape))))