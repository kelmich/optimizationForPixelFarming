import random
from scorer import score, maxScore, maxScoreAlt


def genProblemStriped(x, y, dist):

    noOfCrops = len(dist)
    path = list(range(noOfCrops))
    random.shuffle(path)

    print(path)

    if noOfCrops != y:
        exit("Crop number must match y dimension")
    else:
        r = []
        for i in range(noOfCrops):
            r.append([])
            for _ in range(noOfCrops):
                r[i].append(-1)
        for i in range(len(path)-1):
            r[i][i] = 1
            r[path[i]][path[i + 1]] = 1
            r[path[i + 1]][path[i]] = 1
        r[noOfCrops-1][noOfCrops-1] = 1

        return lambda a, b: r[a][b]


def genProblemRandom(x, y, dist, solLevel):

    # generate random 1d field
    fieldStretched = []
    for i, d in enumerate(dist):
        fieldStretched += [i] * d
    random.shuffle(fieldStretched)

    # generate 2d field from 1d field
    field = []
    for i in range(x):
        field.append([])
        for j in range(y):
            field[i].append(fieldStretched[i * x + j])

    # define an R function
    noOfCrops = len(dist)
    r = [[-1] * noOfCrops] * noOfCrops
    R = lambda a, b: r[a][b]

    while score(R, field) / maxScore(R, field) < solLevel:
        i = random.randint(0, noOfCrops-1)
        j = random.randint(0, noOfCrops-1)
        r[i][j] += (1.0 - r[i][j]) / 2.0
        R = lambda a, b: r[a][b]

    print("Benchmark Final Score: ", score(R, field) / maxScore(R, field))

    return R

