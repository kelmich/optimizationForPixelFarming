import itertools

def scorePixel(R, field, pixel):
    score = 0
    x = pixel[0]
    y = pixel[1]
    fieldX = len(field)
    fieldY = len(field[0])
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            # not a neighbor
            if n1 == 0 and n2 == 0:
                continue

            # test valid neighbor
            if not (0 <= (x + n1) and (x + n1) < fieldX):
                continue
            if not (0 <= (y + n2) and (y + n2) < fieldY):
                continue

            # update score
            score += R(field[x][y], field[x+n1][y+n2])
    return score

def score(R, field):
    fieldX = len(field)
    fieldY = len(field[0])
    score = 0
    for i in range(fieldX):
        for j in range(fieldY):
            score += scorePixel(R, field, (i, j))
    return score

def scorePeriodic(R, field):
    fieldX = len(field)
    fieldY = len(field[0])
    score = 0
    for i in range(fieldX):
        for j in range(fieldY):
            for n1 in range(-1, 2):
                for n2 in range(-1, 2):
                    # not a neighbor
                    if n1 == 0 and n2 == 0:
                        continue

                    # update score
                    score += R(field[i][j], field[(i+n1+fieldX) % fieldX][(j+n2+fieldY) % fieldY])
    return score

def maxScore(R, sol):
    return score(lambda a, b: 1, sol)

def maxScoreAlt(R, sol):
    crops = set(sum(sol, []))
    score = 0
    for i in range(len(sol)):
        for j in range(len(sol[0])):
            maxNeighbor = -2
            for n in crops:
                if maxNeighbor < R(sol[i][j], n):
                    maxNeighbor = R(sol[i][j], n)
            score += 8 * maxNeighbor
    return score

def maxScoreGaertner(R, sol):
    crops = sorted(list(set(sum(sol, []))))
    flatfield = [item for sublist in sol for item in sublist]
    dist = [len(list(filter(lambda p: p == crop, flatfield))) for crop in crops]
    
    score = 0
    maxCropScores = [-100 for _ in dist]

    iteration = 0

    for n in itertools.product(crops, repeat=9):
        # print(string)
        tmpScore = 0.0

        iteration += 1

        # red edges
        for i in range(3):
            for j in range(2):
                # should this really be 6?
                tmpScore += R(n[i * 3 + j], n[i * 3 + j+1]) / 6
                tmpScore += R(n[i * 3 + j+1], n[i * 3 + j]) / 6

                tmpScore += R(n[j * 3 + i], n[(j+1) * 3 + i]) / 6
                tmpScore += R(n[(j+1) * 3 + i], n[j * 3 + i]) / 6
            
        # blue edges
        for i in range(2):
            for j in range(2):
                tmpScore += R(n[i * 3 + j], n[(i+1) * 3 + j+1]) / 4
                tmpScore += R(n[(i+1) * 3 + j+1], n[i * 3 + j]) / 4

                tmpScore += R(n[i * 3 + j+1], n[(i+1) * 3 + j]) / 4
                tmpScore += R(n[(i+1) * 3 + j], n[i * 3 + j+1]) / 4

        if tmpScore > maxCropScores[n[4]]:
            maxCropScores[n[4]] = tmpScore

        if iteration % 1000000 == 0:
            print(iteration)
    
    for crop in crops:
        score += maxCropScores[crop] * dist[crop]

    return score