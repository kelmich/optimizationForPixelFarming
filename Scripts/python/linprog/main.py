import random
import itertools
from scipy.optimize import linprog

def generate_problem():
    random_list = []
    for i in range(3):
        mini_list = []
        for j in range(3):
            n = random.randint(1,1000)
            mini_list.append(n)
        random_list.append(mini_list)
    return random_list

def lp_solve(problem):

    # problem specification
    obj = problem[0] + problem[1] + problem[2]
    for i in range(9):
        obj[i] *= -1

    lhs_eq = [
        [1,1,1,0,0,0,0,0,0],
        [0,0,0,1,1,1,0,0,0],
        [0,0,0,0,0,0,1,1,1],
        [1,0,0,1,0,0,1,0,0],
        [0,1,0,0,1,0,0,1,0],
        [0,0,1,0,0,1,0,0,1]
    ]
    rhs_eq = [1,1,1,1,1,1]

    bnd = [(0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1)]

    opt = linprog(c=obj, A_eq=lhs_eq, b_eq=rhs_eq, bounds=bnd, method="revised simplex")
    return opt

def brute_force(problem):
    max_score = 0
    for s_prime in itertools.permutations([0,1,2]):
        score = problem[0][s_prime[0]]
        score += problem[1][s_prime[1]]
        score += problem[2][s_prime[2]]
        if score > max_score:
            max_score = score
    return max_score



def main():
    for _ in range(10_000):
        problem = generate_problem()
        if float(brute_force(problem)) != float(-1 * lp_solve(problem).fun):
            print(problem)
            print(lp_solve(problem).x)
        print(float(brute_force(problem)), float(-1 * lp_solve(problem).fun))

main()