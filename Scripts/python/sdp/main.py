import cvxpy as cp
import numpy as np

# problem definition
x = 1
y = 3
R = lambda a, b: 1 if abs(a - b) <= 1 else 0
dist = [1,1,1]

# autocalculated variables
numb_crops = len(dist)
n = x * y * numb_crops
pixels = [(i, j) for i in range(x) for j in range(y)]


# Helper functions
def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors

def get_ind(p, c):
    return (p[0] * y + p[1]) * numb_crops + c

# the important stuff
def main():
    
    #
    # generate the SDP
    #
    A = []
    b = []
    C = np.zeros((n, n))

    # build the objective function
    for pA in pixels:
        for pB in pixels:
            for cA in range(numb_crops):
                for cB in range(numb_crops):
                    if pB in get_neighbors(pA):
                        C[get_ind(pA, cA)][get_ind(pB, cB)] = -R(cA, cB) # solver minimizes

    # force the matrix to be symmetric



    # all neighborhoods imply the same pixel
    for pA in pixels:
        neighbors = get_neighbors(pA)
        n0 = neighbors.pop()
        for pB in neighbors:
            for cA in range(numb_crops):
                constr = np.zeros((n, n))
                for cB in range(numb_crops):
                    constr[get_ind(pA, cA)][get_ind(pB, cB)] = 1
                    constr[get_ind(pA, cA)][get_ind(n0, cB)] = -1
                A.append(constr)
                b.append(0)
    
    # pixels contain a sum of 1 crop
    # for pA in pixels:
    #     constrA = np.zeros((n, n))
    #     constrB = np.zeros((n, n))
    #     for i in range(n):
    #         for j in range(n):
    #             constrA[i][j] = 0
    #             constrB[i][j] = 0
    #     neighbors = get_neighbors(pA)
    #     for pB in pixels:
    #         for cA in range(numb_crops):
    #             for cB in range(numb_crops):
    #                 if pB in neighbors:
    #                     constrA[get_ind(pA, cA)][get_ind(pB, cB)] = 1
    #                 else:
    #                     constrB[get_ind(pA, cA)][get_ind(pB, cB)] = 1
    #     A.append(constrA)
    #     A.append(constrB)
    #     b.append(1)
    #     b.append(0)

    # constrain total connection count
    A.append(np.ones((n, n)))
    b.append(4)



    #
    # Build the solver and run it
    #
    xTx = cp.Variable((n, n), symmetric=True)
    constraints = [0 <= xTx, xTx <= 1]
    constraints += [xTx >> 0]
    constraints += [
        cp.trace(A[i] @ xTx) == b[i] for i in range(len(A))
    ]
    prob = cp.Problem(cp.Minimize(cp.trace(C @ xTx)),
                  constraints)
    prob.solve()
    print("status:", prob.status)
    print("The optimal value is", abs(prob.value))
    print("A solution X is")
    # print(xTx.value)
    for v in xTx.value:
        print(list(map(lambda x: round(x, 5), list(v))))
    
    # find the solution
    largest_entry = xTx.value[0]
    for v in xTx.value:
        if v[0] > largest_entry[0]:
            largest_entry = v
    sol = []
    for v in largest_entry:
        sol.append(v / largest_entry[0])

    # print the solution
    print("\n", largest_entry, "\n")
    for p in pixels:
        for c in range(numb_crops):
            print(round(sol[get_ind(p, c)], 5), end=", ")
        print("")
    

main()