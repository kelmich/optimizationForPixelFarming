import copy

def solver_greedy(x, y, R, distribution):
    field = [[-1 for _ in range(y)] for _ in range(x)]
    dist = copy.deepcopy(distribution)

    for i in range(x):
        for j in range(y):
            maxLocalScore = -10
            maxCrop = -1
            for c in range(len(dist)):

                if dist[c] == 0:
                    continue

                localScore = 0
                if i > 0 and j > 0:
                    localScore += R(field[i-1][j-1], c)
                if j > 0:
                    localScore += R(field[i][j-1], c)
                if i > 0:
                    localScore += R(field[i-1][j], c)

                if localScore > maxLocalScore:
                    maxLocalScore = localScore
                    maxCrop = c
            
            field[i][j] = maxCrop
            dist[maxCrop] -= 1


    return field