import cma
from project import Birkhoff, Polytope


# Agropol Data
# maxScoreGaertner is: 986.333333
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

def get_ind(i, j, c):
    return (i * y + j) * len(dist) + c

def scorePixel(field, pixel):
    score = 0
    p_x = pixel[0]
    p_y = pixel[1]
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            # not a neighbor
            if n1 == 0 and n2 == 0:
                continue

            # test valid neighbor
            if not (0 <= (p_x + n1) and (p_x + n1) < x):
                continue
            if not (0 <= (p_y + n2) and (p_y + n2) < y):
                continue

            # update score
            numb_crops = len(dist)
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    indA = get_ind(p_x, p_y, c1)
                    indB = get_ind(p_x + n1, p_y + n2, c2)
                    if field[indA] > 0 and field[indB] > 0:
                        score += field[indA] * field[indB] * R(c1, c2)
    return score

def score(field):
    score = 0
    for i in range(x):
        for j in range(y):
            score += scorePixel(field, (i, j))
    return score

def project(field):
    # put field into birkhoff form
    birkhoff_field = []
    for i in range(x):
        for j in range(y):
            pixel = [0.0 for _ in range(sum(dist))]
            for c in range(len(dist)):
                crop_amount = field[get_ind(i, j, c)]
                
                offset = 0
                for a in range(c):
                    offset += dist[a]
                
                for a in range(dist[c]):
                    pixel[a + offset] = crop_amount / dist[c]

            birkhoff_field += pixel

    # project
    birkhoff_polytope = Birkhoff()
    projected = birkhoff_polytope.Euclidean_project(birkhoff_field)

    # put field back into standard form
    projected_standard_field = []
    for i in range(x):
        for j in range(y):
            pixel = [0.0 for _ in dist]
            crop = 0
            for c in range(sum(dist)):
                while c >= sum(dist[0:(crop+1)]):
                    crop += 1
                pixel[crop] += projected[c + sum(dist) * (i * y + j)]
            
            projected_standard_field += pixel



    return projected_standard_field

def f(field):
    return -score(project(field))


def main():
    # generate random 1d field
    field_stretched = []
    for i, d in enumerate(dist):
        field_stretched += [i] * d

    # generate x0 vector
    x0 = []

    for i in range(x):
        for j in range(y):
            pixel = [0 for _ in range(len(dist))]
            pixel[field_stretched[i * y + j]] = 1.0
            x0 += pixel


    es = cma.CMAEvolutionStrategy(x0, 0.5)
    es.optimize(f)
    es.result_pretty()

main()

