import copy
from scorer import score

def tick(clock):
    # find smallest pos and increment
    i = 0
    while i < len(clock) - 1 and clock[i] + 1 == clock[i+1]:
        i += 1
    clock[i] += 1

    # reset bottom of clock
    for j in range(i):
        clock[j] = j
    return clock
    



def recSolve(field, R, dist, scorer, iteration):

    # terminate if necessary
    if iteration >= len(dist):
        return field

    # rec vars
    n = dist[iteration]
    availableSlots = sum(dist[iteration:])
    maxSol = field
    maxSolScore = None

    # skip if crop not available
    if n == 0:
        return recSolve(field, R, dist, scorer, iteration+1)

    # iterate through all options
    clock = [i for i in range(n)]
    while clock[n-1] != availableSlots:
        
        # set field
        pos = 0
        count = 0
        for x, x_lst in enumerate(field):
            for y, v in enumerate(x_lst):
                if v == -1:
                    if pos < n and count == clock[pos]:
                        field[x][y] = iteration
                        pos += 1
                    count += 1
        
        # recurse
        if iteration + 1 < len(dist):
            sol = recSolve(field, R, dist, scorer, iteration+1)
            score = scorer(R, sol)
            if maxSolScore == None or maxSolScore < score:
                maxSol = copy.deepcopy(sol)
                maxSolScore = score
        else:
            return field
        
        # reset field
        for x, x_lst in enumerate(field):
            for y, v in enumerate(x_lst):
                if v == iteration or v == len(dist)-1:
                    field[x][y] = -1

        clock = tick(clock)
    return maxSol

def solver(x, y, R, dist):
    field = [[-1 for _ in range(y)] for _ in range(x)]
    return recSolve(field, R, dist, score, 0)
