import random
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm


# Cotrini Data (Sum = 26)
X = 15
Y = 15
R = lambda a, b: [
    [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
    [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
    [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
    [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
    [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
    [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
    [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
    [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
    [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
    [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# Agropol Data (Sum = 8)
# X = 15
# Y = 15
# R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
#     [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
#     [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
#     [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
#     [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
#     [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
#     [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
# dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]


def score(R, field):
    score = 0
    for i in range(X):
        for j in range(Y):
            for n1 in range(-1, 2):
                for n2 in range(-1, 2):
                    # not a neighbor
                    if n1 == 0 and n2 == 0:
                        continue

                    # test valid neighbor
                    if not (0 <= (i + n1) and (i + n1) < X):
                        continue
                    if not (0 <= (j + n2) and (j + n2) < Y):
                        continue

                    # update score
                    score += R(field[i * Y + j], field[(i+n1) * Y + (j+n2)])
    return score

def main():

    trials = []
    # generate random 1d field
    field = []
    for i, d in enumerate(dist):
        field += [i] * d

    for i in range(1_000_000):

        random.shuffle(field)
        trials.append(score(R, field))

        if i % 10000 == 0:
            print(i)

    # Fit a normal distribution to the data:
    mu, std = norm.fit(trials)

    # histogram = numpy.histogram(trials, bins=20, range=None, normed=None, weights=None, density=None)
    # print(histogram)
    _ = plt.hist(trials, bins=range(int(min(trials)), int(max(trials) + 2), 1))
    plt.title("Unused Data Matrix, mu =" + str(mu) + ", std =" + str(std))
    plt.savefig('histogram.png')
main()