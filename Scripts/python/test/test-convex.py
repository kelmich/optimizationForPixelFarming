import itertools

# trivial example
X = 1
Y = 7
R  = lambda a, b:  [
    [-1,1,0,0],
    [1,-1,1,0],
    [0,0,-1,1],
    [1,0,0,-1]
][a][b]
dist = [3,4,0,0]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(X) for j in range(Y)]


# helper functions
def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < X and n[1] < Y:
                neighbors.append(n)
    return neighbors

def score(field):
    s = 0
    for p in pixels:
        neigh = get_neighbors(p)
        for n in neigh:
            s += R(field[get_pixel_id(p)], field[get_pixel_id(n)])
    return s

def score_frac(field):
    s = 0
    for p in pixels:
        neigh = get_neighbors(p)
        for n in neigh:
            for cP in range(numb_crops):
                for cN in range(numb_crops):
                    s += field[get_frac_pixel_id(p, cP)] * field[get_frac_pixel_id(n, cN)] * R(cP, cN)
    return s

def get_pixel_id(p):
    return p[1] * X + p[0]

def get_frac_pixel_id(p, c):
    return get_pixel_id(p) * numb_crops + c
def print_field(field):
    for j in range(Y):
        for i in range(X):
            print(field[get_pixel_id((i, j))], end=" ")
        print("")
    print("")


# build an initial field
initial_field = sum([[c for _ in range(dist[c])] for c in range(numb_crops)], [])

violations = 0

for p1 in itertools.permutations(initial_field):
    for p2 in itertools.permutations(initial_field):

        score1 = score(p1)
        score2 = score(p2)

        # build the fractional solution
        factor = 0.5
        frac_field = [0 for _ in range(numb_crops * X * Y)]
        for p in pixels:
            frac_field[get_frac_pixel_id(p, p1[get_pixel_id(p)])] += factor
            frac_field[get_frac_pixel_id(p, p2[get_pixel_id(p)])] += (1.0 - factor)

        scoreF = score_frac(frac_field)

        if factor * score1 + (1.0 - factor) * score2 < scoreF:

            violations += 1

            # print_field(p1)
            # print_field(p2)
            # print(score1, score2, scoreF)
            # print(frac_field)
            # exit(0)
print(violations, "violations found")