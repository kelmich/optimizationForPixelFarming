from random import choice
from itertools import permutations
from copy import deepcopy
from scorer import scorePixel, score
from bruteForce import solver
import random
from PyProbs import Probability as pr

def simulatedAnnealing(R, field, starting_temp, stopping_temp, rate):
    temp = starting_temp
    x = len(field)
    y = len(field[0])
    coordinates = [(i, j) for i in range(x) for j in range(y)]
    while temp > stopping_temp:
        posA = choice(coordinates)
        posB = choice(coordinates)
        scoreBefore = scorePixel(R, field, posA) + scorePixel(R, field, posB)
        colorA = field[posA[0]][posA[1]]
        colorB = field[posB[0]][posB[1]]
        field[posA[0]][posA[1]] = colorB
        field[posB[0]][posB[1]] = colorA
        scoreAfter = scorePixel(R, field, posA) + scorePixel(R, field, posB)
        if scoreAfter <= scoreBefore or pr.Prob(0.5):
            field[posA[0]][posA[1]] = colorA
            field[posB[0]][posB[1]] = colorB
        
            temp *= rate

    return field

def simulatedAnnealingMultiple(R, field, tries, mulitplicity):
    x = len(field)
    y = len(field[0])
    coordinates = [[i, j] for i in range(x) for j in range(y)]
    for _ in range(tries):
        pixels = []
        for _ in range(mulitplicity):
            p = choice(coordinates)
            while p in pixels:
                p = choice(coordinates)
            pixels.append(p)

        maxScore = score(R, field)
        tmpField = deepcopy(field)
        perms = permutations(pixels)
        for perm in perms:
            for i, p in enumerate(perm):
                tmpField[pixels[i][0]][pixels[i][1]] = field[p[0]][p[1]]
            tmpScore = score(R, tmpField)
            if tmpScore > maxScore:
                field = deepcopy(tmpField)
                maxScore = tmpScore

    return field

def simulatedAnnealingField(R, field, tries, dX, dY):
    x = len(field)
    y = len(field[0])
    coordinates = [(i, j) for i in range(x-dX) for j in range(y-dY)]
    topLeft = choice(coordinates)
    for _ in range(tries):
        dist = [0 for _ in range(max([max(l) for l in field]))]
        for i in range(topLeft[0], topLeft[0] + dX):
            for j in range(topLeft[1], topLeft[1] + dY):
                dist[field[i][j]] += 1
        subfield = solver(dX, dY, R, dist)
        tmpField = deepcopy(field)
        for i in range(topLeft[0], topLeft[0] + dX):
            for j in range(topLeft[1], topLeft[1] + dY):
                tmpField[i][j] = subfield[i - topLeft[0]][j - topLeft[1]]
        if score(R, tmpField) > score(R, field):
            field = tmpField

    return field