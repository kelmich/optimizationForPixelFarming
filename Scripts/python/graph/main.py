import itertools
import copy

# sol: (1, 3, 7, 8, 9, 4, 6, 2, 0, 5) 2

# Cotrini Data (Sum = 26)
X = 15
Y = 15
R = lambda a, b: [
    [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
    [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
    [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
    [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
    [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
    [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
    [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
    [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
    [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
    [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

def score(path):
    s = 0
    for i in range(len(dist)-1):
        s += R(path[i], path[i+1]) + R(path[i+1], path[i])
    return s

best_path = [i for i in range(len(dist))]
best_score = -100000
base = [i for i in range(len(dist))]
for i, perm in enumerate(itertools.permutations(base)):
    if i % 100000 == 0:
        print(i)
    s = score(perm)
    if best_score < s:
        best_path = copy.deepcopy(perm)
        best_score = s
print(best_path, s)