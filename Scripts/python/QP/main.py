# Import packages.
import cvxpy as cp
import numpy as np

# Toy Example
# x = 1
# y = 2
# R = lambda a, b: 1 if abs(a - b) <= 1 else 0
# dist = [1,1]

# larger example
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# Cotrini Data (Sum = 26)
# x = 15
# y = 15
# R = lambda a, b: [
#     [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
#     [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
#     [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
#     [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
#     [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
#     [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
#     [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
#     [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
#     [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
#     [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
# dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

#
# HELPERS
#
dp = {}
def get_ind(pA, pB, c1, c2):
    return dp.get(str(pA) + "|" + str(pB) + "|" + str(c1) + "|" + str(c2))

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors
            
#
# END HELPERS
#

def main():

    # build the objective function
    ind = 0
    obj = []
    for p in pixels:
        neighbors = get_neighbors(p)
        for n in neighbors:
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    if get_ind(p, n, c1, c2) is None:
                        obj.append(-(R(c1, c2) + R(c2, c1))) # the solver minimizes

                        # speeds up index queries
                        dp[str(p) + "|" + str(n) + "|" + str(c1) + "|" + str(c2)] = ind
                        dp[str(n) + "|" + str(p) + "|" + str(c2) + "|" + str(c1)] = ind
                        ind += 1

    print(len(obj))
    
    # build the constraints
    A = []
    b = []

    # constrain that all neighborhood relationships sum to 1
    for p in pixels:
        neighbors = get_neighbors(p)
        for n in neighbors:
            constr = [0 for _ in obj]
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    constr[get_ind(p, n, c1, c2)] = 1
            A.append(constr)
            b.append(1)
    
    # constrain that all neighbors imply the same pixel
    for p in pixels:
        neighbors = get_neighbors(p)
        n0 = neighbors.pop()
        for n in neighbors:
            for c1 in range(numb_crops):
                constr = [0 for _ in obj]
                for c2 in range(numb_crops):
                    constr[get_ind(p, n, c1, c2)] = -1
                    constr[get_ind(p, n0, c1, c2)] = 1

                A.append(constr)
                b.append(0)
    

    # constrain the global crop distribution
    for cTest in range(numb_crops):
        constr = [0 for _ in obj]
        for p in pixels:
            n0 = get_neighbors(p).pop()
            for c2 in range(numb_crops):
                constr[get_ind(p, n0, cTest, c2)] = 1
        A.append(constr)
        b.append(dist[cTest])

    # Define and solve the CVXPY problem.
    P = np.ones((len(obj), len(obj)))
    q = np.ones(len(obj))
    x = cp.Variable(len(obj))
    G = np.matrix(obj)
    h = np.matrix([-1.5])
    A = np.matrix(A)
    b = np.array(b).T
    print(np.all(np.linalg.eigvals(P) >= 0))
    print(np.linalg.eigvals(P))
    prob = cp.Problem(cp.Minimize(cp.quad_form(x, P) - q.T @ x),
                    [G @ x <= h,
                    0 <= x, x <= 1,
                    A @ x == b])
    prob.solve()

    # Print result.
    print("\nThe optimal value is", prob.value)
    print("A solution x is")
    print(list(x.value))
    print("A dual solution corresponding to the inequality constraints is")
    print(prob.constraints[0].dual_value)

main()