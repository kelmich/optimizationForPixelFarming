from cmath import sqrt
import random
import numpy as np
import gurobipy as gp
from gurobipy import GRB

# Agroscope
X = 15
Y = 15
R  = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
        [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
        [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
        [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
        [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
        [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
        [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(X) for j in range(Y)]

#
# Helper functions
#
def get_ind(p, c):
    return numb_crops * pixels.index(p) + c

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < X and n[1] < Y:
                neighbors.append(n)
    return neighbors

#
# The interesting stuff
#

def main():



    # define the score function
    obj = [[0 for _ in range(X * Y * numb_crops)] for _ in range(X * Y * numb_crops)]
    for pA in pixels:
        neighbors = get_neighbors(pA)
        for pB in neighbors:
            for cA in range(numb_crops):
                for cB in range(numb_crops):
                    # v = -1 if R(cA, cB) + R(cB, cA) >= 0 else 0 # (-R(cA, cB) - R(cB, cA)) / 2 + 1
                    v = R(cA, cB) + R(cB, cA) + 2
                    obj[get_ind(pA, cA)][get_ind(pA, cA)] = 2
                    obj[get_ind(pA, cA)][get_ind(pB, cB)] = -v
                    obj[get_ind(pB, cB)][get_ind(pA, cA)] = -v
                    obj[get_ind(pB, cB)][get_ind(pB, cB)] =2
    # print(obj)
    obj = np.array(obj)
    n = len(obj)

    # Build the QP
    m = gp.Model("qpc")
    x = m.addMVar(n, lb=[0.0 for _ in obj], ub=[1.0 for _ in obj])
    m.setObjective(x @ obj @ x)

    # all pixel amounts sum to 1
    pixels_totals_one = []
    pixels_totals_one_b = []
    for p in pixels:
        constr = [0 for _ in obj]
        for c in range(numb_crops):
            constr[get_ind(p, c)] = 1
        pixels_totals_one.append(constr)
        pixels_totals_one_b.append(1)
    pixels_totals_one = np.array(pixels_totals_one)
    pixels_totals_one_b = np.array(pixels_totals_one_b)
    m.addConstr(pixels_totals_one @ x == pixels_totals_one_b, "pixels totals one")
    
    # Enforce the global crop distribution
    global_crops = []
    global_crops_b = []
    for c in range(numb_crops):
        constr = [0 for _ in obj]
        for p in pixels:
            constr[get_ind(p, c)] = 1
        global_crops.append(constr)
        global_crops_b.append(dist[c])
    global_crops = np.array(global_crops)
    global_crops_b = np.array(global_crops_b)
    m.addConstr(global_crops @ x == global_crops_b, "global crops")

    # enforce no same crops next to each other
    # for pA in pixels:
    #     neighbors = get_neighbors(pA)
    #     for pB in neighbors:
    #         for c in range(numb_crops):
    #             constr = np.zeros((n, n))
    #             constr[get_ind(pA, c)][get_ind(pA, c)] = 1
    #             constr[get_ind(pB, c)][get_ind(pB, c)] = 1
    #             m.addConstr(x @ constr @ x <= 1, "quad constr")



    # ask the oracle
    print("Ask Oracle")
    m.optimize()

    # the solution is
    sol = []
    for v in m.getVars():
        sol.append(v.X)
    for p in pixels:
        for c in range(numb_crops):
            print(round(sol[get_ind(p, c)], 5), end=", ")
        print("")
        



main()