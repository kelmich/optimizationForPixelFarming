import numpy as np

# Toy Example
# x = 3
# y = 3
# R = lambda a, b: 1 if abs(a - b) <= 1 else 0
# dist = [1 for _ in range(9)]

# larger example
x = 5
y = 5
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [3, 3, 3, 3, 3, 2, 2, 2, 2, 2]

# Cotrini Data (Sum = 26)
# x = 15
# y = 15
# R = lambda a, b: [
#     [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
#     [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
#     [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
#     [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
#     [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
#     [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
#     [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
#     [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
#     [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
#     [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
# dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

#
# HELPERS
#
dp = {}
def get_ind(pA, pB, c1, c2):
    return dp.get(str(pA) + "|" + str(pB) + "|" + str(c1) + "|" + str(c2))

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors

def is_sem_pos_def(x):
    return np.all(np.linalg.eigvals(x) >= -0.00000000001)
            
#
# END HELPERS
#



def main():
    #
    # build the LP
    #
    obj = []

    # build the objective function
    ind = 0
    for p in pixels:
        neighbors = get_neighbors(p)
        for n in neighbors:
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    if get_ind(p, n, c1, c2) is None:
                        obj.append(-(R(c1, c2) + R(c2, c1))) # the solver minimizes

                        # speeds up index queries
                        dp[str(p) + "|" + str(n) + "|" + str(c1) + "|" + str(c2)] = ind
                        dp[str(n) + "|" + str(p) + "|" + str(c2) + "|" + str(c1)] = ind
                        ind += 1
    
    # test if positive semi definite
    A = np.zeros((len(obj), len(obj)))
    for p in pixels:
        neighbors = get_neighbors(p)
        for n in neighbors:
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    for cA in range(numb_crops):
                        for cB in range(numb_crops):
                                A[get_ind(p, n, c1, c2)][get_ind(p, n, cA, cB)] = 1
    
    # B = np.zeros((numb_crops, numb_crops))
    # for i in range(numb_crops):
    #     for j in range(numb_crops):
    #         B[i][j] = (R(i, j) + R(j, i)) / 2 + 5


    # print(A)
    # for l in A:
    #     print(list(l))
    print(is_sem_pos_def(A))

    # seen = {}
    # for i in range(1, 2000):
    #     if is_sem_pos_def(np.ones((i,i))):
    #         print(i)

main()



