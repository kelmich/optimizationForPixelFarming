from math import floor
import random

def print_field(field):
    for row in field:
        for p in row:
            print(p, end="\t")
        print("")

# Grow method A 
def growA(field):
    newField = []
    x = len(field)
    y = len(field[0])
    for i in range(x):
        newRow = []
        for j in range(int(y/2)):
            newRow.append(field[i][2 * j])
            newRow.append(field[i][2 * j + 1])
            newRow.append(field[i][2 * j])
            newRow.append(field[i][2 * j + 1])
        newField.append(newRow)
    return newField

# Grow method A (alt def)
def growA_alt(field, f):
    x = len(field)
    y = len(field[0])
    newField = [[-1 for _ in range(f * (y-1)+1)] for _ in range(f * (x-1)+1)]
    for i in range(f * (x-1)+1):
        for j in range(f * (y-1)+1):
            a = int( floor(i / f) + ((i + floor(i / f)) % 2))
            b = int( floor(j / f) + ((j + floor(j / f)) % 2))

            newField[i][j] = field[a][b]
    return newField

def growB(field, fx, fy):
    newField = []
    y = len(field)
    x = len(field[0])
    for j in range((y-1) * fy + 1):
        newRow = []
        a = floor(j / fy) + (j + floor(j / fy)) % 2
        for i in range((x-1) * fx + 1):
            b = floor(i / fx) + (i + floor(i / fx)) % 2
            newRow.append(field[a][b])
        newField.append(newRow)
    return newField

# makes a solution fit exact problem constraints
# Assumes field is smaller than wanted
def fix(field, dist, xGoal, yGoal):
    x = len(field)
    y = len(field[0])
    dx = xGoal - x
    dy = yGoal - y

    # increase the field size
    for i in range(dx):
        field.append([-1] * y)
    for i in range(yGoal):
        field[i] += [-1] * dy

    # see what has already been planted
    for i in range(x):
        for j in range(y):
            dist[field[i][j]] -= 1

    # assign fields randomly
    coordinates = [(j, i) for i in range(xGoal) for j in range(yGoal)]
    choices = list(filter(lambda c: field[c[0]][c[1]] == -1, coordinates))

    # plant randomly
    random.shuffle(choices)
    for index, d in enumerate(dist):
        for _ in range(d):
            pixel = choices.pop()
            field[pixel[0]][pixel[1]] = index
    
    return field