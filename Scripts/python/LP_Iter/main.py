import random
from scipy.optimize import linprog
import copy
import multiprocessing as mp


# Toy Example
# x = 3
# y = 3
# R = lambda a, b: 1 if abs(a - b) <= 1 else 0
# dist = [1,1,1,1,1,1,1,1,1]

# larger example
# maxScoreGaertner is: 986.333333
# x = 15
# y = 15
# R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
#     [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
#     [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
#     [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
#     [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
#     [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
#     [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
# dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# Cotrini Data (Sum = 26)
x = 15
y = 15
R = lambda a, b: [
    [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
    [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
    [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
    [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
    [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
    [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
    [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
    [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
    [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
    [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# Cotrini from Thesis
x = 15
y = 15
R = lambda a, b: [[-0.5,1,1,1,1,1,0.5,-1,0.5,0.5,1],
    [1 , -0.5 , -1 , 1 , 1 , 0.5 , 0.5 , -1 , 1 , 1 , 1],
    [1 , -1 , -0.5 , -1 , 1 , 0 , 0.5 , 0.5 , 0 , 0 , 0],
    [1 , 1 , -1 , -0.5 , 0 , 0 , 0.5 , -1 , 1 , -1 , 0],
    [1 , 1 , 1 , 0 , -0.5 , 0 , 0.5 , 1 , 1 , 1 , 1],
    [1 , 0.5 , 0 , 0 , 0 , -0.5 , 0.5 , 0 , 0 , 0 , 0],
    [0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5],
    [-1 , -1 , 0.5 , -1 , 1 , 0 , 0.5 , -0.5 , 0 , 0.5 , 0],
    [0.5 , 1 , 0 , 1 , 1 , 0 , 0.5 , 0 , -0.5 , 1 , 0],
    [0.5 , 1 , 0 , -1 , 1 , 0 , 0.5 , 0.5 , 1 , -0.5 , 1],
    [1 , 1 , 0 , 0 , 1 , 0 , 0.5 , 0 , 0 , 1 , -0.5]][a][b]
dist = [20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21]

# auto calculate some vars
numb_crops = len(dist)

#
# HELPERS
#

# i is x-coord, j is y coord, c is the crop
def get_ind(i, j, c):
    if i < 0 or j < 0:
        return -1
    ind = int((i * y + j) * len(dist) + c)
    if ind >= x * y * numb_crops:
        return -1
    return ind

def get_pos(var):
    cPos = var % numb_crops
    xPos = int(((var - cPos) / numb_crops) / y)
    yPos = ((var - cPos) / numb_crops) % y

    return [xPos, yPos, cPos]

def scorePixel(field, pixel):
    score = 0
    p_x = pixel[0]
    p_y = pixel[1]
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            # not a neighbor
            if n1 == 0 and n2 == 0:
                continue

            # test valid neighbor
            if not (0 <= (p_x + n1) and (p_x + n1) < x):
                continue
            if not (0 <= (p_y + n2) and (p_y + n2) < y):
                continue

            # update score
            numb_crops = len(dist)
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    indA = get_ind(p_x, p_y, c1)
                    indB = get_ind(p_x + n1, p_y + n2, c2)
                    if field[indA] > 0 and field[indB] > 0:
                        score += field[indA] * field[indB] * (R(c1, c2) + R(c2, c1)) /2
    return score

def score(field):
    score = 0
    for i in range(x):
        for j in range(y):
            score += scorePixel(field, (i, j))
    return score

def print_field(x0):
    for i in range(x):
        print("[", end="")
        for j in range(y):
            for c in range(numb_crops):
                if x0[get_ind(i, j, c)] == 1:
                    print(c, end=" ")
            print(", ", end="")
        print("], ")


#
# END HELPERS
#

def scramble(x0):
    for i in range(x-1):
        for j in range(y-1):
            for n1 in range(0, 2):
                for n2 in range(0, 2):
                    if n1 == 0 and n2 == 0:
                        continue
                    c1 = -1
                    c2 = -1
                    for c in range(numb_crops):
                        ind1 = get_ind(i, j, c)
                        ind2 = get_ind(i + n1, j + n2, c)

                        if x0[ind1] == 1:
                            c1 = c
                        if x0[ind2] == 1:
                            c2 = c
                    if c1 == -1 or c2 == -1:
                        continue
                    
                    score_delta = -(scorePixel(x0, (i, j)) + scorePixel(x0, (i+n1, j+n2)))

                    x0[get_ind(i, j, c1)] = 0.0
                    x0[get_ind(i, j, c2)] = 1.0
                    x0[get_ind(i+n1, j+n2, c2)] = 0.0
                    x0[get_ind(i+n1, j+n2, c1)] = 1.0

                    score_delta += scorePixel(x0, (i, j)) + scorePixel(x0, (i+n1, j+n2))

                    if score_delta < 0 and random.random() > 0.95:
                        x0[get_ind(i, j, c1)] = 1.0
                        x0[get_ind(i, j, c2)] = 0.0
                        x0[get_ind(i+n1, j+n2, c2)] = 1.0
                        x0[get_ind(i+n1, j+n2, c1)] = 0.0
    return x0


def iteration(x0):

    # generate pixel subset
    all_pixels = [(i, j) for i in range(x) for j in range(y)]
    pixels = []
    random.shuffle(all_pixels)
    for p in all_pixels:
        # if random.choice([True, False, False, False]):
        #     pixels.append(p)
        neighbors = False
        for q in pixels:
            if abs(p[0] - q[0]) <= 1 and abs(p[1] - q[1]) <= 1:
                neighbors = True
                break
        if not neighbors and random.random() < 0.125:
            pixels.append(p)
    
    # generate objective function
    obj = []
    for p in pixels:
        for c in range(numb_crops):
            s = 0
            for cn in range(numb_crops):
                for n1 in range(-1, 2):
                    for n2 in range(-1, 2):
                        if n1 == 0 and n2 == 0:
                            continue
                        # test valid neighbor
                        if not (0 <= (p[0] + n1) and (p[0] + n1) < x):
                            continue
                        if not (0 <= (p[1] + n2) and (p[1] + n2) < y):
                            continue
                        ind = get_ind(p[0] + n1, p[1] + n2, cn)
                        if ind > -1:
                            s -= (R(c, cn) + R(cn, c)) * x0[ind] # the solver minimizes
            obj.append(s)
    
    # generate constraints for pixel sum
    lhs_eq = []
    for p in range(len(pixels)):
        const = []
        for q in range(len(obj)):
            if int(q / numb_crops) == p:
                const.append(1)
            else:
                const.append(0)
        lhs_eq.append(const)
    rhs_eq = [1 for _ in range(len(lhs_eq))]

    # generate constraints for global sum
    for c in range(numb_crops):
        s = 0
        const = [ 0 for _ in range(len(obj)) ]
        for i, p in enumerate(pixels):
            const[numb_crops * i + c] = 1
            s += x0[get_ind(p[0], p[1], c)]
        lhs_eq.append(const)
        rhs_eq.append(s)
    
    # generate variable bounds
    bnd = [(0, 1) for _ in range(len(obj))]

    # ask the oracle
    opt = linprog(c=obj, A_eq=lhs_eq, b_eq=rhs_eq, bounds=bnd, method="revised simplex", options={ "tol": 0.001 }).x
    
    # insert the solution
    for i, p in enumerate(pixels):
        for c in range(numb_crops):
            ind = get_ind(p[0], p[1], c)
            x0[ind] = opt[i * numb_crops + c]
    
    return x0




def main():

    best_x0 = []
    best_x0_score = 1100.00

    while True:

        # generate random 1d field
        field_stretched = []
        for i, d in enumerate(dist):
            field_stretched += [i] * d
        random.shuffle(field_stretched)

        # generate x0 vector
        x0 = []


        for i in range(x):
            for j in range(y):
                pixel = [0 for _ in range(len(dist))]
                pixel[field_stretched[i * y + j]] = 1.0
                x0 += pixel

        iterations = 0
        prev_score = -100 * x * y
    
    
        print("Initial Score: ", score(x0), "Sum: ", sum(x0))
        x0 = scramble(x0)
        print(score(x0))


        scrambled = True
    
        while iterations < 100 or (iterations < 200 and score(x0) > best_x0_score):
            x0 = iteration(x0)
            if random.random() < 0.5 and not scrambled:
                print("Scramble")
                x0 = scramble(x0)
                scrambled = True
            iterations += 1

            scr = score(x0)

            if scr > prev_score:
                prev_score = scr
                iterations = 0



            print("Score: ", scr)
        
            if score(x0) > best_x0_score:
                best_x0 = copy.deepcopy(x0)
                best_x0_score = score(x0)

                print("Write to file")
                f = open("best-score-easy-prob.txt", "a")
                f.write("Score: " + str(best_x0_score) + "\n x0 = " + str(best_x0) + "\n\n")
                f.close()

        print("Terminated")
        print("Score: ", score(x0))
        print("Best Score: ", best_x0_score)
        # print(best_x0)



main()