import numpy as np
import gurobipy as gp
from gurobipy import GRB


# test if the quadratic constraint is spd

R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(X) for j in range(Y)]
Rnew = lambda a, b: R(i, j) + 99999999999999999


# print(np.linalg.eigvals(A) >= -0.000001)

#
# Helper functions
#
def get_ind(p, c):
    return numb_crops * p + c

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < X and n[1] < Y:
                neighbors.append(n)
    return neighbors

#
# The interesting stuff
#
def main():
    # define the score function
    obj = []
    ind = 0
    for pA in pixels:
        neighbors = get_neighbors(pA)
        for pB in neighbors:
            for cA in range(numb_crops):
                for cB in range(numb_crops):
                    if get_ind(pA, pB, cA, cB) is None:
                        obj.append(-(R(cA, cB) + R(cB, cA)))
    obj = np.array(obj)
    n = len(obj)

    # Build the QP
    m = gp.Model("qp")
    x = m.addMVar(n, lb=[0.0 for _ in obj], ub=[1.0 for _ in obj])
    m.setObjective(obj @ x)

main()