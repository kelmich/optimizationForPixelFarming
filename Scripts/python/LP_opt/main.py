from scipy.optimize import linprog
import numpy as np


# Toy Example
# x = 1
# y = 15
# R = lambda a, b: 1 if abs(a - b) <= 1 else 0
# dist = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

# larger example
x = 15
y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]


# Cotrini Data (Sum = 26)
# x = 15
# y = 15
# R = lambda a, b: [
#     [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
#     [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
#     [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
#     [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
#     [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
#     [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
#     [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
#     [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
#     [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
#     [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
# dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
pixels = [(i, j) for i in range(x) for j in range(y)]

#
# HELPERS
#
dp = {}
def get_ind(pA, pB, c1, c2):
    return dp.get(str(pA) + "|" + str(pB) + "|" + str(c1) + "|" + str(c2))

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < x and n[1] < y:
                neighbors.append(n)
    return neighbors
            
#
# END HELPERS
#





def main():
    #
    # build the LP
    #
    obj = []
    rhs_leq = []
    lhs_leq = []
    rhs_eq = []
    lhs_eq = []

    # build the objective function
    ind = 0
    for p in pixels:
        neighbors = get_neighbors(p)
        for n in neighbors:
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    if get_ind(p, n, c1, c2) is None:
                        obj.append(-(R(c1, c2) + R(c2, c1))) # the solver minimizes

                        # speeds up index queries
                        dp[str(p) + "|" + str(n) + "|" + str(c1) + "|" + str(c2)] = ind
                        dp[str(n) + "|" + str(p) + "|" + str(c2) + "|" + str(c1)] = ind
                        ind += 1

    # constrain that all neighborhood relationships sum to 1
    for p in pixels:
        neighbors = get_neighbors(p)
        for n in neighbors:
            constr = [0 for _ in obj]
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    constr[get_ind(p, n, c1, c2)] = 1
            lhs_eq.append(constr)
            rhs_eq.append(1)
    
    # constrain that all neighbors imply the same pixel
    for p in pixels:
        neighbors = get_neighbors(p)
        n0 = neighbors.pop()
        for n in neighbors:
            for c1 in range(numb_crops):
                constr = [0 for _ in obj]
                for c2 in range(numb_crops):
                    constr[get_ind(p, n, c1, c2)] = -1
                    constr[get_ind(p, n0, c1, c2)] = 1

                lhs_eq.append(constr)
                rhs_eq.append(0)
    

    # constrain the global crop distribution
    for cTest in range(numb_crops):
        constr = [0 for _ in obj]
        for p in pixels:
            n0 = get_neighbors(p).pop()
            for c2 in range(numb_crops):
                constr[get_ind(p, n0, cTest, c2)] = 1
        lhs_eq.append(constr)
        rhs_eq.append(dist[cTest])


    



    
    # generate variable bounds
    bnd = [(0, 1) for _ in obj]

    print("Started solving the LP with", len(obj), "variables")

    # solve the LP
    opt = linprog(c=obj, A_eq=lhs_eq, b_eq=rhs_eq, bounds=bnd, method="revised simplex", options={ "tol": 0.001})

    print("Solved the LP. Optimal score is: ", abs(opt.fun))
    sol = list(opt.x)
    for p in pixels:
        neighbors = get_neighbors(p)
        n0 = neighbors.pop()
        for c1 in range(numb_crops):
            s = 0
            for c2 in range(numb_crops):
                s += sol[get_ind(p, n0, c1, c2)]
        #     print(s, end=", ")
        # print("")

    



        




main()