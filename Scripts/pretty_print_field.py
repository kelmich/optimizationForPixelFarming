import matplotlib as mpl
from matplotlib import pyplot
import matplotlib.patches as mpatches
import numpy as np

fig = pyplot.figure(2, figsize=(9, 6))

# make values from -5 to 5, for this example
zvals = [
    [7, 2, 7, 2, 7, 2, 7, 2, 7, 2, 7, 2, 7, 2, 7], 
    [6, 6, 4, 9, 4, 9, 4, 9, 4, 9, 4, 9, 4, 6, 6], 
    [5, 2, 10, 7, 10, 7, 10, 7, 10, 1, 10, 7, 10, 2, 5], 
    [6, 0, 4, 9, 4, 9, 4, 9, 4, 9, 4, 9, 4, 0, 6], 
    [5, 2, 10, 7, 10, 7, 10, 7, 10, 7, 10, 7, 10, 2, 5], 
    [6, 0, 4, 9, 4, 9, 4, 9, 4, 9, 4, 9, 4, 0, 6], 
    [2, 9, 10, 1, 8, 1, 8, 1, 8, 1, 8, 1, 10, 2, 5], 
    [7, 4, 10, 9, 10, 9, 10, 9, 10, 9, 10, 9, 4, 0, 6], 
    [2, 6, 8, 1, 8, 1, 8, 1, 8, 1, 8, 0, 10, 2, 5], 
    [7, 6, 3, 3, 0, 3, 0, 3, 0, 3, 3, 1, 5, 0, 6], 
    [2, 6, 8, 1, 8, 1, 8, 1, 8, 1, 8, 0, 10, 6, 5], 
    [7, 6, 3, 3, 3, 3, 3, 3, 3, 0, 3, 1, 5, 0, 2], 
    [5, 8, 1, 8, 1, 8, 1, 8, 1, 8, 5, 0, 5, 6, 5], 
    [3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 2, 6, 2, 0, 2], 
    [5, 6, 5, 8, 3, 8, 5, 6, 5, 6, 5, 7, 5, 6, 5]
]

# make a color map of fixed colors
cmap = mpl.colors.ListedColormap(["red", "green", "yellow", "orange", "blue", "pink", "purple", "brown", "black", "white", "cyan"])
bounds=[0,1,2,3,4,5,6,7,8,9,10]
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

# tell imshow about color map so that only set colors are used
img = pyplot.imshow(zvals,interpolation='nearest',
                    cmap = cmap,norm=norm)

# make a color bar
colorbar = pyplot.colorbar(img,cmap=cmap,
                norm=norm,boundaries=bounds,ticks=[0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5])

# colorbar.ax.set_yticklabels(["Beets", "Lettuce", "Herbal Mixture", "Green Beans", "Sweetcorn", "Hemp", "Onions", "Potatoes", "Carrots", "Cabbage"])  # horizontal colorbar
colorbar.ax.set_yticklabels(["Cruciferous plants", "Papilionaceous plants", "Solanaceous plants", "Potatoes", "Compositae", "Grains", "Herbs", "Lily plants", "Sweet grasses", "Cucurbitaceous plants", "Goosefoot plants"])  # horizontal colorbar



fig.savefig("plot-2.png")