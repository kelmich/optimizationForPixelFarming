import numpy as np

# Toy Example
X = 2
Y = 2
R = lambda a, b: [
        [2,-1,0],
        [-1,2,-1],
        [0,-1,2]
    ][a][b]
dist = [1,1,2]

# auto calculate some vars
numb_crops = len(dist)
crops = list(range(numb_crops))
pixels = [(i, j) for i in range(X) for j in range(Y)]

def get_ind(p, c):
    return numb_crops * pixels.index(p) + c

def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < X and n[1] < Y:
                neighbors.append(n)
    return neighbors


obj = [[0 for _ in range(X * Y * numb_crops)] for _ in range(X * Y * numb_crops)]

for pA in pixels:
    neighbors = get_neighbors(pA)
    for pB in neighbors:
        for cA in crops:
            for cB in crops:
                obj[get_ind(pA, cA)][get_ind(pB, cB)] = -((R(cA, cB) + R(cB, cA)) / 2)
for l in obj:
    print(l)

print("Eigenvalues are: ", np.linalg.eigvals(obj))
print("Constraint is Convex: ", np.all(np.linalg.eigvals(obj) >= 0))