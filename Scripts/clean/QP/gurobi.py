# from importlib_metadata import distribution
import numpy as np
import gurobipy as gp
from gurobipy import GRB

# Toy Example
X = 15
Y = 15
R = lambda a, b:  [[-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0],
    [0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0],
    [0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5],
    [-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0],
    [0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0],
    [0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1]][a][b]
dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# auto calculate some vars
numb_crops = len(dist)
crops = list(range(numb_crops))
pixels = [(i, j) for i in range(X) for j in range(Y)]

# get the index of a crop variable for a specific pixel in our x vector
def get_ind(p, c):
    return numb_crops * pixels.index(p) + c

# returns neighboring pixels for pixel p
def get_neighbors(p):
    neighbors = []
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            if n1 == 0 and n2 == 0:
                continue
            n = (p[0] + n1, p[1] + n2)

            if 0 <= n[0] and 0 <= n[1] and n[0] < X and n[1] < Y:
                neighbors.append(n)
    return neighbors

# the fun stuff
def main():

    # define objective function
    obj = [[0 for j in range(X * Y * numb_crops)] for i in range(X * Y * numb_crops)]
    for pA in pixels:
        neighbors = get_neighbors(pA)
        for pB in neighbors:
            for cA in crops:
                for cB in crops:
                    obj[get_ind(pA, cA)][get_ind(pB, cB)] = -(R(cA, cB) + R(cB, cA))/2
    obj = np.array(obj)
    print(obj)
    # print("Eigenvalues of obj: ", np.linalg.eigvals(obj))

    # set helper vars
    n = len(obj)

    # define the QP
    m = gp.Model("qpc")
    m.params.NonConvex = 2
    x = m.addMVar(n, lb=[0.0 for _ in range(n)], ub=[1.0 for _ in range(n)])
    # x = m.addMVar(n, lb=[0.0 for _ in range(n)], ub=[1.0 for _ in range(n)], vtype=GRB.INTEGER)
    m.setObjective(x @ obj @ x)

    # all pixel amounts sum to 1
    constr_left = []
    constr_right = []
    for p in pixels:
        constr = [0 for _ in range(n)]
        for c in crops:
            constr[get_ind(p, c)] = 1.0
        constr_left.append(constr)
        constr_right.append(1)
    constr_left = np.array(constr_left)
    constr_right = np.array(constr_right)
    m.addConstr(constr_left @ x == constr_right, "pixels totals one")

    # enforce the global crop distribution
    constr_left = []
    for c in crops:
        constr = [0 for _ in range(n)]
        for p in pixels:
            constr[get_ind(p, c)] = 1.0
        constr_left.append(constr)
    constr_left = np.array(constr_left)
    m.addConstr(constr_left @ x == np.array(dist), "global crop distribution")

    # that weird quad constraint
    # constr = [[1.0 for b in range(X * Y * numb_crops)] for a in range(X * Y * numb_crops)]
    # for pA in pixels:
    #     neighbors = get_neighbors(pA)
    #     for pB in neighbors:
    #         for cA in crops:
    #             for cB in crops:
    #                 constr[get_ind(pA, cA)][get_ind(pB, cB)] = 0
    # constr = np.array(constr)
    # m.addConstr(x @ constr @ x <= 1, "quad constraint")

    # ask the oracle
    print("Ask Oracle")
    m.optimize()





main()