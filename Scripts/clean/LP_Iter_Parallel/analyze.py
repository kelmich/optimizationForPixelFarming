import numpy as np

f = open("euler.txt", "r")

performances = []

for l in f:

    if not "0:" in l:
        continue

    # read in line
    line = l.split(":")[1].split("/")[0]
    performances.append(float(line))


# find the best
print("Avg Score:", np.average(performances))
print("Std Devitaion: ", np.std(performances))
print("Number of Trials:", len(performances))