import numpy as np


f = open("euler.txt", "r")

cases = 0
s = 0

performances = []

for l in f:

    try:
        cases += 1
        performances.append(float(l))
    except:
        pass


# find the best
print("Avg Score:", np.average(performances))
print("Std Devitaion: ", np.std(performances))
print("Number of Trials:", len(performances))