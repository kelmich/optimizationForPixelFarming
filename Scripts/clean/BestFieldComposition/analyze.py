import numpy as np


f = open("euler.txt", "r")

performances = [ [] for _ in range(210)]

for l in f:

    try:
        values = l.split(":")
        paramId = int(values[0])
        val = float(values[1].split("(")[0])
        performances[paramId].append(val)
    except:
        pass


# find the best
results = []
for i in range(len(performances)):
    performances[i] = np.array(performances[i])
    results.append([i, np.min(performances[i]), np.std(performances[i])])
results = sorted(results,key=lambda l:l[1], reverse=True)
for l in results:
    print(l)