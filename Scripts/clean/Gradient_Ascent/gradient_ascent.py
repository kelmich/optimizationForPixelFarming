import random
import copy
import math

from project import Birkhoff, Polytope


# Toy Example
# x = 3
# y = 3
# R = lambda a, b: 1 if abs(a - b) <= 1 else 0
# dist = [1,1,1,1,1,1,1,1,1]

# larger example
# x = 15
# y = 15
# R = lambda a, b: [
#     [-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1],
#     [1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1],
#     [1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0],
#     [1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0],
#     [1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1],
#     [1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0],
#     [-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0],
#     [0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0],
#     [0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1],
#     [1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5]][a][b]
# dist = [23, 23, 23, 23, 23, 22, 22, 22, 22, 22]

# Cotrini from Thesis
x = 15
y = 15
R = lambda a, b: [[-0.5,1,1,1,1,1,0.5,-1,0.5,0.5,1],
    [1 , -0.5 , -1 , 1 , 1 , 0.5 , 0.5 , -1 , 1 , 1 , 1],
    [1 , -1 , -0.5 , -1 , 1 , 0 , 0.5 , 0.5 , 0 , 0 , 0],
    [1 , 1 , -1 , -0.5 , 0 , 0 , 0.5 , -1 , 1 , -1 , 0],
    [1 , 1 , 1 , 0 , -0.5 , 0 , 0.5 , 1 , 1 , 1 , 1],
    [1 , 0.5 , 0 , 0 , 0 , -0.5 , 0.5 , 0 , 0 , 0 , 0],
    [0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5 , 0.5],
    [-1 , -1 , 0.5 , -1 , 1 , 0 , 0.5 , -0.5 , 0 , 0.5 , 0],
    [0.5 , 1 , 0 , 1 , 1 , 0 , 0.5 , 0 , -0.5 , 1 , 0],
    [0.5 , 1 , 0 , -1 , 1 , 0 , 0.5 , 0.5 , 1 , -0.5 , 1],
    [1 , 1 , 0 , 0 , 1 , 0 , 0.5 , 0 , 0 , 1 , -0.5]][a][b]
dist = [20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21]

# auto calculate some vars
numb_crops = len(dist)
eps = 0.2

#
# HELPERS
#

# i is x-coord, j is y coord, c is the crop
def get_ind(i, j, c):
    if i < 0 or j < 0:
        return -1
    ind = int((i * y + j) * len(dist) + c)
    if ind >= x * y * numb_crops:
        return -1
    return ind

def get_pos(var):
    cPos = var % numb_crops
    xPos = int(((var - cPos) / numb_crops) / y)
    yPos = ((var - cPos) / numb_crops) % y

    return [xPos, yPos, cPos]

def scorePixel(field, pixel):
    score = 0
    p_x = pixel[0]
    p_y = pixel[1]
    for n1 in range(-1, 2):
        for n2 in range(-1, 2):
            # not a neighbor
            if n1 == 0 and n2 == 0:
                continue

            # test valid neighbor
            if not (0 <= (p_x + n1) and (p_x + n1) < x):
                continue
            if not (0 <= (p_y + n2) and (p_y + n2) < y):
                continue

            # update score
            numb_crops = len(dist)
            for c1 in range(numb_crops):
                for c2 in range(numb_crops):
                    indA = get_ind(p_x, p_y, c1)
                    indB = get_ind(p_x + n1, p_y + n2, c2)
                    if field[indA] > 0 and field[indB] > 0:
                        score += field[indA] * field[indB] * R(c1, c2)
    return score

def score(field):
    score = 0
    for i in range(x):
        for j in range(y):
            score += scorePixel(field, (i, j))
    return score

def print_field(x0):
    for i in range(x):
        for j in range(y):
            for c in range(numb_crops):
                if x0[get_ind(i, j, c)] > 0.1:
                    print(c, end=" ")
            print("|", end="")
        print("")


#
# END HELPERS
#

def gradient(x0):
    # calculate gradient
    grad = [0 for _ in x0]

    for var in range(len(x0)):
        pos = get_pos(var)
        s = 0
        for n1 in range(-1, 2):
            for n2 in range(-1, 2):
                for c2 in range(numb_crops):
                    ind = get_ind(pos[0] + n1, pos[1] + n2, c2)
                    if ind >= 0:
                        s += (R(pos[2], c2) + R(c2, pos[2])) * x0[ind]
        grad[var] = s

    return project(grad)

def iteration(x0, step):
    
    grad = gradient(x0)
    new_x0 = []

    for i in range(len(x0)):
        new_x0.append(x0[i] * (1.0 - step) + grad[i] * step)

    return new_x0

def project(field):
    # put field into birkhoff form
    birkhoff_field = []
    for i in range(x):
        for j in range(y):
            pixel = [0.0 for _ in range(sum(dist))]
            for c in range(len(dist)):
                crop_amount = field[get_ind(i, j, c)]
                
                offset = 0
                for a in range(c):
                    offset += dist[a]
                
                for a in range(dist[c]):
                    pixel[a + offset] = crop_amount / dist[c]

            birkhoff_field += pixel

    # project
    birkhoff_polytope = Birkhoff()
    projected = birkhoff_polytope.Euclidean_project(birkhoff_field)

    # put field back into standard form
    projected_standard_field = []
    for i in range(x):
        for j in range(y):
            pixel = [0.0 for _ in dist]
            crop = 0
            for c in range(sum(dist)):
                while c >= sum(dist[0:(crop+1)]):
                    crop += 1
                pixel[crop] += projected[c + sum(dist) * (i * y + j)]
            
            projected_standard_field += pixel



    return projected_standard_field

def gradient_ascent():

    best_x0_score = 687.0

    while True:
        # generate random 1d field
        field_stretched = []
        for i, d in enumerate(dist):
            field_stretched += [i] * d
        random.shuffle(field_stretched)

        # generate x0 vector
        x0 = []

        for i in range(x):
            for j in range(y):
                pixel = [0 for _ in range(len(dist))]
                pixel[field_stretched[i * y + j]] = 1.0
                x0 += pixel

        x0 = project(x0)
        
        prev_x0 = []
        iterations = 0

        violations = 0

        while violations < 4:

            if prev_x0 != [] and score(prev_x0) > score(x0):
                violations += 1
            else:
                violations = 0

            prev_x0 = copy.deepcopy(x0)
            x0 = iteration(x0, eps)
            iterations += 1


            if score(x0) > best_x0_score:
                best_x0 = copy.deepcopy(x0)
                best_x0_score = score(x0)

                # print("Write to file")
                # f = open("best-score.txt", "a")
                # f.write("Score: " + str(best_x0_score) + "\n x0 = " + str(best_x0) + "\n\n")
                # f.close()

        # print("Terminated")
        print(score(x0))
        # print("Relative Score: ", score(x0) / (8 * x * y - 6 * y - 6 * y + 4))
        # print(x0)


# gradient_ascent()
