import numpy as np


f = open("slurm-1305575.out", "r")


performances = []

for l in f:
    try:
        performances.append(float(l))
    except:
        pass


# find the best
print("Avg Score:", np.average(performances))
print("Std Devitaion: ", np.std(performances))
print("Number of Trials:", len(performances))