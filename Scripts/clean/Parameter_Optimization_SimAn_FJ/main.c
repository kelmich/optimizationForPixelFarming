#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <glpk.h>            

// Specify the pixel farming problem
int X = 15;
int Y = 15;
int C = 10;
double R[] = {
    -1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0,
    0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0,
    0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5,
    -0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5,
    0, 0, 0, 0, -1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0,
    0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0,
    0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1
};
int dist[] = { 23, 23, 23, 23, 23, 22, 22, 22, 22, 22 };

// specify the simulated annealing parameters
int max_same_iters = 5;
double T0 = -1;
double K = -1;
int NT = -1;
int NG = -1;

// Helper Variables
int* pixels;

// Some Helper Macros
#define R_ind(cA, cB) (((cA) + (cB) * C))
#define F_ind(x, y) (((x) + X * (y)))
#define F_ind_frac(p_id, c) (((p_id) * C + (c)))

// Some helper functions
void get_neighbors(int* res, int p) {
    int pX = p % X;
    int pY = p / X;
    int neighbors = 0;
    for(int i = -1; i < 2; i++) {
        for(int j = -1; j < 2; j++) {
            if(0 <= pX + i && pX + i < X) {
                if(0 <= pY + j && pY + j < Y) {
                    if(i == 0 && j == 0) {
                        continue;
                    }
                    neighbors++;
                    res[neighbors] = F_ind(pX + i, pY + j);
                }
            }
        }
    }
    res[0] = neighbors;
}
double energy(int* field) {
    double score = 0;
    int* neighbors = (int*) malloc(sizeof(int) * 10);
    for(int i = 0; i < X; i++) {
        for(int j = 0; j < Y; j++) {
            get_neighbors(neighbors, F_ind(i, j));
            for(int n = 1; n <= neighbors[0]; n++) {
                int cA = field[F_ind(i, j)];
                int cB = field[neighbors[n]];
                score -= R[R_ind(cA, cB)];
            }
        }
    }
    free(neighbors);
    return score;
}

double deltaEnergy(int* field, int u, int v) {

    double score = 0;
    int cU = field[u];
    int cV = field[v];

    int* neighbors = (int*) malloc(sizeof(int) * 9);
    get_neighbors(neighbors, u);
    for(int n = 1; n <= neighbors[0]; n++) {
        int cN = field[neighbors[n]];
        score += R[R_ind(cU, cN)];
        score += R[R_ind(cN, cU)];
        score -= R[R_ind(cV, cN)];
        score -= R[R_ind(cN, cV)];
    }
    get_neighbors(neighbors, v);
    for(int n = 1; n <= neighbors[0]; n++) {
        int cN = field[neighbors[n]];
        score += R[R_ind(cV, cN)];
        score += R[R_ind(cN, cV)];
        score -= R[R_ind(cU, cN)];
        score -= R[R_ind(cN, cU)];
    }
    free(neighbors);

    return score;
}

// A function to generate a random permutation of arr[] (https://www.geeksforgeeks.org/shuffle-a-given-array-using-fisher-yates-shuffle-algorithm/)
void randomize (int* arr, int n) {
    for (int i = n-1; i > 0; i--)
    {
        int j = rand() % (i+1);
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}

void pretty_print_field(int* field) {
    for(int i = 0; i < X; i++) {
        for(int j = 0; j < Y; j++) {
            printf("%i, ", field[F_ind(i, j)]);
        }
        printf("\n");
    }
    printf("Score: %f\n", energy(field));
    printf("\n");
}

void get_non_neighboring_pixels(int* res, int max_size) {

    int* neighbors = malloc(sizeof(int) * 9);

    randomize(pixels, X * Y);
    int size = 1;
    res[size] = pixels[0];
    for(int i = 0; size < max_size && i < X * Y; i++) {
        get_neighbors(neighbors, pixels[i]);
        int is_non_neighboring = 1;
        for(int j = 1; j <= neighbors[0]; j++) {
            for(int k = 1; k <= size; k++) {
                // not a neighbor or a pixel already in the set
                if(neighbors[j] == res[k] || pixels[i] == res[k]) {
                    is_non_neighboring = 0;
                }
            }
        }
        if(is_non_neighboring == 1) {
            size++;
            res[size] = pixels[i];
        }
    }
    res[0] = size;
    free(neighbors);
}

void initialize_field(int* field) {
    // initialize field
    int pos = 0;
    for(int i = 0; i < C; i++) {
        for(int j = 0; j < dist[i]; j++) {
            field[pos] = i;
            pos++;
        }
    }
    randomize(field, X * Y);
}

// The actual simulated annealing
void simulatedAnnealing(int* field) {

    // Setup Process
    double H = energy(field);
    double T = T0;
    for(int iT = 0; iT < NT; iT++) {
        for(int iG = 0; iG < NG; iG++) {

            // pick two random points
            int pA = rand() % (X * Y);
            int pB = rand() % (X * Y);
            while(field[pA] == field[pB]) {
                pB = rand() % (X * Y);
            }

            // Improve or Anneal
            double deltaH = deltaEnergy(field, pA, pB);
            double r = ((double) rand()) / ((double) RAND_MAX);
            double p = exp(-deltaH / T);
            if(deltaH <= 0 || r < p) {
                H += deltaH;
                int tmp = field[pA];
                field[pA] = field[pB];
                field[pB] = tmp;
            }
        }
        // Cool system down
        T *= K;
    }
}
// The driver for running it in parallel
int main(int argc, char** argv) {

        // MPI Initialization
        int world_size;
        int world_rank;
        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
        srand(world_rank);

        // variable initialization
        double maxScore = -660.5;
        pixels = malloc(sizeof(int) * X * Y);
        for(int i = 0; i < X * Y; i++) {
            pixels[i] = i;
        }
        
        // while(1) {

            // initialize a fractional field
            int* field = (int*) malloc(sizeof(int) * X * Y);
            int* field_prev = (int*) malloc(sizeof(int) * X * Y);

            int parameter_set_id = 0;

            // Iterate through all possible parameters we want to try
            for(double t0 = 5; t0 <= 25; t0 += 5) {
                for(double k = 0.8; k <= 0.995; k += 0.02) {
                    for(int nt = 2000; nt <= 6000; nt += 500) {
                        for(int ng = 500; ng <= 2500; ng += 500) {
                            for(int trial = 0; trial < 3; trial++) {

                                initialize_field(field);
                   
                                T0 = t0;
                                K = k;
                                NT = nt;
                                NG = ng;


                                // iterate on the best solution a node has until it converges
                                int same_iters = 0;
                                // printf("Initial Field: %f\n", energy(frac_field));
                                double prev_score = 0;
                                while(same_iters < max_same_iters) {

                                    // housekeeping
                                    same_iters++;
                                    memcpy(field_prev, field, sizeof(int) * X * Y);

                                    // iterate
                                    simulatedAnnealing(field);

                                    // inform the other nodes of the best current solution
                                    double s = energy(field);
                                    double s_prev = energy(field_prev);
                                    int* best_sol;
                                    if(s < s_prev) {
                                        best_sol = field;
                                    } else {
                                        best_sol = field_prev;
                                    }
                                    MPI_Barrier(MPI_COMM_WORLD);
                                    
                                    // find the process with the max score
                                    int* rbuf;
                                    rbuf = (int *) malloc(world_size * X * Y * sizeof(int)); 
                                    MPI_Allgather( best_sol, X * Y, MPI_INT, rbuf, X * Y, MPI_INT, MPI_COMM_WORLD);
                                    MPI_Barrier(MPI_COMM_WORLD);
                                    int* best_round_sol = rbuf;
                                    double x = energy(best_round_sol);
                                    for(int i = 0; i < world_size; i++) {
                                        double y = energy(rbuf + i * X * Y);
                                        if(y < x) {
                                            x = y;
                                            best_round_sol = rbuf + i * X * Y;
                                        }
                                    }
                                    memcpy(field, best_round_sol, X * Y * sizeof(int));
                                    if(energy(field) < prev_score) {
                                        same_iters = 0;
                                        prev_score = energy(field);
                                    }
                                    free(rbuf);
                                }

                                // print the converged solution if it's any good
                                double s = energy(field);
                                if(world_rank == 0) {
                                    if(s < maxScore) {
                                        maxScore = s;
                                        pretty_print_field(field);
                                    }
                                    // parameter_id, score, maxscore, 
                                    printf("PARAMETER_ID: %i, Score: %f, Max Score: %f, T0: %f, K: %f, NT: %i, NG: %i\n", parameter_set_id, s, maxScore, T0, K, NT, NG);
                                }


                            }

                            parameter_set_id++;

                        }
                    }
                }
            }
            // cleanup
            free(field);
            free(field_prev);
        // }

        // cleanup
        free(pixels);

        // MPI Termination
        MPI_Finalize();
}
