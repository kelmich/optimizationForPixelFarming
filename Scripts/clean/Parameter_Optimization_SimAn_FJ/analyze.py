

f = open("euler.txt", "r")

curr_case = -1
s = 999999999

performances = []

for l in f:

    if not "PARAMETER_ID" in l:
        continue

    # read in line
    line = l.split(",")
    param_id = int(line[0].split(":")[1])
    score = float(line[1].split(":")[1])
    maxscore = float(line[2].split(":")[1])
    T0 = float(line[3].split(":")[1])
    K = float(line[4].split(":")[1])
    nt = int(line[5].split(":")[1])
    ng = int(line[6].split(":")[1])

    # calculate the avg
    if curr_case != param_id:
        performances.append([s / 3, param_id, T0, K, nt, ng])
        curr_case = param_id
        s = 0
     
    s += score


# find the best
performances = sorted(performances,key=lambda x: (x[0]))
for i in range(30):
    print(performances[i])
