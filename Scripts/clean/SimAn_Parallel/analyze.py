import numpy as np

f = open("euler-2.txt", "r")

performances = []
for l in f:

    if not l.count(":") == 1:
        continue

    # read in line
    try:
        line = l.split(":")[1]
        performances.append(float(line))
    except:
        i = 0  


# find the best
print("Avg Score:", np.average(performances))
print("Std Devitaion: ", np.std(performances))
print("Number of Trials:", len(performances))