#include <math.h>

double maxScore(int x, int y) {
    return (double) (8 * x * y - 6 * x - 6 * y + 4);
}

double maxScoreGaertner(int *field, int x, int y, double *R, int crops) {
    double score = 0.0;

    // find crop distribution
    int dist[crops];
    for(int i = 0; i < crops; i++) {
        dist[i] = 0;
    }
    for(int i = 0; i < x*y; i++) {
        dist[field[i]]++;
    }

    // iterate through all options
    int clock[9];
    for(int i = 0; i < 9; i++) {
        clock[i] = 0;
    }

    // save max scores for each crop
    double maxCropScore[crops];
    for(int i = 0; i < crops; i++) {
        maxCropScore[i] = -1000.0;
    }

    int iteration = 0;

    while(clock[8] != crops) {

        // calculate score with this neighborhood
        double tmpScore = 0.0;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 2; j++) {

                // Div by 4 or 6?
                // Div by 4 => 1186.750000
                // Div by 6 => 986.333333

                tmpScore += R[clock[i * 3 + j] * crops + clock[i * 3 + j+1]] / 6;
                tmpScore += R[clock[i * 3 + j+1] * crops + clock[i * 3 + j]] / 6;

                tmpScore += R[clock[j * 3 + i] * crops + clock[(j+1) * 3 + i]] / 6;
                tmpScore += R[clock[(j+1) * 3 + i] * crops + clock[j * 3 + i]] / 6;
            }
        }
        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                tmpScore += R[clock[i * 3 + j] * crops +  clock[(i+1) * 3 + j+1]] / 4;
                tmpScore += R[clock[(i+1) * 3 + j+1] * crops +  clock[i * 3 + j]] / 4;

                tmpScore += R[clock[i * 3 + j+1] * crops +  clock[(i+1) * 3 + j]] / 4;
                tmpScore += R[clock[(i+1) * 3 + j] * crops +  clock[i * 3 + j+1]] / 4;
            }
        }

        if(tmpScore > maxCropScore[clock[4]]) {
            maxCropScore[clock[4]] = tmpScore;
        }

        // tick clock
        clock[0]++;
        int i = 0;
        while(i+1 < 9 && clock[i] == crops) {
            clock[i] = 0;
            clock[i+1]++;
            i++;
        }
        if(iteration % 10000000 == 0) {
            printf("%i %i %i %i\n", clock[8], clock[7], clock[6], clock[5]);
        }
        iteration++;
    }

    for(int i = 0; i < crops; i++) {
        printf("%lf\n", maxCropScore[i]);
        score += maxCropScore[i] * ((double) dist[i]);
    }

    return score;
}

double scorePixel(int *field, int x, int y, int xP, int yP, double *R, int crops)
{
    double score = 0.0;
    for (int n1 = -1; n1 < 2; n1++)
    {
        for (int n2 = -1; n2 < 2; n2++)
        {
            // this is me
            if (n1 == 0 && n2 == 0)
            {
                continue;
            }

            // test if valid neighbor
            if (!(0 <= (xP + n1) && (xP + n1) < x))
            {
                continue;
            }
            if (!(0 <= (yP + n2) && (yP + n2) < y))
            {
                continue;
            }
            int a = field[xP * y + yP];
            int b = field[(xP + n1) * y + (yP + n2)];
            score += R[a * crops + b];
        }
    }
    return score;
}

double scorer(int *field, int x, int y, double *R, int crops)
{
    double score = 0.0;
    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            score += scorePixel(field, x, y, i, j, R, crops);
        }
    }
    return score;
}