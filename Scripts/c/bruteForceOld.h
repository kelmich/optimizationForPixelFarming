#include <limits.h>
#include <memory.h>
#include <math.h>
#include "scorer.h"

void recSolve(int *field, int *maxField, int iter, int x, int y, double *R, int *dist, int n)
{
    // set vars
    double maxScore = -8 * x * y;


    // find available slots
    int availableSlots = 0;
    for (int i = iter; i < n; i++)
    {
        availableSlots += dist[i];
    }

    // define iteration mechanism
    int clock[dist[iter]];
    for (int i = 0; i < dist[iter]; i++)
    {
        clock[i] = i;
    }

    // iterate through all options
    while (clock[dist[iter] - 1] != availableSlots)
    {

        // set field
        int pos = 0;
        int count = 0;
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                if (field[i * y + j] == -1)
                {
                    if (pos < dist[iter] && count == clock[pos])
                    {
                        field[i * y + j] = iter;
                        pos++;
                    }
                    count++;
                }
            }
        }

        // recurse
        if (iter + 1 < n)
        {
            recSolve(field, maxField, iter + 1, x, y, R, dist, n);
            double score = scorer(maxField, x, y, R);
            if (score > maxScore)
            {
                memcpy(maxField, field, x * y * sizeof(int));

            }
        }
        else
        {
            return;
        }

        // reset field
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                int v = field[i * y + j];
                if (v >= iter)
                {
                    field[i * y + j] = -1;
                }
            }
        }

        // update clock value
        int smallestPos = 0;
        while (smallestPos < dist[iter] - 1 && clock[smallestPos] + 1 == clock[smallestPos + 1])
        {
            smallestPos++;
        }
        clock[smallestPos]++;

        // reset bottom part of clock
        for (int i = 0; i < smallestPos; i++)
        {
            clock[i] = i;
        }
    }

}

int *solve(int x, int y, double *R, int *dist, int n)
{

    // initialize a field
    int *field = (int*) malloc(sizeof(int) * x * y);
    for (int i = 0; i < x * y; i++)
    {
            field[i] = -1;
    }

    int *maxField = (int*) malloc(sizeof(int) * x * y);
    memcpy(maxField, field, x * y * sizeof(int));

    recSolve(field, maxField, 0, x, y, R, dist, n);

    free(field);

    return maxField;
}