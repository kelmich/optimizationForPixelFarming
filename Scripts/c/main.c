#include <stdio.h>
#include <stdlib.h>
//#include "bruteForceOld.h"
#include "bruteForce.h"

int main()
{

    // Problems

    // char *name = "Cotrini Data (No Kreuter)";
    // int x = 5;
    // int y = 2;
    // int crops = 10;
    // double R[10][10] = {
    //     {-0.5, 1, 1, 1, 1, 1, -1, 0.5, 0.5, 1},
    //     {1, -0.5, -1, 1, 1, 0.5, -1, 1, 1, 1},
    //     {1, -1, -0.5, -1, 1, 0, 0.5, 0, 0, 0},
    //     {1, 1, -1, -0.5, 0, 0, -1, 1, -1, 0},
    //     {1, 1, 1, 0, -0.5, 0, 1, 1, 1, 1},
    //     {1, 0.5, 0, 0, 0, -0.5, 0, 0, 0, 0},
    //     {-1, -1, 0.5, -1, 1, 0, -0.5, 0, 0.5, 0},
    //     {0.5, 1, 0, 1, 1, 0, 0, -0.5, 1, 0},
    //     {0.5, 1, 0, -1, 1, 0, 0.5, 1, -0.5, 1},
    //     {1, 1, 0, 0, 1, 0, 0, 0, 1, -0.5}};
    // int distances[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    // char *name = "NONNEGATIVE Cotrini Data (No Kreuter)";
    // int x = 15;
    // int y = 15;
    // int crops = 10;
    // double R[10][10] = {
    //     {0, 1, 1, 1, 1, 1, 0, 0.5, 0.5, 1},
    //     {1, 0, 0, 1, 1, 0.5, 0, 1, 1, 1},
    //     {1, 0, 0, 0, 1, 0, 0.5, 0, 0, 0},
    //     {1, 1, 0, 0, 0, 0, 0, 1, 0, 0},
    //     {1, 1, 1, 0, 0, 0, 1, 1, 1, 1},
    //     {1, 0.5, 0, 0, 0, 0, 0, 0, 0, 0},
    //     {0, 0, 0.5, 0, 1, 0, 0, 0, 0.5, 0},
    //     {0.5, 1, 0, 1, 1, 0, 0, 0, 1, 0},
    //     {0.5, 1, 0, 0, 1, 0, 0.5, 1, 0, 1},
    //     {1, 1, 0, 0, 1, 0, 0, 0, 1, 0}};
    // int distances[] = {23, 23, 23, 23, 23, 22, 22, 22, 22, 22};

    char *name = "Agropol Data";
    int x = 3;
    int y = 3;
    int crops = 10;
    double R[10][10] = {
        {-1, 0, 0.5, -0.5, 1, 0, 0.5, 1, 0.5, 0},
        {0, -1, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0},
        {0.5, 0, -1, 0, 0, 0, 0.5, 0.5, 0, -0.5},
        {-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5},
        {0, 0, 0, 0, -1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, -1, 1, 0.5, 0, 0},
        {0.5, 0.5, 0.5, -0.5, 0, 0.5, -1, 0.5, 0.5, 0.5},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, -1, 0},
        {0, 0, -0.5, 0.5, 0, 0, 1, 0.5, 0.5, -1}};
    int distances[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 0};

    // char *name = "Arnold (CAUTION, MATRIX DIFFERENT)";
    // int x = 15;
    // int y = 15;
    // int crops = 10;
    // double R[10][10] = {
    //     {-1, 0, 0.5, -0.5, 0, 1, 0.5, 1, 0, 0.5},
    //     {0, -1, 0, 1, 0, 0, 0.5, 0.5, 0, 0.5},
    //     {0.5, 0, -1, 0, 0, 0, 0.5, 0.5, -0.5, 0},
    //     {-0.5, 1, 0, -1, 0, 0, -1, 0.5, 0.5, 0.5},
    //     {0, 0, 0, 0, -1, 0, 1, 0.5, 0, 0},
    //     {0, 0, 0, 0, 0, -1, 0, 0, 0, 0},
    //     {0.5, 0.5, 0.5, -0.5, 0.5, 0, -1, 0.5, 0.5, 0.5},
    //     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    //     {0, 0, -0.5, 0.5, 0, 0, 1, 0.5, -1, 0.5},
    //     {0.5, 0.5, 0, 1, 0, 0, 0.5, 0.5, 0, -1}};
    // int distances[] = {23, 23, 23, 23, 23, 22, 22, 22, 22, 22};

    // make the datatypes work
    double *r = (double *)malloc(sizeof(double) * crops * crops);
    for (int i = 0; i < crops; i++)
    {
        for (int j = 0; j < crops; j++)
        {
            r[i * crops + j] = R[i][j];
        }
    }
    int *dist = malloc(sizeof(double) * crops);
    for (int i = 0; i < crops; i++)
    {
        dist[i] = distances[i];
    }

    // build example field
    int *field = malloc(sizeof(int) * x * y);
    int counter = 0;
    int crop = 0;
    for(int i = 0; i < x * y; i++) {
        field[i] = crop;
        counter++;
        if(counter == dist[crop]) {
            counter = 0;
            crop++;
        }
    }

    // double maxScoreCalc = maxScoreGaertner(field, x, y, r, crops);
    // printf("maxScoreGaertner is: %lf\n", maxScoreCalc);
    // printf("regular maxscore is: %lf\n", maxScore(x, y));

    printf("Started Solving...\n");
    int *sol = solve(x, y, r, dist, crops);
    // int* sol = malloc(sizeof(int) * x * y);

    // sol[0 * y + 0] = 5;
    // sol[0 * y + 1] = 4;
    // sol[0 * y + 2] = 5;
    // sol[0 * y + 3] = 4;
    // sol[0 * y + 4] = 5;
    // sol[0 * y + 5] = 4;
    // sol[0 * y + 6] = 5;
    // sol[0 * y + 7] = 4;
    // sol[0 * y + 8] = 5;
    // sol[0 * y + 9] = 4;
    // sol[0 * y + 10] = 5;
    // sol[0 * y + 11] = 4;
    // sol[0 * y + 12] = 5;
    // sol[0 * y + 13] = 4;
    // sol[0 * y + 14] = 5;

    // sol[1 * y + 0] = 6;
    // sol[1 * y + 1] = 0;
    // sol[1 * y + 2] = 6;
    // sol[1 * y + 3] = 0;
    // sol[1 * y + 4] = 6;
    // sol[1 * y + 5] = 0;
    // sol[1 * y + 6] = 6;
    // sol[1 * y + 7] = 0;
    // sol[1 * y + 8] = 6;
    // sol[1 * y + 9] = 0;
    // sol[1 * y + 10] = 6;
    // sol[1 * y + 11] = 0;
    // sol[1 * y + 12] = 6;
    // sol[1 * y + 13] = 0;
    // sol[1 * y + 14] = 7;

    // sol[2 * y + 0] = 2;
    // sol[2 * y + 1] = 4;
    // sol[2 * y + 2] = 2;
    // sol[2 * y + 3] = 4;
    // sol[2 * y + 4] = 2;
    // sol[2 * y + 5] = 4;
    // sol[2 * y + 6] = 2;
    // sol[2 * y + 7] = 4;
    // sol[2 * y + 8] = 2;
    // sol[2 * y + 9] = 4;
    // sol[2 * y + 10] = 2;
    // sol[2 * y + 11] = 4;
    // sol[2 * y + 12] = 2;
    // sol[2 * y + 13] = 4;
    // sol[2 * y + 14] = 5;

    // sol[3 * y + 0] = 6;
    // sol[3 * y + 1] = 0;
    // sol[3 * y + 2] = 6;
    // sol[3 * y + 3] = 0;
    // sol[3 * y + 4] = 6;
    // sol[3 * y + 5] = 0;
    // sol[3 * y + 6] = 6;
    // sol[3 * y + 7] = 0;
    // sol[3 * y + 8] = 6;
    // sol[3 * y + 9] = 0;
    // sol[3 * y + 10] = 6;
    // sol[3 * y + 11] = 0;
    // sol[3 * y + 12] = 6;
    // sol[3 * y + 13] = 0;
    // sol[3 * y + 14] = 7;

    // sol[4 * y + 0] = 2;
    // sol[4 * y + 1] = 4;
    // sol[4 * y + 2] = 2;
    // sol[4 * y + 3] = 4;
    // sol[4 * y + 4] = 2;
    // sol[4 * y + 5] = 4;
    // sol[4 * y + 6] = 2;
    // sol[4 * y + 7] = 4;
    // sol[4 * y + 8] = 2;
    // sol[4 * y + 9] = 4;
    // sol[4 * y + 10] = 2;
    // sol[4 * y + 11] = 4;
    // sol[4 * y + 12] = 2;
    // sol[4 * y + 13] = 4;
    // sol[4 * y + 14] = 5;

    // sol[5 * y + 0] = 6;
    // sol[5 * y + 1] = 7;
    // sol[5 * y + 2] = 6;
    // sol[5 * y + 3] = 7;
    // sol[5 * y + 4] = 6;
    // sol[5 * y + 5] = 7;
    // sol[5 * y + 6] = 6;
    // sol[5 * y + 7] = 7;
    // sol[5 * y + 8] = 6;
    // sol[5 * y + 9] = 7;
    // sol[5 * y + 10] = 6;
    // sol[5 * y + 11] = 0;
    // sol[5 * y + 12] = 6;
    // sol[5 * y + 13] = 0;
    // sol[5 * y + 14] = 7;

    // sol[6 * y + 0] = 4;
    // sol[6 * y + 1] = 8;
    // sol[6 * y + 2] = 1;
    // sol[6 * y + 3] = 8;
    // sol[6 * y + 4] = 1;
    // sol[6 * y + 5] = 8;
    // sol[6 * y + 6] = 1;
    // sol[6 * y + 7] = 8;
    // sol[6 * y + 8] = 1;
    // sol[6 * y + 9] = 8;
    // sol[6 * y + 10] = 9;
    // sol[6 * y + 11] = 2;
    // sol[6 * y + 12] = 5;
    // sol[6 * y + 13] = 2;
    // sol[6 * y + 14] = 5;

    // sol[7 * y + 0] = 7;
    // sol[7 * y + 1] = 3;
    // sol[7 * y + 2] = 9;
    // sol[7 * y + 3] = 3;
    // sol[7 * y + 4] = 9;
    // sol[7 * y + 5] = 3;
    // sol[7 * y + 6] = 9;
    // sol[7 * y + 7] = 3;
    // sol[7 * y + 8] = 9;
    // sol[7 * y + 9] = 3;
    // sol[7 * y + 10] = 7;
    // sol[7 * y + 11] = 0;
    // sol[7 * y + 12] = 7;
    // sol[7 * y + 13] = 0;
    // sol[7 * y + 14] = 7;

    // sol[8 * y + 0] = 2;
    // sol[8 * y + 1] = 8;
    // sol[8 * y + 2] = 2;
    // sol[8 * y + 3] = 8;
    // sol[8 * y + 4] = 2;
    // sol[8 * y + 5] = 8;
    // sol[8 * y + 6] = 2;
    // sol[8 * y + 7] = 8;
    // sol[8 * y + 8] = 2;
    // sol[8 * y + 9] = 8;
    // sol[8 * y + 10] = 9;
    // sol[8 * y + 11] = 2;
    // sol[8 * y + 12] = 5;
    // sol[8 * y + 13] = 2;
    // sol[8 * y + 14] = 5;

    // sol[9 * y + 0] = 9;
    // sol[9 * y + 1] = 3;
    // sol[9 * y + 2] = 9;
    // sol[9 * y + 3] = 3;
    // sol[9 * y + 4] = 9;
    // sol[9 * y + 5] = 3;
    // sol[9 * y + 6] = 9;
    // sol[9 * y + 7] = 3;
    // sol[9 * y + 8] = 9;
    // sol[9 * y + 9] = 3;
    // sol[9 * y + 10] = 7;
    // sol[9 * y + 11] = 0;
    // sol[9 * y + 12] = 7;
    // sol[9 * y + 13] = 0;
    // sol[9 * y + 14] = 7;

    // sol[10 * y + 0] = 1;
    // sol[10 * y + 1] = 8;
    // sol[10 * y + 2] = 1;
    // sol[10 * y + 3] = 8;
    // sol[10 * y + 4] = 1;
    // sol[10 * y + 5] = 8;
    // sol[10 * y + 6] = 1;
    // sol[10 * y + 7] = 8;
    // sol[10 * y + 8] = 1;
    // sol[10 * y + 9] = 8;
    // sol[10 * y + 10] = 9;
    // sol[10 * y + 11] = 2;
    // sol[10 * y + 12] = 5;
    // sol[10 * y + 13] = 2;
    // sol[10 * y + 14] = 5;

    // sol[11 * y + 0] = 9;
    // sol[11 * y + 1] = 3;
    // sol[11 * y + 2] = 9;
    // sol[11 * y + 3] = 3;
    // sol[11 * y + 4] = 9;
    // sol[11 * y + 5] = 3;
    // sol[11 * y + 6] = 9;
    // sol[11 * y + 7] = 3;
    // sol[11 * y + 8] = 9;
    // sol[11 * y + 9] = 3;
    // sol[11 * y + 10] = 7;
    // sol[11 * y + 11] = 0;
    // sol[11 * y + 12] = 7;
    // sol[11 * y + 13] = 0;
    // sol[11 * y + 14] = 7;

    // sol[12 * y + 0] = 1;
    // sol[12 * y + 1] = 8;
    // sol[12 * y + 2] = 1;
    // sol[12 * y + 3] = 8;
    // sol[12 * y + 4] = 1;
    // sol[12 * y + 5] = 8;
    // sol[12 * y + 6] = 1;
    // sol[12 * y + 7] = 8;
    // sol[12 * y + 8] = 1;
    // sol[12 * y + 9] = 8;
    // sol[12 * y + 10] = 9;
    // sol[12 * y + 11] = 2;
    // sol[12 * y + 12] = 5;
    // sol[12 * y + 13] = 2;
    // sol[12 * y + 14] = 5;

    // sol[13 * y + 0] = 7;
    // sol[13 * y + 1] = 3;
    // sol[13 * y + 2] = 9;
    // sol[13 * y + 3] = 3;
    // sol[13 * y + 4] = 9;
    // sol[13 * y + 5] = 3;
    // sol[13 * y + 6] = 9;
    // sol[13 * y + 7] = 3;
    // sol[13 * y + 8] = 9;
    // sol[13 * y + 9] = 3;
    // sol[13 * y + 10] = 7;
    // sol[13 * y + 11] = 0;
    // sol[13 * y + 12] = 6;
    // sol[13 * y + 13] = 0;
    // sol[13 * y + 14] = 7;

    // sol[14 * y + 0] = 5;
    // sol[14 * y + 1] = 8;
    // sol[14 * y + 2] = 1;
    // sol[14 * y + 3] = 3;
    // sol[14 * y + 4] = 1;
    // sol[14 * y + 5] = 3;
    // sol[14 * y + 6] = 1;
    // sol[14 * y + 7] = 3;
    // sol[14 * y + 8] = 1;
    // sol[14 * y + 9] = 8;
    // sol[14 * y + 10] = 5;
    // sol[14 * y + 11] = 4;
    // sol[14 * y + 12] = 5;
    // sol[14 * y + 13] = 2;
    // sol[14 * y + 14] = 5;


    printf("Solution for %s:\n", name);
    printField(sol, x, y);

    double score = scorer(sol, x, y, r, crops);
    printf("Score: %lf, Relative Score: %lf", score, score / maxScore(x, y));

    

    // cleanup allocated memory
    free(r);
    free(sol);
    free(dist);
    free(field);
}