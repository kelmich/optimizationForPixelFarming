#include <limits.h>
#include <memory.h>
#include <stdio.h>
#include "scorer.h"
#include "helpers.h"

// returns the delta of the score
double JohnsonTrotterIter(int *field, int x, int y, int *direction, double *R, int crops)
{

    // see https://www.geeksforgeeks.org/johnson-trotter-algorithm/

    // find largest mobile integer
    int n = x * y;
    int lmi = -1;
    int lmiIndex = -1;
    for (int i = 0; i < n; i++)
    {
        int neighbor = i + direction[i];
        if (0 <= neighbor && neighbor < n)
        {
            if (field[i] > field[neighbor] && field[i] > lmi)
            {
                lmi = field[i];
                lmiIndex = i;
            }
        }
    }

    // prescore
    int neighborIndex = lmiIndex + direction[lmiIndex];
    double score = 0.0;
    score -= 2 * scorePixel(field, x, y, lmiIndex / y, lmiIndex % y, R, crops);
    score -= 2 * scorePixel(field, x, y, neighborIndex / y, neighborIndex % y, R, crops);
    
    // swap lmi
    double tmp = direction[lmiIndex];
    direction[lmiIndex] = direction[neighborIndex];
    direction[neighborIndex] = tmp;
    field[lmiIndex] = field[neighborIndex];
    field[neighborIndex] = lmi;

    // postscore
    score += 2 * scorePixel(field, x, y, lmiIndex / y, lmiIndex % y, R, crops);
    score += 2 * scorePixel(field, x, y, neighborIndex / y, neighborIndex % y, R, crops);

    // switch direction of all int larger than lmi
    for (int i = 0; i < n; i++)
    {
        if (field[i] > lmi)
        {
            direction[i] *= -1;
        }
    }

    return score;
}

// NO DUPLICATES!!!!
int *solve(int x, int y, double *R, int *dist, int crops)
{
    // initialize a field
    int *field = (int *)malloc(sizeof(int) * x * y);
    int cropType = 0;
    int plantedCrop = 0;
    for (int i = 0; i < x * y; i++)
    {
        while (dist[cropType] - plantedCrop == 0)
        {
            cropType++;
            plantedCrop = 0;
        }
        field[i] = cropType;
        plantedCrop++;
    }

    // initialize direction differences, i.e. points left <=> -1, point right <=> +1
    int *direction = (int *) malloc(sizeof(int) * x * y);
    for (int i = 0; i < x * y; i++)
    {
        direction[i] = -1;
    }

    // initialize the maximum field
    int *maxField = (int *)malloc(sizeof(int) * x * y);
    memcpy(maxField, field, x * y * sizeof(int));

    // iterate through all options using Johnson & Trotter
    unsigned long iterations = 1;
    for (int i = 1; i <= x * y; i++)
    {
        iterations *= (unsigned long)i;
    }

    double score = scorer(field, x, y, R, crops);
    double maxScore = score;
    int percent = 0;
    for (unsigned long i = 0; i < iterations-1; i++)
    {
        // if (iterations > 1000 && i % (iterations / 1000) == 0)
        // {
        //     printf("%lf\% Done\n", ((double )percent)/10);
        //     percent++;
        // }
        score += JohnsonTrotterIter(field, x, y, direction, R, crops);


        // test if combo with opt sol is good
        // double comboScore = multi_score([1, 8, 6, 0, 7, 3, 9, 2, 5, 4], field);
        if(score == 14.5) {
            printField(field, x, y);
        }

        
        if (score > maxScore)
        {
            maxScore = score;
            memcpy(maxField, field, x * y * sizeof(int));
        }
    }

    return maxField;
}