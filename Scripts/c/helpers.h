void printField(int *field, int x, int y)
{
    for (int j = 0; j < y; j++)
    {
        for (int i = 0; i < x; i++)
        {
            printf("%i ", field[i * y + j]);
        }
        printf("\n");
    }
    printf("\n\n");
}