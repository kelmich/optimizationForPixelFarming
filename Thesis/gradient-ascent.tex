\chapter{Gradient Ascent}

Gradient ascent is a method
for searching through large search spaces.
When given a point
in the solution space, we find the locally
most beneficial direction to move
in order to increase the score. In a
second step we move slightly in this direction.
That process is repeated until we converge
to a solution.

We know from the auxiliary Theorems \ref{makeInt1} and
\ref{makeInt2} that we can build almost entirely integer
fields from fractional fields.
Hence we can use gradient ascent
over the solution space consisting of all
fractional solutions. This is natural
as it allows for adding and removing
small amounts of crop from a pixel, an important
feature of the small step size gradient ascent has.

There are now two remaining issues we need
to tackle. The first is computing the gradient.
The second is that after following the gradient
we might no longer be within the valid solution
space. An example would
be the gradient suggesting we plant more than
one unit of crop per pixel to achieve a higher score.
While this would be beneficial to the score,
it no longer represents a valid fractional solution
for our problem. Thus we may need to find the closest
valid solution. In the following sections
we tackle these issues.

\section{Finding the closest valid solution}

Again the Birkhoff Polytope offers a solution.
There are methods for finding points within
the Birkhoff Polytope that have a minimal
Euclidian distance to any arbitrary point \cite{https://doi.org/10.48550/arxiv.1910.11369}.
Hence, if we could represent all valid fractional
solutions within a Birkhoff Polytope we could use these
methods.

The core idea is that we expand the fractional
solution representation slightly to get
a Birkhoff polytope. Usually for every pixel we
have $C$ variables, which would lead to a $(X \cdot Y) \times C$
matrix. However, for the  Birkhoff polytope we need
a square matrix where both all rows and all columns sum to $1$.
To achieve this we ``clone'' crops. That is, if crop $c$ should
be represented $n$ times within the field, then we introduce
$n$ crops, that all behave identically to $c$,
which all should be represented once within the field.
As the total amount of crops to be planted
is the same as the number of pixels, we must get
a square $(X \cdot Y) \times (X \cdot Y)$ matrix.
This process should be familiar from the proof
of Theorem \ref*{thm:multiswap}.

Every matrix of that form within the corresponding
Birkhoff Polytope is a valid
fractional solution, as every pixel contains exactly
a total amount of $1$ planted crops (rows
sum to $1$) and we have one of each crop (columns sum to $1$).
As we have one of each crop and some crops behave exactly
the same, we can group these together to obtain
the original fractional solution space we started out with.
Hence every element of our Birkhoff Polytope maps
to a valid fractional solution. This means we can
project any $(X \cdot Y) \times (X \cdot Y)$ matrix
to the closest valid fractional solution.

Finally we compute the gradient by taking
the partial derivative of the following
function where variables of the from $p_c$ denote
the amount of crop $c$ contained in pixel $p$:
\begin{align*}
    \frac{\partial}{\partial p_c} \sum_{n \in N(p)} \sum_{c_i \in \Cps} R(c, c_i) \cdot p_{c} \cdot n_{c_j}
    = \sum_{n \in N(p)} \sum_{c_i \in \Cps} R(c, c_i) \cdot n_{c_j}
\end{align*}

\section{Method}

Using these elements we now build the following algorithm:
\FloatBarrier
\begin{algorithm}
    \caption{Random gradient ascent}
    \begin{algorithmic}
        \State $F \gets randomField()$
        \While{$F$ not converged}
            \State $G \gets gradient(F)$
            \State $F \gets F + \text{stepsize} \cdot G$
            \State $F \gets project(F)$
        \EndWhile\\
        \Return $integerize(F)$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
The only hyperparameter we have to tune is the stepsize.
We tried the following step sizes: $\{0.001, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0\}$
for ten iterations each. As we achieved the best average result with $0.2$
the following benchmark on the euler cluster was run with $0.2$ as the step size:
\FloatBarrier
\begin{figure}[h]
    \centering
    \begin{tabular}{@{}lccc@{}} \toprule
        Method & Average Score & Standard Deviation & Iterations\\ \midrule
        Simulated Annealing & $664.1$ & $6.1$ & $7509821$\\ 
        Gradient Ascent & $572.4$ & $15.9$ & $9854$\\ 
        \bottomrule
    \end{tabular}
\end{figure}
\FloatBarrier
where we can see that this method unfortunately does not
perform as well as simulated annealing. We note that
these results are obtained without the integerization
step to ensure that making the fractional solution
an integer solution was not a bottleneck on the score. We suspect
that the reason for this unimpressive performance is
that the method often gets caught in local maxima.

Lastly we note that,
in contrast to the other algorithms we present, this algorithm
was implemented in python. The reason for this is that the library
we used for projecting onto the Birkhoff polytope is written in python.