\chapter{Related Work}

To our knowledge, the only people to have worked on the optimization
problem are Helfenstein and Edlinger from Agroscope and
Arnold from the University of Bern. Arnold was kind
enough to share some of the progress he made
with us \cite{arnold-paper}. As Agroscope used Arnolds
methods for a first round of their real world experiment, we will
focus on his work in this section. We summarize the main two methods
he developed below.

The first method is hill climbing. Here
Arnold proposes a method where the following
steps are repeated many times. We start out
with an initial field configuration.
We note that every field configuration can be assigned a score.
Then we:
\begin{enumerate}
    \item Propose a new random field configuration based on the current field
    \item $\Delta H = \text{Score change caused by switching to the proposed configuration}$
    \item If $\Delta H > 0$ switch to the proposed configuration,
    otherwise remain with the current configuration.
    \item Go back to step 1
\end{enumerate}
The program terminates when we have reached the maximum number
of iterations. Because of the way new field configurations
are proposed based on the current field, the method
sometimes gets caught in a local optimum. From this
local optimum the method can propose no new configurations
that improve the score. In Arnolds experiments
this method often get caught in local optima that
were far from other better solutions. A possible
approach for tackling this issue is to rerun the
algorithm with different initial field configurations.
In practice this is unfortunately not enough to
fix the issue on large fields.

The second method is simulated annealing. This
method is similar to the hill climbing approach
with one key difference: we sometimes accept
configurations that are worse with a certain
probability $p$. The process works as follows.
Pick an initial field configuration. 
Then we have two loops, an outer and an inner
loop. After every iteration $i$ of the outer loop
the system is ``cooled''. This is done by multiplying
the system temperature with a constant $K$ smaller
than $1$. Then we try the following procedure
in the inner loop:
\begin{enumerate}
    \item Propose a new random field configuration based on the current field
    \item $\Delta H = \text{Score change caused by switching to the proposed configuration}$
    \item Calculate the current probability $p$ (see below)
    \item If changing to the new configuration imporves the score we
    switch our current field to it,
    otherwise maybe switch our current field to the proposed configuration
    with probability $p$
    \item Go back to step 1
\end{enumerate}
where the current swap probability is determined by
a Boltzmann distribution:
\begin{align*}
    T_i &= T_0 \cdot K^i\\
    p &= e^{-\Delta H / T_i}
\end{align*}
where $T_0$ and $K$
are constants Arnold determines experimentally. The smaller
$K$ is, the faster the method converges. The idea
with this method is that the algorithm is allowed
to escape local optima in the early stages of
the algorithm. If the algorithm accepts a change
that makes the score worse, this could lead to it being
"unstuck" from a local optimum it might be in
and find a solution closer to the global or even
the global optimum.

The simulated annealing approach appears
to work quite well. It outperforms the
hill climbing approach
significantly. We will elaborate
further on the specific results of this method
in section \ref{chap:benchmarks}: Benchmarks.

Finally Arnold also considers other practical
aspects of pixel farming, such as what crops
to best replace other with after a cycle
of farming is completed. However, we will not consider
these issues in this thesis.