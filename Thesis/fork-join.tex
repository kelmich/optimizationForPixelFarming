\chapter{Parallel Simulated Annealing}
\label{chap:sim-an}

In this chapter we will look at a way of utilizing
multiple computers to increase the performance of simulated
annealing. Let us first formally state the simulated annealing
algorithm. We assume the parameters $T_0, K$, outer rounds and 
inner rounds have been defined. This algorithm is the
algorithm Arnold considered:
\FloatBarrier
\begin{algorithm}
    \caption{Simulated Annealing}
    \label{arnold-simAn}
    \begin{algorithmic}
        \State $F \gets randomField()$
        \State $T \gets T_0$
        \For{$i \in \{1, \dots, \text{outer rounds}\}$}
            \For{$j\in \{1, \dots, \text{inner rounds}\}$}
                \State $u, v \gets twoRandomDistinctPixels()$
                \State $F' \gets swap(F, u, v)$
                \State $\Delta S \gets score(F') - score(F)$
                \State $p \gets e^{\Delta S / T}$
                \If{$random() < p$}
                    \State $F \gets F'$
                \EndIf
            \EndFor
            \State $T \gets T \cdot K$
        \EndFor\\
        \Return $F$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
We note that a positive score change is always accepted
due to the property that $e^x \geq 1$ for $x \geq 0$ and
$random() \mapsto [0, 1]$

A typical way of extending Algorithm $\ref{arnold-simAn}$
is ``jump starting'' an iteration with a previously
computed solution. In essence we can chain multiple
iterations of the simulated annealing algorithm
together. We assume `max same iterations' to be a given parameter:
\FloatBarrier
\begin{algorithm}
    \caption{Chained Simulated Annealing}
    \label{arnold-simAn-chained}
    \begin{algorithmic}
        \State $i \gets 0$
       \State $F \gets randomField()$
        \While{$i < \text{max same iterations}$}
            \State $prevScore \gets score(F)$
            \State $Fcopy \gets F$
            \State $F \gets simulatedAnnealing(F)$
            \State $i \gets i + 1$
            \If{$score(F) > prevScore$}
                \State $i \gets 0$
            \Else
                \State $F \gets Fcopy$
            \EndIf
        \EndWhile\\
        \Return $F$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
Lastly we can extend Algorithm $\ref{arnold-simAn-chained}$ to make
use of multiple computers. For this we first have all of the computers
split up and perform a round of simulated annealing on the current
field. Then the computers join back up and determine which computer
made the most progress. In the next round all computers start
from the best field of the previous round:
\FloatBarrier
\begin{algorithm}
    \caption{Parallel Simulated Annealing}
    \label{arnold-simAn-parallel}
    \begin{algorithmic}
        \State $i \gets 0$
        \State $F < randomField()$
        \While{$i < \text{max same iterations}$}
            \State $prevScore \gets score(F)$
            \State $Fcopy \gets F$
            \State $results \gets []$
            \State $... \; all\; computers \; split \; up \; ...$
            \State $ $
            \State $computerId \gets \text{The ID of the individual computer}$
            \State $F \gets simulatedAnnealing(F)$
            \State $results[computerId] \gets F$
            \State $ $
            \State $... \; all\; computers \; join \; up \; ...$
            \State $F \gets bestResult(results)$
            \State $i \gets i + 1$
            \If{$score(F) > prevScore$}
                \State $i \gets 0$
            \Else
                \State $F \gets Fcopy$
            \EndIf
        \EndWhile\\
        \Return $F$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
In order to choose good hyperparameters
we iterated over the cross product of the following sets for
three iterations each. We note that only three iterations
were performed for each set of hyperparameters in order to ensure
the program testing all of the parameter combinations would
terminate in a reasonable time:
\begin{itemize}
    \item $T_0 \in \{5, 10, \dots, 25\}$
    \item $K \in \{0.8, 0.82, \dots, 0.98\}$
    \item outer rounds $\in \{2000, 2500, \dots, 6000\}$
    \item inner rounds $\in \{500, 1000, \dots, 2500\}$
\end{itemize}
and chose the following hyperparameters based on
the average score the three iterations had:
\begin{itemize}
    \item $T_0 = 25$
    \item $K = 0.98$
    \item outer rounds $= 4500$
    \item inner rounds $= 2500$
    \item max same iterations $= 10$
\end{itemize}
Where `max same iterations' was set at $10$ because
we deemed this to be reasonable and wanted it to be
consistent a method we will intorduce later.
We further note that this tuning again does not
provide any guarantees of producing optimal parameters.


Those hyperparameters yielded the following benchmark results:
\FloatBarrier
\begin{figure}[h]
    \centering
    \begin{tabular}{@{}lccc@{}} \toprule
        Method & Average Score & Standard Deviation & Iterations\\ \midrule
        Simulated Annealing & $664.1$ & $6.1$ & $7509821$\\
        Gradient Ascent & $572.4$ & $15.9$ & $9854$\\ 
        Parallel Simulated Annealing & $675.3$ & $1.7$ & $1068$\\ 
        \bottomrule
    \end{tabular}
\end{figure}
\FloatBarrier
Which is an improvement over only doing simulated annealing.
Not only does the average score increase but the algorithm
also produces much stabler results (lower standard deviation).
We also note that, while the parallel simulated annealing
is quite a bit slower than simulated annealing alone,
as in less iterations were performed in the same time period,
on average the method still converges rather quickly
at around 81 seconds.