\changetocdepth {2}
\babel@toc {english}{}\relax 
\contentsline {chapter}{Contents}{iii}{section*.1}%
\contentsline {chapter}{\chapternumberline {1}Acknowledgements}{1}{chapter.1}%
\contentsline {chapter}{\chapternumberline {2}Introduction}{3}{chapter.2}%
\contentsline {chapter}{\chapternumberline {3}Related Work}{5}{chapter.3}%
\contentsline {chapter}{\chapternumberline {4}Problem Statement}{7}{chapter.4}%
\contentsline {chapter}{\chapternumberline {5}Computational Difficulty}{11}{chapter.5}%
\contentsline {chapter}{\chapternumberline {6}Benchmark Problems}{15}{chapter.6}%
\contentsline {chapter}{\chapternumberline {7}Bounds}{19}{chapter.7}%
\contentsline {section}{\numberline {7.1}Basic Bound}{19}{section.7.1}%
\contentsline {section}{\numberline {7.2}LP bound}{20}{section.7.2}%
\contentsline {chapter}{\chapternumberline {8}Auxiliary Statements}{21}{chapter.8}%
\contentsline {section}{\numberline {8.1}Creating integer solutions from fractional solutions}{21}{section.8.1}%
\contentsline {subsection}{\numberline {8.1.1}The standard method}{21}{subsection.8.1.1}%
\contentsline {subsection}{\numberline {8.1.2}The advanced method}{23}{subsection.8.1.2}%
\contentsline {section}{\numberline {8.2}Optimally swapping non-neighboring pixels}{26}{section.8.2}%
\contentsline {chapter}{\chapternumberline {9}Gradient Ascent}{31}{chapter.9}%
\contentsline {section}{\numberline {9.1}Finding the closest valid solution}{31}{section.9.1}%
\contentsline {section}{\numberline {9.2}Method}{32}{section.9.2}%
\contentsline {chapter}{\chapternumberline {10}Parallel Simulated Annealing}{35}{chapter.10}%
\contentsline {chapter}{\chapternumberline {11}The Multiswap Method}{39}{chapter.11}%
\contentsline {chapter}{\chapternumberline {12}Future Work}{43}{chapter.12}%
\contentsline {chapter}{\chapternumberline {13}Conclusion}{45}{chapter.13}%
\contentsline {appendix}{\chapternumberline {A}Appendix}{47}{appendix.A}%
\contentsline {section}{\numberline {A.1}Agroscope Data}{47}{section.A.1}%
\contentsline {chapter}{Bibliography}{49}{appendix*.2}%
