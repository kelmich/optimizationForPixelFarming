\chapter{The Multiswap Method}

As seen in Theorem \ref{thm:multiswap} we can efficiently
compute the optimal swap for a set of non-neighboring pixels.
We will use this fact to extend the parallel simulated annealing
and swap multiple crops at the same time instead of just two.
We assume that the regular simulated annealing parameters are defined:
$T_0, K$, Outer Rounds and Inner Rounds, as well as two new parameters
`max size rise' and `max size fall' that specify the maximum size of the pixel set when improving
or worsening the score:
\FloatBarrier
\begin{algorithm}
    \caption{Multiswap Simulated Annealing}
    \label{kelmich-simAn}
    \begin{algorithmic}
        \State $F \gets randomField()$
        \State $T \gets T_0$
        \For{$i \in \{1, \dots, \text{Outer Rounds}\}$}
            \For{$j\in \{1, \dots, \text{Inner Rounds}\}$}
                \State $P \gets randomNonneighboringSet(\text{max size rise})$
                \State $F \gets multiswap(F, P)$
                \If{$random() < T$}
                    \State $P \gets randomPixelSet(\text{max size fall})$
                    \State $F \gets shufflePixels(F; P)$
                \EndIf
            \EndFor
            \State $T \gets T \cdot K$
        \EndFor\\
        \Return $F$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
One key difference to simulated annealing is that we
worsen the score differently. In regular simulated
annealing we first swap two pixels and then decide
to either accept or reject the swap based on the score change the
swap causes. This cannot be extended analogously to the Multiswap
method, because the multiswap step never worsens the score, unlike
randomly swapping two pixels. This means
that, if we didn't manually worsen the score every couple of rounds, then
we would only be hillclimbing, albeit by optimally swapping multiple
pixels at the same time. Hence, with decreasing probability throughout
the run of the method, we randomly swap a set of pixels.



Similarly to the parallel simulated annealing approach, we chain
multiple Multiswap Simulated Annealing rounds after each other as
a next step:
\FloatBarrier
\begin{algorithm}
    \caption{Chained Multiswap Simulated Annealing}
    \label{kelmich-simAn-chained}
    \begin{algorithmic}
        \State $i \gets 0$
       \State $F \gets randomField()$
        \While{$i < \text{max same iterations}$}
            \State $prevScore \gets score(F)$
            \State $Fcopy \gets F$
            \State $F \gets simulatedAnnealing(F)$
            \State $i \gets i + 1$
            \If{$score(F) > prevScore$}
                \State $i \gets 0$
            \Else
                \State $F \gets Fcopy$
            \EndIf
        \EndWhile\\
        \Return $F$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
and in a final step, again similar to the parallel simulated annealing approach,
allow multiple computers to search simultaneously:
\FloatBarrier
\begin{algorithm}
    \caption{Parallel Simulated Annealing}
    \label{kelmich-simAn-parallel}
    \begin{algorithmic}
        \State $i \gets 0$
        \State $F \gets randomField()$
        \While{$i < \text{max same iterations}$}
            \State $Fcopy \gets F$
            \State $results \gets []$
            \State $... \; all\; computers \; split \; up \; ...$
            \State $ $
            \State $computerId \gets \text{The ID of the individual split up computer}$
            \State $prevScore \gets score(F)$
            \State $F \gets simulatedAnnealing(F)$
            \State $results[computerId] \gets F$
            \State $ $
            \State $... \; all\; computers \; join \; up \; ...$
            \State $F \gets bestResult(results)$
            \State $i \gets i + 1$
            \If{$score(F) > prevScore$}
                \State $i \gets 0$
            \Else
                \State $F \gets Fcopy$
            \EndIf
        \EndWhile\\
        \Return $F$
    \end{algorithmic}
\end{algorithm}
\FloatBarrier
Again we need to define hyperparameters. Unfortunately we cannot take
the same approach to finding good hyperparameters as we have in the
previous methods. While with the previous methods it was possible
to try large sets of parameters in a sensible amount of time,
a run of the Multiswap method on the cluster takes
over an hour on average,
which makes trying tousands of parameter sets impractical.
Hence we chose the following parameters through manual trial and error:
\begin{itemize}
    \item $T_0 = 0.8$
    \item $K = 0.85$
    \item outer rounds $= 450$
    \item inner rounds $= 105$
    \item max size rise $= X \cdot Y = 225$
    \item max size fall $= 2$
    \item max same iterations $= 10$
\end{itemize}
and achieved another score improvement
over the previous improvement parallel simulated annealing already provided:
\FloatBarrier
\begin{figure}[h]
    \centering
    \begin{tabular}{@{}lccc@{}} \toprule
        Method & Average Score & Standard Deviation & Trials\\ \midrule
        Simulated Annealing & $664.1$ & $6.1$ & $7509821$\\
        Gradient Ascent & $572.4$ & $15.9$ & $9854$\\ 
        Parallel Simulated Annealing & $675.3$ & $1.7$ & $1068$\\ 
        Multiswap Simulated Annealing & $683.7$ & $1.5$ & $19$\\
        \bottomrule
    \end{tabular}
\end{figure}
\FloatBarrier
Noteworthy is that it now takes significantly longer for the method to converge, as evidenced
by the low number of iterations performed over the same time period as the other methods.
While it may take longer to converge, we are at least rewarded with a higher average score.