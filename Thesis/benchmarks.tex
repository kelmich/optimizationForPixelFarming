\chapter{Benchmark Problems}
\label{chap:benchmarks}

Our goal is to develop methods that work well in
many or all cases. However, data that is based
on real observations is of particular interest.
Agroscope, the Swiss Federal Research center
for agriculture, nutrition and the environment,
has compiled some real world data. As they intend
to perform real world experiments with pixel
farming, we will focus on the pixel farming problem
instance they intend on using \cite{agroscope-matrix}.
We will consider this our benchmark problem:
\begin{align*}
    X = 15 && Y = 15\\
    \Cps = \{ c_1, \dots, c_{10} \}
    &&
    D(c_i) =
    \begin{cases}
        23 / 225 & i \leq 5\\
        22 / 225 & 5 < i
    \end{cases}
\end{align*}
\FloatBarrier
\begin{figure}[h]
    \centering
    \begin{tabular}{ c | c c c c c c c c c c c }
        $R(x, y)$ & $c_1$ & $c_2$ & $c_3$ & $c_4$ & $c_5$ & $c_6$ & $c_7$ &  $c_8$ & $c_9$ & $c_{10}$ & $x$\\
        \hline
        $c_1$ & -1&  0 & 0.5 & -0.5 & 1 & 0 & 0.5 & 1 & 0.5 & 0\\
        $c_2$ & 0 & -1 & 0 & 1 & 0 & 0 & 0.5 & 0.5 & 0.5 & 0\\
        $c_3$ & 0.5 & 0 & -1 & 0 & 0 & 0 & 0.5 & 0.5 & 0 & -0.5\\
        $c_4$ & -0.5 & 1 & 0 & -1 & 0 & 0 & -1 & 0.5 & 0.5 & 0.5\\
        $c_5$ & 0 & 0 & 0 & 0 & -1 & 0 & 0 & 0 & 0 & 0\\
        $c_6$ & 0 & 0 & 0 & 0 & 0 & -1 & 1 & 0.5 & 0 & 0\\
        $c_7$ & 0.5 & 0.5 & 0.5 & -0.5 & 0 & 0.5 & -1 & 0.5 & 0.5 & 0.5\\
        $c_8$ & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
        $c_9$ & 0.5 & 0.5 & 0 & 1 & 0 & 0 & 0.5 & 0.5 & -1 & 0\\
        $c_{10}$ & 0 & 0 & -0.5 & 0.5 & 0 & 0 & 1 & 0.5 & 0.5 & -1\\
        $y$
    \end{tabular}
\end{figure}
\FloatBarrier
Whenever we present benchmarks for a method we will also
present the following information:
\begin{itemize}
    \item The hyperparameters we chose
    \item How we chose our hyperparameters
    \item The average score the method achieved on the benchmark problem
    \item The standard deviation
    \item The number of times the algorithm was run
\end{itemize}
Further, unless we specify otherwise, the following
applies for all benchmarks:
\begin{itemize}
    \item The benchmark was run on the ETH Zürich Euler cluster for 24h using 48 compute nodes
    \item The random seed used for each compute node corresponds to the compute nodes
    Id, so a number ranging from $0$ to $47$. This means all results
    can be run again and verified.
    \item The method was implemented in C.
    \item If a method does not make explicit use of parallelism,
    then the algorithm was run on the 48 compute nodes independently.
    Thus for such methods often more runs of the algorithm could
    be performed in the 24h window in comparison to explicitly
    parallel algorithms.
\end{itemize}

Here we state the results Arnold's simulated annealing approach
obtained when we reimplemented it. A formal specification
of the algorithm can be found in Section \ref*{chap:sim-an} 
as Algorithm \ref*{arnold-simAn}.
We chose the following hyperparameters:
\begin{itemize}
    \item $T_0 = 10$ as our starting ``temperature''
    \item $K = 0.995$ as our cooling factor
    \item outer rounds $= 3000$ as the number of outer rounds
    \item inner rounds $= 1400$ as the number of inner rounds
\end{itemize}
Because Arnold suggests these parameters for the size
of problem we are solving \cite[Page 19]{arnold-paper}.
Further we iterated a large set of hyperparameters
to look for parameters that might increase the performance
of the method. We tried three rounds of simulated annealing
for every element of the cross product of the following
sets:
\begin{itemize}
    \item $T_0 \in \{ 5, 10, \dots, 25 \}$
    \item $K \in \{ 0.8, 0.82, \dots, 0.98 \}$
    \item outer rounds $\in \{ 2000, 2500, \dots, 6000 \}$
    \item inner rounds $\in \{500, 1000, \dots, 2500\}$
\end{itemize}
and we did not find a set of parameters whose average score
out of the three runs improved significantly over Arnolds hyperparameters.
The ranges we chose to iterate over were chosen in order
to cover a large, but in our opinion still sensible, range.
While this approach is not very rigerous, it is unfortunately
as good as we can do given the amount of compute resources and
time available.

Simulated annealing with the specified parameters yielded the following scores:
\FloatBarrier
\begin{figure}[h]
    \centering
    \begin{tabular}{@{}cccc@{}} \toprule
        Method & Average Score & Standard Deviation & Iterations\\ \midrule
        Simulated Annealing & $664.1$ & $6.1$ & 7509821\\ 
        \bottomrule
    \end{tabular}
\end{figure}
\FloatBarrier
where we note that the performed number of iterations is
rather high because the simulated annealing is very fast
and the amount of performed rounds in 24h over 48 nodes is rather high.