\chapter{Problem Statement}
\label{chap:problem-statement}

We would first like to clearly define the problem we are attempting
to optimize for and introduce some terminology. We will consider
two versions of pixel farming. The first version is the integer version
of pixel farming. While the specifics are outlined below, the key takeaway
is that this is the regular version we are mainly interested in. We further
define a second fractional variant, whose key distinction from the integer
version is that we allow multiple different crop types to be planted within
a pixel. This version is not very useful for practice, but it enables us
to try more traditional methods for optimization the integer version would
not allow. An example where the fractional version is useful
is gradient ascent, which we will see later.


We define the following terms:

\begin{definition}[Pixel Farming Problem Instance]
\label{def:PFprobleminstance}
A Pixel Farming problem instance consists of four components:
\begin{enumerate}
    \item The field dimensions $X, Y \in \mathbb{N}$ denoting the number of rows and columns of the field, when seen as a grid.
    \item The types of crop we would like to plant. We let
    $C\in \N$ denote the number of different crop types we would
    like to plant and $\Cps := \{c_1, \cdots, c_{C}\}$
    as the set of all crops.
    \item A function $R: \Cps^2 \to [-1, 1]$ that measures the
    synergy between two crops. The higher the value of
    $R(c_a, c_b)$, the more crop $c_a$ likes $c_b$. We do not
    require this function to be symmetric.
    \item A function $D$ mapping from $\Cps \to [0, 1]$
    where $D(c)$ denotes the fraction of the field
    that should be occupied by $c$. $D$ should satisfy
    $D(c) \cdot X \cdot Y \in \N \quad \forall c \in \Cps$
    as well as $\sum_{c \in \Cps} D(c) = 1$.
\end{enumerate}
\end{definition}
as well as the term pixel:
\begin{definition}[Pixel]
    Given a Pixel Farming problem instance,
    we define a pixel to be an element of
    $\{0, \dots, X-1\} \times \{0, \dots, Y-1\}$.
    Further we allow indexing into our solutions
    matrices by pixel:
    \begin{align*}
        p = (x, y) & & F_p := F_{x, y}
    \end{align*}
\end{definition}
To formulate both the integer and fractional versions
of the pixel farming problem we will require a notion
of neighboring pixels. Hence we define the following
helper function:
\begin{definition}[Neighborhood Function]
    \label{def:Neighborhood_Function}
Given a pixel, the neighborhood function $N$ returns all
valid field locations that are off by at most $1$
in both the $x$ and $y$ direction of the given pixel.
We note that field position are zero indexed.
Further a pixel cannot be its own neighbor:
\begin{displaymath}
    N(x, y) = \{(a, b) \in \N_0^2 \ | \ a < X \land b < Y \land \abs{a - x} \leq 1 \land  \abs{b - y} \leq 1 \land (a, b) \neq (x, y) \}
\end{displaymath}
\end{definition}

Next we define what it means to solve the integer
version of the problem:
\begin{definition}[Integer Version of Pixel Farming]
    \label{def:PF_Int_Version}
We are given a Pixel Farming problem instance and
are asked to find an arrangement of the crops within
the field that maximizes the synergies between all
neighboring plants. Formally we are searching for a matrix
$F \in \Cps^{X \times Y}$ that maximizes the following
score function:
\begin{displaymath}
    S(F, R) = \sum_{i = 0}^{X-1} \sum_{j = 0}^{Y-1} \sum_{(p, q) \in N(i, j)} R(F_{i, j}, F_{p, q})
\end{displaymath}
where we also fulfill the distribution constraint:
\begin{align*}
    \forall c \in \Cps \;:\; D(c) &= \frac{\abs{\{ (i, j) : F_{i, j} = c \}}}{X \cdot Y}
\end{align*}
\end{definition}

Finally we define the fractional version of pixel farming.
Again we stress that we will only use this version as a means
for applying more traditional methods of exploring large search
spaces. This version of the problem will allow us to plant
fractional amounts of multiple different types of crop
within one pixel. Formally we have:

\begin{definition}[Fractional Version of Pixel Farming]
    \label{def:PF_frac_Version}
We are given a Pixel Farming problem instance and are asked
to find an arrangement of crops within the field that
maximizes the synergies between neighboring plants.
Formally we are searching for a matrix $F \in [0, 1]^{C \times X \times Y}$,
where every pixel gets $C$  variabes that represents how much of each
crop is planted within. We denote how much of crop $c$ is contained
within pixel $p$ as follows:
\begin{displaymath}
    F_{x, y}^c := \text{Amount of crop $c$ in pixel } (x, y)
\end{displaymath}
and we are looking to maximize the following
score function:
\begin{align*}
    S(F, R) &= \sum_{i = 0}^{X-1} \sum_{j = 0}^{Y-1} \sum_{(p, q) \in N(i, j)} \sum_{c_a \in \Cps} \sum_{c_b \in \Cps} R(c_a, c_b) \cdot F_{i, j}^{c_a} \cdot F_{p, q}^{c_b}
\end{align*}
\end{definition}
where we constrain that the sum of crops planted
within a pixel is $1$ and that the field follows the
crop distribution constraint (as before):
\begin{align*}
    \forall c \in \Cps : D(c) \cdot X \cdot Y &= \sum_{i = 0}^{X-1} \sum_{j = 0}^{Y-1} F_{i, j}^c\\
    0 \leq x < X,\; 0 \leq y < Y \; : \; \ \sum_{c \in \Cps} F_{x, y}^c &= 1
\end{align*}

We note that the fractional version is an expansion of the integer version.
Any integer field can be transformed into a fractional field. To transform
an integer pixel $(x, y)$ containing crop $c$ simply set the fractional field
$F_{x, y}^c = 1$ and all other crops $c'$ for the same pixel $(x, y)$ to $F_{x, y}^{c'} = 0$.