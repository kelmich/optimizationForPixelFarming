\chapter{Computational Difficulty}

We prove that the problem of finding a
solution of a certain quality to a pixel
farming problem (integer version) is NP-complete.
To do this we will first formulate the problem as
a decision problem. In a second step we will
show that the problem is in NP. Lastly we will
prove that pixel farming can be used to find
Hamiltonian Paths, which will imply that finding
solutions for pixel farming is NP-complete.

\begin{definition}[Pixel Farming Decision Version]
    We define the decision version of the pixel farming
    problem very similarly to the
    regular version. However, instead of asking
    for a field with the maximum score, we ask
    for a field with a score of at least $s$. Formally:
    Does a field $F$ for relation function $R$ exist
    such that the score $S(F, R)$ is at least $s$:
    \begin{displaymath}
        s \leq S(F, R)
    \end{displaymath}
\end{definition}

Now that we have defined the decision version of
the problem, we first prove that pixel farming
is in NP. In a next step we will prove that pixel
farming is NP-hard. Finally we will conclude
that pixel farming must be NP-complete.

\begin{theorem}
    \label{thm:PFinNP}
    The decision version of pixel farming is in NP.
\end{theorem}

\begin{proof}

We non-deterministically select
a field $F$ for a given relation
function $R$. By computing the score $S(F, R)$, which
can be done in polynomial time in relation to the field
size $X \cdot Y$, we can easily test wether the score
is at least a certain value. Further, testing whether
our field $F$ meets the requirements set forth by $D$,
our function specifying how much of
the field should consist of a certain crop, can also
be done in polynomial time in relation to the field
size $X \cdot Y$ and number of crops $C$.
\end{proof}

Now that we know that pixel farming is in NP,
our next step is to show that pixel farming is
NP-hard.


It is known that finding Hamiltonian Paths is NP-complete \cite[Page 199, GT39]{alma990005774300205503}.
Thus, reducing the decision version
of Hamiltonian Paths (explained below) to the decision
version of pixel farming demonstrates that pixel
farming is NP-complete.

\begin{definition}[Hamiltonian Paths Decision Version]
    Let $G = (V, E)$ be an undirected graph. We formulate the
    decision version of the Hamiltonian Path problem as follows:
    Does a path that visits every vertex
    in the graph exactly once exist?
\end{definition}

Now that we have an understanding of the Hamiltonian path
problem and its difficulty we are ready to farmulate the
following Theorem:

\begin{theorem}
    \label{thm:PFNPHard}
    The decision version of pixel farming is NP-hard.
\end{theorem}

\begin{proof}
    We assume we are given an undirected
graph $G$ and a polynomial time algorithm in $X \cdot Y$
and $C$ for the pixel farming decision problem. We are asked
to solve the Hamiltonian path decision problem in polynomial
time in relation to the number of vertices and edges of $G$.

The core idea is that we translate vertices to crops and
edges to crops liking each other. Then, a pixel farming
decision problem on a $1 \times \abs{V}$ field
with a score that means every pixel
likes both of its neighbors must imply that there exists
a Hamiltonian path in the graph $G$. Formally we build
the following pixel farming decision problem:
\begin{align*}
    X &= \abs{V} && Y = 1\\
    \Cps &= V && R(a, b) = \begin{cases}
        1 & \{a, b\} \in E\\
        0 & otherwise.
    \end{cases}\\
    s &= 2 \cdot (\abs{V} - 1) && D(c) = \frac{1}{\abs{V}} \quad \forall c \in \Cps
\end{align*}
The following example illustrates this:
\FloatBarrier
\begin{figure}[h]
    \centering
    \begin{minipage}{0.45\textwidth}
        \centering
        \begin{tikzpicture}[node distance={15mm}, main/.style = {draw, circle}]
            \node[main] (1) {$v_1$}; 
            \node[main] (2) [right of=1] {$v_2$};
            \node[main] (3) [below of=2] {$v_3$}; 
            \node[main] (4) [below of=1] {$v_4$};
            \draw (1) -- (2);
            \draw (2) -- (4);
            \draw (3) -- (1);
            \draw (4) -- (3);
        \end{tikzpicture} 
        \caption{example graph}
    \end{minipage}\hfill
    \begin{minipage}{0.45\textwidth}
        \centering
        \begin{tikzpicture}[node distance={0.75cm}, main/.style = {draw, minimum size=0.75cm}]
            \node[main] (1) {$v_1$};
            \node[main] (2) [right of=1] {$v_2$};
            \node[main] (3) [right of=2] {$v_4$}; 
            \node[main] (4) [right of=3] {$v_3$};
        \end{tikzpicture}
        \hfill
        \caption{corresponding solution field}
    \end{minipage}
\end{figure}
\FloatBarrier
It should be noted that this problem can be built
in polynomial time in relation to the field size $X \cdot Y$ and number
of crops $C$. Now we pass the pixel farming problem formulated
above to our algorithm
that solves the decision version of the pixel farming problem.
If we get the answer that no solution
with value at least $s$ exists, we conclude that no longest path of
length $\abs{V} - 1$ exists. If, on the other hand, a solution
of at least $s$ exists, we know that a Hamiltonian path
must exist in $G$.

The rationale for this is as follows: Let us assume
a solution of value at least $s$ exists. This would
imply that every pixel likes all of their neighbors.
As "crop $a$ likes crop $b$" is the same function as
"vertex $a$ is connected to vertex $b$", we must have
found $\abs{V}-1$ edges that connect our $\abs{V}$ vertices. Because we also
know that every crop can only appear once in our field and all crops
have two neighbors (except the ends) we must have found a
path of length $\abs{V} - 1$ that visits every vertex exactly
once.

If, on the other hand, no arrangement of value at least $s$
exists, there must be at least one index $i$ for all fields $F$
where $R(F_{i, 1}, F_{i+1, 1}) = 0$. What this translates
to for our longest path problem is that every order
of vertices has an index $i$ where $\{v_i, v_{i+1}\} \not \in E$,
hence no path of length $\abs{V} - 1$ can exist.
\end{proof}

Finally we utilize these two Theorems to formulate the
following Theorem:

\begin{theorem}
    The decision version of pixel farming is NP-complete.
\end{theorem}

\begin{proof}
    As we know from Theorem \ref*{thm:PFinNP} that the decision
    version of pixel farming is in NP and from Theorem \ref*{thm:PFNPHard}
    that the decision version of pixel farming is NP-hard, we can conclude that
    the decision version of pixel farming must be NP-complete.
\end{proof}


% \section{Fractional Pixel Farming is NP-complete}
% We consider the slightly modified version of the
% standard pixel farming decision problem. Instead of allowing
% a pixel to only contain a single crop,
% a pixel may now contain multiple crops, as explained
% in the problem definition section. One might suspect that
% such a relaxation might enable us to formulate a
% convex optimization problem. We show however that
% this relaxation is also NP-complete.

% We define the decision version of fractional pixel farming
% analogously to the standard version: Does a field $F'$
% for a relation function $R$ exist such that the score
% $S(F', R)$ is at least $s$:
% \begin{align*}
%     s \leq S(F', R)
% \end{align*}

% \begin{theorem}
%     The fractional pixel farming problem (decision version) is NP-complete.
% \end{theorem}

% \begin{proof}
%     The problem is obviously in NP, as we can non-deterministically
%     choose a field $F'$ for a given score function
%     $R$ and calculate the
%     score in polynomial time in relation to the size of the
%     field and number of crops. In a next step we verify
%     that the solution meets the requirement for crop
%     distribution given to us. This can also be done in
%     polynomial time relative to field size and number
%     of crop types. Finally, we test if the score $S(F', R)$
%     is at least $s$.

%     For the proof of NP Hardness we again reduce to the
%     Hamiltonian Paths problem as in the proof that
%     regular pixel farming is NP-complete. For this
%     purpose we define the constructed pixel farming
%     problem for a given graph $G = (V, E)$ as follows:

%     \begin{align*}
%         X &= \abs{V} && Y = 1\\
%         \Cps &= V && R(a, b) = \begin{cases}
%             1 & \{a, b\} \in E\\
%             0 & otherwise.
%         \end{cases}\\
%         s &= 2 \cdot (\abs{V} - 1) && D(c) = \frac{1}{\abs{V}} \quad \forall c \in \Cps\\
%         F' &\in R_+^{C \times X \times Y}
%     \end{align*}
    
%     We show that
%     the following implications hold:
%     \begin{enumerate}
%         \item There exists no optimal fractional solution to
%         the constructed pixel farming problem $\implies$ 
%         There exists no Hamiltonian path in $G$
%         \item There exists an optimal fractional
%         solution to the constructed pixel farming problem
%         $\implies$ There exists a Hamiltonian path in $G$
%     \end{enumerate}
%
%     The first implication is obviously true. We prove this indirectly,
%     that is we show that: ``There exists a Hamiltonian path in $G$''
%     $\implies$ ``There exists an optimal fractional solution to the
%     constructed pixel farming problem''. This statement is a
%     direct consequence of the NP-completeness proof for the standard
%     pixel farming problem. If a Hamiltonian path exists, then
%     an optimal solution for the standard pixel farming problem
%     instance exists. As this standard solution can also be
%     viewed as an optimal fractional solution ($1$'s and $0$'s
%     are fractions too) we are done.

%     The second implication we prove by directly.
%     We assume that there exists an optimal solution to the
%     constructed pixel farming problem for a
%     Graph $G$. An optimal
%     solution in our case means that every crop likes
%     all of the crops in its neighborhood, otherwise
%     the solution would not obtain the maximal
%     possible score $s$. Further there must also
%     exist a sequence of crops such that:
%     \FloatBarrier
%     \begin{enumerate}
%         \item The sequence only contains each crop exactly once
%         \item Every pixel along the proposed sequence of crops contains some of the proposed crop
%     \end{enumerate}
%     \FloatBarrier
%     If these two facts did not hold, then there must
%     exist a crop within our valid solution that is contained
%     less than once, which would mean our solution is invalid.

%     TODO: MAKE MORE FORMAL

%     As a sequence of all crops where every crop
%     likes its neighbors corresponds to $G$
%     containing a Hamiltonian path, we know
%     our statement must be true.
% \end{proof}