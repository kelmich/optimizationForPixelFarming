\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning}% To get more advances positioning options
\usetikzlibrary{arrows}% To get more arrow heads
\graphicspath{ {./imgs/} }

\title{Stretching Method A}
\author{Michael Keller}
\date{February 2022}

\begin{document}
\maketitle
\section{Problem Description}
We are concerned with instances of the pixel farming problem
where the different types of crops are limited but the field
may be very large. We will propose a method for generating
solutions for larger fields given a (somewhat) optimal
solution for a smaller field.
\section{Notation}
We will use the following shorthand:
\begin{itemize}
    \item $x$: The $x$ dimension of our small field
    \item $y$: The $y$ dimension of our small field
    \item $f_x$: The odd factor by which we will
                stretch our small solution in the $x$ direction.
    \item $f_y$: The odd factor by which we will
    stretch our small solution in the $y$ direction.
\end{itemize}
\section{Method}
We propose the following method for generating a larger
solution $sol'$ based on a smaller solution $sol$ (0 indexing):
\begin{align*}
    sol'_{i, j} &= sol_{a, b} \quad \forall i \in \{0, \dots, f_y \cdot (y-1)\}, \forall j \in \{0, \dots, f_x \cdot (x-1)\}\\
    a &= \lfloor i / f_y \rfloor + ((i + \lfloor i / f_y \rfloor) \text{ mod } 2)\\
    b &= \lfloor j / f_x \rfloor + ((j + \lfloor j / f_x \rfloor) \text{ mod } 2)\\
\end{align*}
The following is an example for $x = 4$, $y = 3$, $f_x = 3$ and $f_y = 1$:
\begin{align*}
    \begin{bmatrix}
        9 & 10 & 11 & 12 \\
        5 & 6 & 7 & 8\\
        1 & 2 & 3 & 4\\
    \end{bmatrix}
    \to
    \begin{bmatrix}
        9 & 10 & 9 & 10 & 11 & 10 & 11 & 12 & 11 & 12 \\
        5 & 6 & 5 & 6 & 7 & 6 & 7 & 8 & 7 & 8\\
        1 & 2 & 1 & 2 & 3 & 2 & 3 & 4 & 3 & 4\\
    \end{bmatrix}\\
\end{align*}
The intuition for this method is that we want to multiply
all used neighbor relations by the same factor. The reader
is invited to check that this does indeed hold for the
crops in the center of the small field (by the factor $3$).\\ \\
This method falls slightly short of multiplying \textit{all} used
neighbor relations by the same factor. Some of the
relationships on the border are not multiplied as often as
connections in the center of the small field. Further we
slightly violate the problem constraints. Not all crop types
are used in the same frequency in our new field. All of
these issues will be analyzed further in the following paragraphs.
\section{Relative Score}
Before considering how close to the optimum our grown solution
is, let us first investigate the new relative score defined as:
\begin{align*}
    \text{Relative Score} &= \frac{\text{score}}{\text{maximum possible score}}
\end{align*}
and the maximum possible score is defined as the score where
every neighbor relation is $1$. Thus we obtain for a given
field size:
\begin{align*}
    \text{Pixels with $8$ neighbors} &= (x-2) \cdot (y-2)\\
    \text{Pixels with $5$ neighbors} &= 2 \cdot (x-2) + 2 \cdot (y-2) = 2x + 2y - 8\\
    \text{Pixels with $3$ neighbors} &= 4\\
    \text{Maximum possible score} &= 8 \cdot (x-2) \cdot (y-2)
                                    + 5 \cdot (2x + 2y - 8)
                                    + 3 \cdot 4\\
                                &= 8xy - 16x - 16y + 32 + 10x + 10y - 40 + 12\\
                                &= 8xy -6x -6y + 4
\end{align*}
Hence if $s$ is the score for our small field the relative
score for our small field is:
\begin{align*}
    \frac{s}{8xy -6x -6y + 4}
\end{align*}
and we would like to know when the relative score
for our new larger field is at least as good. Let
$s'$ denote the score for our new field. Then our new relative
score is:
\begin{align*}
    &\frac{s'}{8(f_x (x-1) + 1)(f_y (y-1) + 1) -6(f_x (x-1) + 1) -6(f_y (y-1) + 1) + 4}\\ 
    &= \frac{s'}{8(f_x x - f_x + 1)(f_y y - f_y + 1) -6(f_x x - f_x + 1) -6(f_y y - f_y + 1) + 4}\\ 
    &= \frac{s'}{8 \cdot f_x x \cdot f_y y - 8f_y \cdot f_x x + 8 f_x x
    - 8 f_x f_y y + 8 f_x f_y - 8 f_x
    + 8 f_y y - 8 f_y + 8
    -6(f_x x - f_x + 1) -6(f_y y - f_y + 1) + 4}\\ 
    &= \frac{s'}{8 \cdot f_x x \cdot f_y y + (8 - 8f_y) \cdot f_x x
    + (8- 8 f_x) f_y y + 8 f_x f_y - 8 f_x
    - 8 f_y + 8
    -6(f_x x - f_x + 1) -6(f_y y - f_y + 1) + 4}\\ 
    &= \frac{s'}{8 \cdot f_x x \cdot f_y y + (8 - 8f_y) \cdot f_x x
    + (8- 8 f_x) f_y y + 8 f_x f_y - 8 f_x
    - 8 f_y + 8
    -6 f_x x + 6 f_x - 6 - 6 f_y y + 6 f_y - 6 + 4}\\ 
    &= \frac{s'}{8 \cdot f_x x \cdot f_y y + (2 - 8f_y) \cdot f_x x
    + (2- 8 f_x) \cdot f_y y + 8 f_x f_y - 2 f_x
    - 2 f_y}\\ 
\end{align*}
Further we can bound $s'$ from the bottom by:
\begin{align*}
    s' &\geq f_x \cdot f_y \cdot s - 3 \cdot (f_x (x-1) + 1) - 3 \cdot (f_y (y-1) + 1) + 4\\ 
    &\geq f_x \cdot f_y \cdot s - 3 \cdot (f_x x - f_x + 1) - 3 \cdot (f_y y - f_y + 1) + 4\\ 
    &\geq f_x \cdot f_y \cdot s - 3f_x x + 3 f_x - 3 - 3 f_y y + 3 f_y - 3 + 4\\ 
    &\geq f_x \cdot f_y \cdot s - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2\\ 
\end{align*}
Which means our question is when the following condition holds:
\begin{align*}
    \frac{s}{8xy -6x -6y + 4}
    \leq
    \frac{f_x \cdot f_y \cdot s - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2}{8 \cdot f_x x \cdot f_y y + (2 - 8f_y) \cdot f_x x
    + (2- 8 f_x) \cdot f_y y + 8 f_x f_y - 2 f_x
    - 2 f_y}\\
    \leq
    \frac{s'}{8 \cdot f_x x \cdot f_y y + (2 - 8f_y) \cdot f_x x
    + (2- 8 f_x) \cdot f_y y + 8 f_x f_y - 2 f_x
    - 2 f_y}\\ 
\end{align*}
We get
\begin{align*}
    &\frac{s}{8xy -6x -6y + 4}
    \leq
    \frac{f_x \cdot f_y \cdot s - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2}{8 \cdot f_x x \cdot f_y y + (2 - 8f_y) \cdot f_x x
    + (2- 8 f_x) \cdot f_y y + 8 f_x f_y - 2 f_x
    - 2 f_y}\\
    &\frac{f_x f_y \cdot s}{f_x f_y \cdot (8xy -6x -6y + 4)}
    \leq
    \frac{f_x \cdot f_y \cdot s - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2}{8 \cdot f_x x \cdot f_y y + (2 - 8f_y) \cdot f_x x
    + (2- 8 f_x) \cdot f_y y + 8 f_x f_y - 2 f_x
    - 2 f_y}\\
    &\frac{f_x f_y \cdot s}{8 \cdot f_x x \cdot f_y y -6 \cdot f_x x \cdot f_y -6 \cdot f_y y \cdot f_x + 4 f_x f_y)}
    \leq
    \frac{f_x \cdot f_y \cdot s - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2}{8 \cdot f_x x \cdot f_y y + (2 - 8f_y) \cdot f_x x
    + (2- 8 f_x) \cdot f_y y + 8 f_x f_y - 2 f_x
    - 2 f_y}\\
\end{align*}
and now make the following substitutions:
\begin{align*}
    a &= f_x f_y \cdot s\\
    b &= 8 \cdot f_x x \cdot f_y y -6 \cdot f_x x \cdot f_y -6 \cdot f_y y \cdot f_x + 4 f_x f_y
\end{align*}
which gives us:
\begin{align*}
    \frac{a}{b} \leq
    \frac{a - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2}{b + 2 f_x x - 2 f_x x \cdot f_y + 2 f_y y - 2 f_x \cdot f_y y + 4 f_x f_y - 2f_x - 2f_y}\\ 
    a \cdot (b + 2 f_x x - 2 f_x x \cdot f_y + 2 f_y y - 2 f_x \cdot f_y y + 4 f_x f_y - 2f_x - 2f_y) \leq
    b \cdot (a - 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2)\\ 
    ab + 2a f_x x - 2a f_x x \cdot f_y + 2a f_y y - 2a f_x \cdot f_y y + 4a f_x f_y - 2af_x - 2af_y \leq
    ab - 3bf_x x - 3b f_y y + 3b f_x + 3b f_y - 2b\\ 
    2a f_x x - 2a f_x x \cdot f_y + 2a f_y y - 2a f_x \cdot f_y y + 4a f_x f_y - 2af_x - 2af_y \leq
    - 3bf_x x - 3b f_y y + 3b f_x + 3b f_y - 2b\\ 
    a \cdot (2f_x x - 2 f_x x \cdot f_y + 2 f_y y - 2 f_x \cdot f_y y + 4 f_x f_y - 2f_x - 2f_y )\leq
    b \cdot (- 3f_x x - 3 f_y y + 3 f_x + 3 f_y - 2)\\ 
    a \cdot (-2f_x x + 2 f_x x \cdot f_y - 2 f_y y + 2 f_x \cdot f_y y - 4 f_x f_y + 2f_x + 2f_y )\geq
    b \cdot (3f_x x + 3 f_y y - 3 f_x - 3 f_y + 2)\\ 
    \frac{a}{b}\geq
    \frac{3f_x x + 3 f_y y - 3 f_x - 3 f_y + 2}{2 f_x x \cdot f_y -2f_x x + 2 f_x \cdot f_y y - 2 f_y y - 4 f_x f_y + 2f_x + 2f_y}\\ 
\end{align*}
Which admittedly looks quite messy. Let us therefore consider an example. Let $f_x = f_y$. Then:
\begin{align*}
    \frac{a}{b}&\geq
    \frac{3f_x x + 3 f_x y - 3 f_x - 3 f_x + 2}{2 f_x x \cdot f_x -2f_x x + 2 f_x \cdot f_x y - 2 f_x y - 4 f_x f_x + 2f_x + 2f_x}\\ 
    \frac{a}{b}&\geq
    \frac{3f_x \cdot (x-1) + 3f_x \cdot (y-1) + 2}{2 f_x^2 x -2f_x x + 2 f_x^2 y - 2 f_x y - 4 f_x^2 + 4f_x}\\ 
    \frac{a}{b}&\geq
    \frac{3f_x \cdot (x-1) + 3f_x \cdot (y-1) + 2}{2 f_x^2 (x + y - 2) -2 f_x x - 2 f_x y + 4f_x}\\ 
    \frac{a}{b}&\geq
    \frac{3f_x \cdot (x-1) + 3f_x \cdot (y-1)}{2 f_x^2 (x + y - 2) -2 f_x x - 2 f_x y + 4f_x}\\ 
    \frac{a}{b}&\geq
    \frac{3 (x-1) + 3 (y-1)}{2 f_x (x + y - 2) -2 x - 2 y + 4}\\ 
    \frac{a}{b}&\geq
    \frac{3x + 3y - 6}{2 f_x (x + y - 2) -2 x - 2 y + 4}\\ 
    a \cdot (2 f_x (x + y - 2) -2 x - 2 y + 4)&\geq
    b \cdot (3x + 3y - 6)\\ 
    2a f_x (x + y - 2) -2a x - 2a y + 4a&\geq
    b \cdot (3x + 3y - 6)\\ 
    2a f_x (x + y - 2)&\geq
    b \cdot (3x + 3y - 6) + 2a x + 2a y - 4a\\ 
    f_x&\geq
    \frac{b \cdot (3x + 3y - 6) + 2a x + 2a y - 4a}{2a \cdot (x + y - 2)}\\ 
    f_x = f_y &\geq
    \frac{b \cdot (3x + 3y - 6) + 2a x + 2a y - 4a}{2ax + 2ay - 4a}\\ 
    f_x = f_y &\geq
    \frac{b}{a} \cdot \frac{3x + 3y - 6}{2x + 2y - 4} + 1\\ 
    f_x = f_y &\geq \frac{3b}{2a} + 1\\ 
\end{align*}
which means that if we stretch the $x$ and $y$ directions by the same factor
there will always be a larger field with the same or better relative score.
\section{Closeness to Optimum}
Let $OPT$ denote the optimal score for our small field and let $OPT'$ denote the optimal
score for our new larger field.
\section{Constraint Violations}
Todo
\end{document}