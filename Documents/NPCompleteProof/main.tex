\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning}% To get more advances positioning options
\usetikzlibrary{arrows}% To get more arrow heads
\graphicspath{ {./imgs/} }

\title{A proof that Pixel Farming is NP complete}
\author{Michael Keller}
\date{January 2022}

\begin{document}
\maketitle
\section{Problem Description}
We will consider the following decision version of Pixel Farming. We are given:
\begin{itemize}
    \item A set of $M$ crops, identified by $[M] := \{1, \dots, M\}$
    \item A binary function $R: [M] \times [M] \to [-1, 1]$. For two crops $m_1, m_2$
          the quantity $R(m_1, m_2)$ measures the synergy or
          anti-synergy between two crops.
    \item Two numbers $n_1, n_2 \in \mathbf{N}$ representing the width and
          length of a field.
    \item A probability distribution $p$ over $[M]$
    \item A goal value $v^*$.
\end{itemize}
We say a Matrix $A$ is a field arrangement if:
\begin{align*}
    A_{i, j} \in [M] &  & \forall i, j \text{ where } 1 \leq i \leq n_1 \text{ and } 1 \leq j \leq n_2
\end{align*}
We say a field arrangement $A$ has value $v$ if
\begin{align*}
    v = \sum_{(i, j), (k, l) \in \mathbf{N}} R(A_{i,j}, A_{k, l})
\end{align*}
where $(i, j)$ and $(k, l)$ represent two neighboring fields, i.e.
we must have $\abs{i - k} \leq 1$ and $\abs{j - l} \leq 1$.\\ \\
Further any arrangement $A$ must satisfy the constraints set forth
by the probability distribution $p$. Formally:
\begin{align*}
    \forall m \in [M] : p(m) = \frac{\abs{\left \{ (i, j) : A_{i, j} = m \right\}}}{ n_1 \cdot n_2}
\end{align*}
Hence we are tasked with finding an arrangement $A^*$ that
satisfies the constrains and has value $v^*$.
\section{Pixel Farming $\in$ NP}
We first show that Pixel Farming is a problem in $NP$. Consider the
following algorithm:
\begin{enumerate}
    \item Nondeterministically choose a field arrangement
          $A$ with $A_{i, j} \in [M]$ for all valid matrix entries $(i, j)$.
    \item Verify that the following constraint holds:
          \begin{align*}
              \forall m \in [M] : p(m) = \frac{\abs{\left \{ (i, j) : A_{i, j} = m \right\}}}{ n_1 \cdot n_2}
          \end{align*}
    \item Verify that the value of $A$ is in fact $v^*$
\end{enumerate}
\section{Pixel Farming solves longest path}
We are given an instance of the longest path problem (decision version):
A graph $G = (V, E)$ (undirected) and a goal value we assume is $\abs{V} - 1$.
Further we assume that we have access to a method that solves the Pixel
Farming Problem as specified in section 1 in $P$. Our next step is to map the
longest path problem to a Pixel Farming problem:
\begin{itemize}
    \item We set $M = \abs{V}$, so every vertex $v$ gets its own crop. Further we will
          denote $M(v)$ as the crop associated with a vertex $v$. $M^{-1}(c)$ will
          return the vertex associated with crop $c$.
    \item We set
          \begin{align*}
              R(M(a), M(b)) =
              \begin{cases}
                  1  & \{a, b\} \in E      \\
                  -1 & \{a, b\} \not \in E
              \end{cases}
          \end{align*}
    \item Our field is one dimensional: $n_1 = 1$, $n_2 = \abs{V}$
    \item And we force that every crop appears exactly once in our field: $p(m) = \frac{1}{\abs{V}}$
    \item Lastly our goal value $v^*$ is $2 \cdot (\abs{V} - 1)$
\end{itemize}
Now we pass the Pixel Farming problem formulated above to our algorithm
that solves Pixel Farming Problems in $P$. If we get the answer that no solution
with value $2 \cdot (\abs{V} - 1)$ exists we return that no longest path of
length $\abs{V} - 1$ exists. If on the other hand we get a solution $A^*$ to
the pixel farming problem with value $2 \cdot (\abs{V} - 1)$ we can map
this to a longest path in $G$ as follows:
\begin{align*}
    \text{Path}[i] = M^{-1}(A^*_{1, i})
\end{align*}
The rationale for this is as follows. Let us assume a perfect solution
for the pixel farming problem exists. This solution would then have
value $2 \cdot (\abs{V} - 1)$ (every neighbor likes all its neighbors). As
the "crop $a$ likes crop $b$" is the same function as
"vertex $a$ is connected to vertex $b$" and we have
a value for our arrangement of $2 \cdot (\abs{V} - 1)$, we must have
found $n-1$ edges that connect our $\abs{V}$ vertices. Because we also
know that every crop can only appear once in our field and all crops
have two neighbors (except the ends) we must have found a
path of length $\abs{V} - 1$.\\ \\
If on the other hand no arrangement of value $2 \cdot (\abs{V} - 1)$
exists, there must be at least one index $i$ for all arrangements
where $R(A_{1, i}, A_{1, i+1}) = -1$. What this translates
to for our longest path problem is that every order
of vertices has an index $i$ where $\{v_i, v_{i+1}\} \not \in E$,
hence no path of length $\abs{V} - 1$ can exist.\\ \\
Thus we know:
\begin{itemize}
    \item Pixel farming is in NP
    \item If Pixel Farming can be solved in $P$ then Longest Path can be solved in $P$
\end{itemize}
and can conclude that pixel farming must be NP complete.
\end{document}