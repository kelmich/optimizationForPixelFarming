\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{tikz}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Cps}{\mathcal{C}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\K}{\mathbb{K}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\X}{\mathbb{X}}
\usetikzlibrary{positioning}% To get more advances positioning options
\usetikzlibrary{arrows}% To get more arrow heads
\graphicspath{ {./imgs/} }

\title{A proof that Fractional Pixel Farming is NP complete}
\author{Michael Keller}
\date{August 2022}

\begin{document}
\maketitle
\section{Problem Description}
(From the thesis)
\subsection{Problem instance}
A Pixel Farming problem instance consists of four components:
\begin{enumerate}
    \item The field dimensions $X, Y \in \mathbb{N}$ denoting the number of rows and columns of the field, when seen as a grid.
    \item The types of crop we would like to plant. We let
    $C\in \N$ denote the number of different crop types we would
    like to plant and $\Cps := \{c_1, \cdots, c_{C}\}$
    as the set of all crops.
    \item A function $R: \Cps^2 \to [-1, 1]$ that measures the
    synergy between two crops. The higher the value of
    $R(c_a, c_b)$, the more crop $c_a$ likes $c_b$. We do not
    require this function to be symmetric.
    \item A function $D$ mapping from $\Cps \to [0, 1]$
    where $D(c)$ denotes the fraction of the field
    that should be occupied by $c$. $D$ should satisfy
    $D(c) \cdot X \cdot Y \in \N \quad \forall c \in \Cps$
    as well as $\sum_{c \in \Cps} D(c) = 1$.
\end{enumerate}
\subsection{Neighborhood function}
Given a pixel, the neighborhood function $N$ returns all
valid field locations that are off by at most $1$
in both the $x$ and $y$ direction of the given pixel.
We note that field position are zero indexed.
Further a pixel cannot be its own neighbor:
\begin{displaymath}
    N(x, y) = \{(a, b) \in \N_0^2 \ | \ a < X \land b < Y \land \abs{a - x} \leq 1 \land  \abs{b - y} \leq 1 \land (a, b) \neq (x, y) \}
\end{displaymath}
\subsection{Fractional Pixel Farming}
We are given a Pixel Farming problem instance and are asked
to find an arrangement of crops within the field that
maximizes the synergies between neighboring plants.
Formally we are searching for a matrix $F \in [0, 1]^{C \times X \times Y}$,
where every pixel gets $C$  variabes that represents how much of each
crop is planted within. We denote how much of crop $c$ is contained
within pixel $p$ as follows:
\begin{displaymath}
    F_{x, y}^c := \text{Amount of crop $c$ in pixel } (x, y)
\end{displaymath}
and we are looking to maximize the following
score function:
\begin{align*}
    S(F, R) &= \sum_{i = 0}^{X-1} \sum_{j = 0}^{Y-1} \sum_{(p, q) \in N(i, j)} \sum_{c_a \in \Cps} \sum_{c_b \in \Cps} R(c_a, c_b) \cdot F_{i, j}^{c_a} \cdot F_{p, q}^{c_b}
\end{align*}

\section{Fractional Pixel Farming $\in$ NP}

The decision verion of Fractional Pixel Farming (where we ask
if a solution with at least a certain score exists)
is in NP. This follows from the fact that we can first nondeterministically
chose a field and then verify its score in polynomial
time. Finally we can test if the score a specified field achieves
is sufficient of not.

\section{Fractional Pixel Farming solves longest path}
We are given an instance of the longest path problem (decision version):
A graph $G = (V, E)$ (undirected) and a goal path length of $\abs{V}$.
Further we assume that we have access to a method that solves the Fractional
Pixel
Farming Problem as specified in section 1 in $P$. Our next step is to map the
longest path problem to a Pixel Farming problem:
\begin{itemize}
    \item We set $\Cps = V$, so every vertex $v$ gets its own crop. Further we will
          denote $M(v)$ as the crop associated with a vertex $v$. $M^{-1}(c)$ will
          return the vertex associated with crop $c$.
    \item We set
          \begin{align*}
              R(M(a), M(b)) =
              \begin{cases}
                  1  & \{a, b\} \in E      \\
                  -1 & \{a, b\} \not \in E
              \end{cases}
          \end{align*}
    \item Our field is one dimensional: $X = 1$, $Y = \abs{V}$
    \item And we enforce that every crop appears exactly once in our field: $D(c) = \frac{1}{\abs{V}}$
    \item Lastly our goal value $v^*$ is $2 \cdot (\abs{V} - 1)$
\end{itemize}
Now we pass the Pixel Farming problem formulated above to our algorithm
that solves Fractional Pixel Farming Problems in $P$. If we get the answer that no solution
with value $2 \cdot (\abs{V} - 1)$ exists, we return that no longest path of
length $\abs{V}$ exists. The reason for this is the following
indirect proof: Assume a longest path of length $\abs{V}$ exists.
Then we could easily translate this into an (integer) solution
for the corresponding pixel farming problem. We note that a valid
integer solution is also a valid fractional solution.\\ \\
If on the other hand we get back a solution of value
$2 \cdot (\abs{V} - 1)$ we can generate a longest path
of length $\abs{V}$. Consider the format a possible solution
might take:
\begin{center}
    \begin{tikzpicture}[node distance={1.35cm}, main/.style = {draw, minimum size=0.75cm}]
        \node[main] (1) {$\{c_1, c_2\}$};
        \node[main] (2) [right of=1] {$\{c_2, c_3\}$};
        \node[main] (3) [right of=2] {$\{c_1, c_3\}$}; 
    \end{tikzpicture}
\end{center}
Where every pixel contains some set of crops that are planted
by some non-zero amount within. The important thing to recognize
here is the following: For the set of crops planted within a
pixel, every crop within that set likes \textbf{every other crop}
in the crop set of the neighboring pixels. Otherwise we would
not be able to achieve a perfect score of $2 \cdot (\abs{V} - 1)$
(there would be some crops generating relational scores of $-1$
otherwise). Thus any integer field assignment where every pixel
is a subset of the fractional version of the same pixel, with exactly one crop
that appears once only within the field is a valid
integer field, e.g.:
\begin{center}
    \begin{tikzpicture}[node distance={0.75cm}, main/.style = {draw, minimum size=0.75cm}]
        \node[main] (1) {$c_2$};
        \node[main] (2) [right of=1] {$c_3$};
        \node[main] (3) [right of=2] {$c_1$};
    \end{tikzpicture}
    $\subseteq$
    \begin{tikzpicture}[node distance={1.35cm}, main/.style = {draw, minimum size=0.75cm}]
        \node[main] (1) {$\{c_1, c_2\}$};
        \node[main] (2) [right of=1] {$\{c_2, c_3\}$};
        \node[main] (3) [right of=2] {$\{c_1, c_3\}$}; 
    \end{tikzpicture}
\end{center}
We can generate this using a Linear Program over the
Birkhoff Polytope. We introduce $C$ variables for every
pixel to denote the amount of each crop within a pixel.
Then we restrict these variables to be over the doubly
stochastic matrices: The amount of crop within a pixel
sums to one and every crop is planted once (possibly in
small sums accross many pixels). Finally we define our objective
as follows: For any given crop variable a pixel has: If that
crop is within the "crop set" our oracle produced for the fractional
version we multiply with $1$, otherwise with $0$. Here an example:
\begin{center}
    \begin{tikzpicture}[node distance={1.35cm}, main/.style = {draw, minimum size=0.75cm}]
        \node[main] (1) {$\{c_1, c_2\}$};
        \node[main] (2) [right of=1] {$\{c_2, c_3\}$};
        \node[main] (3) [right of=2] {$\{c_1, c_3\}$}; 
    \end{tikzpicture}
\end{center}
with Birkhoff constraints:
\begin{align*}
    F_{0,0}^{c_1} + F_{0,0}^{c_2} + F_{0,0}^{c_3} &= 1 & F_{0,0}^{c_1} + F_{1,0}^{c_1} + F_{2,0}^{c_1} &= 1\\
    F_{1,0}^{c_1} + F_{1,0}^{c_2} + F_{1,0}^{c_3} &= 1 & F_{0,0}^{c_2} + F_{1,0}^{c_2} + F_{2,0}^{c_2} &= 1\\
    F_{2,0}^{c_1} + F_{2,0}^{c_2} + F_{2,0}^{c_3} &= 1 & F_{0,0}^{c_3} + F_{1,0}^{c_3} + F_{2,0}^{c_3} &= 1\\
\end{align*}
and objective function:
\begin{align*}
    1 \cdot F_{0,0}^{c_1} + 1 \cdot F_{0,0}^{c_2} + 0 \cdot F_{0,0}^{c_3}
    + 0 \cdot F_{1,0}^{c_1} + 1 \cdot F_{1,0}^{c_2} + 1 \cdot F_{1,0}^{c_3}
    + 1 \cdot F_{2,0}^{c_1} + 0 \cdot F_{2,0}^{c_2} + 1 \cdot F_{2,0}^{c_3}
\end{align*}
Which must have an optimal integer solution, because we are over
the Birkhoff Polytope. Further we know that an optimal solution
must exist, because the solution the oracle returns is also
a valid solution for this LP with an optimal score. Thus
the Birkhoff Polytope "rearranges" the crops such that each crop
appears in only one pixel, thus yielding us an integer solution.
As this integer solutions can finally be translated into a longest
path for our Graph $G$ we are done.\\ \\
Thus we know:
\begin{itemize}
    \item Fractional Pixel farming is in NP
    \item If Fractional Pixel Farming can be solved in $P$ then Longest Path can be solved in $P$
\end{itemize}
and can conclude that Fractional Pixel Farming must be NP complete.
\end{document}