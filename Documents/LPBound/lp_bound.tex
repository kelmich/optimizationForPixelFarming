\documentclass[12pt]{article}
\usepackage{amsthm}
\usepackage{graphicx}
\newtheorem{lemma}{Lemma}
\begin{document}

For each pair of crops $(i,j), i\leq j$, there is a variable $x_{i,j}$ that counts how often crop $i$ and crop $j$ are planted next to each other.

If for each pixel with crop $i$, we add up the number of neighbors (at most $8$), we obtain the number of times some crop $j\neq i$ is planted next to $i$, plus \emph{twice} the number of times crop $i$ is planted next to itself.

In formulas,
\[
  \sum_{j< i} x_{j,i} + \sum_{j>i} x_{i,j}  + 2 x_{i,i}  \leq 8XYD(i).
\]
This seems to be a (slight?) strenghtening of the LP bound.

\paragraph{A further strenghtening?}
For a pixel $p$ and a crop $i$, let $y_{p,i}$ be the indicator variable for crop $i$ being in pixel $p$. Let $n(p)$ be the number of neighbors of pixel $p$. Then the above argument yields an equality: 
\[
   \sum_{j< i} x_{j,i} + \sum_{j>i} x_{i,j}  + 2 x_{i,i} = \sum_{p} n(p) y_{p,i}. 
\]
As constraints, we also have $0\leq y_{p,i}\leq 1$ and 
\[
\sum_{p} y_{p,i} = XYD(i)
\]
as well as
\[
\sum_{i} y_{p,i} = 1.
\]

\paragraph{An ILP.} What happens if we require all variables  $x_{i,j}$ and $y_{p,i}$ to be integer? Does the resulting integer program actually solve our original problem, so did we get rid of the quadratic objective function? Or are we still getting solutions that cannot arise from an actual planting? How do these look like? 

In any case, the ILP still seems to provide an upper bound, so having
an efficient ILP solver (Gurobi?) might give us an even better bound.

\paragraph{More bounds.}
The score function is indeed linear in the  $x_{i,j}$'s, but this just
moves the quadratic behavior into the constraints, due to
\[
x_{i,j} = \sum_{p,q} y_{p,i}y_{q,j}, \quad i\neq j, 
\]
where the sum is over all pairs $(p,q)$ of neigboring pixels.
We also have
\[
2x_{i,i} = \sum_{p,q} y_{p,i}y_{q,i}.
\]

So the set of feasible $x_{ij}'s$ has a nonlinear structure, and for
the LP bound, we are overapproximating it with linear constraints. The
best we could possibly do is put in all facet-defining constraints of
the convex hull of the feasible $x_{ij}'s$. To determine these facets
is an interesting theoretical problem. 

In practice, we can try to find additional constraints that make our
over-approximation (and hopefully the LP bound) tighter.

Here is one set of such constraints. It is not clear whether this
actually improves the upper bound, but at least this is a set
of constraints not implied by the ones that we previously used.

Here we finally put in some information about the geometry of the
neighborhood graph.

\begin{lemma} 
  \[
    x_{i,j} \leq 3 (D(i)+D(j)) XY-6, \quad i\neq j.
  \]
\end{lemma}

\begin{proof}
  For a fixed solution, we look at the graph $G$ that has an edge
  between two neighboring pixels whenever we have crops $i$ and $j$ in
  these pixels. The number of edges in this graph is $x_{i,j}$, and
  the number of vertices is $(D(i)+D(j)) XY$. We will construct a
  \emph{planar} graph $G'$ with the same number of vertices and edges,
  and as a planar graph with $n$ vertices has at most $3n-6$ edges,
  the lemma follows.

  $G'$ is obtained as follows: for every crossing (of two diagonals)
  in $G$, we have two non-edges among the four involved pixels, so we
  rotate one of the diagonal into either the bottom-most, or the
  left-most non-edge:
  
  \begin{center}
  \includegraphics[width=0.5\textwidth]{planarity.pdf}
\end{center}

This removes one crossing and does not introduce any new
crossings, as horizontal and vertical edges cannot cross any other
edges. We repeat this for all crossings, until we have obtained a
crossing-free (planar) graph with the same number of vertices and
edges. For this, we need that every non-edge was used only once
in the process.  
\end{proof}

We remark that the bound of $3$ in the lemma is best possible (but we
could probably improve the $6$). Indeed, filling even rows with crop
$i$ and odd rows with crop $j$ will lead to a neighborhood graph with
around $3 (D(i)+D(j)) XY$ many edges.

In order to see how to proceed here, it would be interesting to
see how the LP bound sets the $x_{i,j}$'s. This might give us
an idea how to best cut off the undesired overapproximation. As
the LP only fixes sums of $x_{i,j}$'s over a fixed crop $i$, it might
just look at the plants with highest pairwise synergies and plant
(infeasible) amounts of them next to each other.

\paragraph{Cutting off infeasible solutions from mutually beneficial
  crops?}

We also have the following inequalities.

\begin{lemma}\label{lem:clique}
  Let $K$ be a set of crops. Then
  \[
    \sum_{i,j\in K, i<j} x_{i,j} \leq 4\sum_{i\in K}D(i) XY.
  \]
\end{lemma}

\begin{proof}
  Given any planting, the graph of all neighborhood relations between
  pixels with different crops from $K$ has $\sum_{i\in K}D(i) XY$
  vertices and at most $4$ times as many edges, since the degree of
  each vertex is at most $8$.
\end{proof}

Now we could try to add these inequalities for sets $K$ with pairwise
high synergies. As the quality of the bound probably increases with
$|K|$, we might want to look at large cliques of mutually
beneficial crops. Assuming that the LP bound is trying to assign (too) many
neighborhood relations between such crops, we can hope that the corresponding
inequalities help. As the number of crops is small, we may even be
able to afford to add all these inequalities. 

In fact, we should probably first check the current LP bound solution  to
see whether any value
\[
\sum_{i,j\in K, i<j} x_{i,j} 
\]
exceeds the bound of Lemma~\ref{lem:clique}. Only in this case, we can
hope that the lemma helps.

\paragraph{An SDP relaxation.}

For each pair $i\neq j$ (now order doesn't matter, so there are $n(n-1)$
pairs), $x_{i,j}$ counts how often crop $i$ is planted next to crop
$j$ in the optimal solution.

The claim is that with suitable (and linearly constrained) diagonal values
$x_{i,i}$, the matrix $X=(x_{i,j})$ is symmetric positive
semidefinite. Symmetry is clear, and for positive semidefinite, we
argue as follows.

Arbitrarily enumerate the edges $\{p,q\}$ between neighboring
pixels, and for crop $i$, let $Y_i$ be the vector such that $Y_{i,k}=1$
iff crop $i$ is in one of the two pixels of the $k$-th edge. 

According to our quadratic constraints from before, we then 
have
\[
x_{i,j} = Y_i^\top Y_j, \quad i\neq j.
\]

Indeed, $Y_{i,k}Y_{j,k}=1$ iff both crops $i$ and $j$ appear in the
$k$-th edge, so the sum over all edges is the number of times crops
$i$ and $j$ are planted next to each other.

Defining the diagonal elements 
\[
x_{i,i} := Y_i^\top Y_i
\]

gives a positive semidefinite matrix $X=(x_{i,j})$ (the product of some
matrix and its transpose).

In fact, $x_{i,i}$ is the number of times crop $i$ appears in a
neighborhood relation, and this is $\frac12\sum_{p}n(p)y_{p,i}$, with the
notation introduced earlier.

Hence, enforcing the latter constraints on the diagonal elements (along
with the other constraints on the $y_{p,i}$), we see that the optimal
solution is a feasible solution of a semidefinite program, so the
optimal solution of this semidefinite program provides an upper bound
for the optimal solution of our problem. This bound is hopefully
better than the LP bound.

Concretely, the SDP should give objective function value $R(c_i,c_j)$ to
variable $x_{i,j}$ for $i\neq j$. For the diagonals $x_{i,i}$, we have
to be careful, since they are not counting how often crop $i$ is
planted next to itself but how often crop $i$ is planted next to
something. But the actual value $z_{i,i}$ (number of edges such that
both pixels have crop $i$ is (according to earlier stuff) 
\[
   \sum_{j< i} x_{j,i} + \sum_{j>i} x_{i,j}  + 2 z_{i,i} = \sum_{p} n(p) y_{p,i},
 \]
 so we can express $z_{i,i}$ in terms of other variables and hence are
 able to put objective value $2 R(c_i,c_i)$ on it.

\end{document}
