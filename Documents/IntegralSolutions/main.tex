\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning}% To get more advances positioning options
\usetikzlibrary{arrows}% To get more arrow heads
\graphicspath{ {./imgs/} }

\title{On Integral Solutions for Pixel Farming}
\author{Michael Keller}
\date{March 2022}

\begin{document}
\maketitle
\section{Introduction}
Assume we are given an integral solution for
a pixel farming problem satisfying the following constraints:
\begin{itemize}
    \item There are two crops, i.e. $C = \{c_1, c_2\}$.
    \item Let $p_c$ denote how much of crop $c$ is planted
          in our fractional pixel $p$. Then $\sum_{c \in C} p_{c} = 1$
          for all pixels $p$ in the fractional field solution.
    \item We have $\sum_{p \in \text{field}} p_c = G_c$ where
          $G_c \in \mathbf{N}_0$ denotes how much of crop $c$ we would like to have planted
          in our field in total (our \textbf{G}oal).
\end{itemize}
\section{Creating an Integer Solution (2 Crops)}
Assume we have a solution that meets the specification
in section $1$. Then we can generate a solution with at
least as good of a score where every pixel contains
exactly one crop $c$ with $p_c = 1$. The scoring
function $R$ will have to meet some constraints. We outline a
procedure.
\subsection{Non neighboring pixels}
Assume where are two non neighboring pixels that both contain
a fractional amount of $c_1$ and $c_2$. Then we can generate
a new solution with $\geq$ score that has one more pixel that
has an Integer amount of each crop. Let $R$ be the usual
scoring function and let $n_{p, c}$ denote how much of
crop $c$ is planted in neighbors of pixel $p$. Then
by shifting $\lambda$ crops from one pixel $x$ to another pixel $y$ we change
our score as follows:
\begin{center}
    \begin{tikzpicture}[->,>=stealth',auto,node distance=3cm,
            thick,main node/.style={rectangle,draw,font=\sffamily\Large\bfseries}]

        \node[main node] (1) {X};
        \node[main node] (2) [right of=1] {Y};

        \path[every node/.style={font=\sffamily\small}]
        (1) edge[bend left] node [above] {$\lambda \cdot c_1$} (2)
        (2) edge[bend left] node [below] {$\lambda \cdot c_2$} (1);
    \end{tikzpicture}
\end{center}

\begin{align*}
    \Delta Score & =
    \left(
    R(c_1, c_1) \cdot -\lambda \cdot n_{x, c_1}
    + R(c_1, c_2) \cdot -\lambda \cdot n_{x, c_2}
    + R(c_2, c_1) \cdot \lambda \cdot n_{x, c_1}
    + R(c_2, c_2) \cdot \lambda \cdot n_{x, c_2}
    \right)                 \\
                 & + \left(
    R(c_1, c_1) \cdot -\lambda \cdot n_{x, c_1}
    + R(c_2, c_1) \cdot -\lambda \cdot n_{x, c_2}
    + R(c_1, c_2) \cdot \lambda \cdot n_{x, c_1}
    + R(c_2, c_2) \cdot \lambda \cdot n_{x, c_2}
    \right)                 \\
                 & + \left(
    R(c_1, c_1) \cdot \lambda \cdot n_{y, c_1}
    + R(c_1, c_2) \cdot \lambda \cdot n_{y, c_2}
    + R(c_2, c_1) \cdot -\lambda \cdot n_{y, c_1}
    + R(c_2, c_2) \cdot -\lambda \cdot n_{y, c_2}
    \right)                 \\
                 & + \left(
    R(c_1, c_1) \cdot \lambda \cdot n_{y, c_1}
    + R(c_2, c_1) \cdot \lambda \cdot n_{y, c_2}
    + R(c_1, c_2) \cdot -\lambda \cdot n_{y, c_1}
    + R(c_2, c_2) \cdot -\lambda \cdot n_{y, c_2}
    \right)                 \\
                 & =
    \left(
    (R(c_2, c_1) + R(c_1, c_2) -2 R(c_1, c_1)) \cdot \lambda \cdot n_{x, c_1}
    + (2 R(c_2, c_2) - R(c_1, c_2) - R(c_2, c_1)) \cdot \lambda \cdot n_{x, c_2}
    \right)                 \\
                 & + \left(
    (2 R(c_1, c_1)- R(c_2, c_1)- R(c_1, c_2)) \cdot \lambda \cdot n_{y, c_1}
    + (R(c_2, c_1) + R(c_1, c_2)- 2 R(c_2, c_2)) \cdot \lambda \cdot n_{y, c_2}
    \right)                 \\
    &= (R(c_2, c_1) + R(c_1, c_2) -2 R(c_1, c_1)) \cdot \lambda \cdot (n_{x, c_1} - n_{y, c_1})\\
    &+ (R(c_2, c_1) + R(c_1, c_2)- 2 R(c_2, c_2)) \cdot \lambda \cdot (n_{y, c_2} - n_{x, c_2})
\end{align*}
\\
Which is a linear function in $\lambda$. This means we can,
by choosing our $\lambda$ appropriately, always find
a way to have a positive (or $0$) $\Delta Score$. Further we
gain at least one pixel that has only one planted crop.
We will argue later about repeatedly applying this
procedure and gaining an integer solution.
\subsection{Neighboring pixels}
The issue it seems with applying the above
procedure to neighboring pixels is that the
neighborhoods don't remain fixed (i.e. the constants
$n_{p, c}$). Let us therefore consider first
only the relationship between the two chosen
neighboring pixels where $0 \leq \alpha, \beta \leq 1$.
\begin{center}
    \begin{tikzpicture}[->,>=stealth',auto,node distance=5cm,
            thick,main node/.style={rectangle,draw,font=\sffamily\Large\bfseries}]

        \node[main node] (1) {$\alpha \cdot c_1, (1 - \alpha) \cdot c_2$};
        \node[main node] (2) [right of=1] {$\beta \cdot c_1, (1 - \beta) \cdot c_2$};

        \path[every node/.style={font=\sffamily\small}]
        (1) edge[bend left] node [above] {$\lambda \cdot c_1$} (2)
        (2) edge[bend left] node [below] {$\lambda \cdot c_2$} (1);
    \end{tikzpicture}
\end{center}
When is it better for this
relationship to be non fractional? We first calculate the score:
\begin{align*}
    Score & =  \left(
    R(c_1, c_1) \cdot \alpha \cdot \beta
    + R(c_1, c_2) \cdot \alpha \cdot (1-\beta)
    + R(c_2, c_1) \cdot (1-\alpha) \cdot \beta
    + R(c_2, c_2) \cdot (1-\alpha) \cdot (1-\beta)
    \right)                                    \\
          & + \left(
    R(c_1, c_1) \cdot \alpha \cdot \beta
    + R(c_2, c_1) \cdot \alpha \cdot (1-\beta)
    + R(c_1, c_2) \cdot (1-\alpha) \cdot \beta
    + R(c_2, c_2) \cdot (1-\alpha) \cdot (1-\beta)
    \right)                                    \\
          & = 2 \alpha \beta \cdot R(c_1, c_1)
    + (\alpha + \beta - 2\alpha \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
    + 2 (1-\alpha)(1-\beta) \cdot R(c_2, c_2)  \\
          & = 2 \alpha \beta \cdot R(c_1, c_1)
    + (\alpha + \beta - 2\alpha \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\beta -2\alpha + 2\alpha \beta) \cdot R(c_2, c_2)
\end{align*}
Next we calculate the score for when one of the pixels
has only one type of crop. We first determine the case where
one of the pixels has had all of it's crop $c_1$ shifted
to the other pixel. Hence $\alpha' = 0$
and $\beta' = \beta + \alpha$:
\begin{align*}
    Score & =
    2 \alpha' \beta' \cdot R(c_1, c_1)
    + (\alpha' + \beta' - 2\alpha' \beta') \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\beta' -2\alpha' + 2\alpha' \beta') \cdot R(c_2, c_2) \\
          & = \beta' \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\beta') \cdot R(c_2, c_2)                             \\
          & = (\alpha + \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\alpha - 2\beta) \cdot R(c_2, c_2)
\end{align*}
and analogously for the case $\alpha' = 1$
and $\beta' = \beta - (1-\alpha)$:
\begin{align*}
    Score & = 2 \alpha' \beta' \cdot R(c_1, c_1)
    + (\alpha' + \beta' - 2\alpha' \beta') \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\beta' -2\alpha' + 2\alpha' \beta') \cdot R(c_2, c_2)  \\
          & = 2 \beta' \cdot R(c_1, c_1)
    + (1 - \beta') \cdot (R(c_1, c_2) + R(c_2, c_1))               \\
          & = 2 (\beta - (1-\alpha)) \cdot R(c_1, c_1)
    + (1 - (\beta - (1-\alpha))) \cdot (R(c_1, c_2) + R(c_2, c_1)) \\
          & = 2 (\alpha + \beta - 1) \cdot R(c_1, c_1)
    + (2 - \alpha - \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
\end{align*}
And we now ask under what condition the integer option is better:
\begin{align*}
     & 2 \alpha \beta \cdot R(c_1, c_1)
    + (\alpha + \beta - 2\alpha \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\beta -2\alpha + 2\alpha \beta) \cdot R(c_2, c_2)    \\
     & \leq (\alpha + \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\alpha - 2\beta) \cdot R(c_2, c_2)                   \\
     & 2 \alpha \beta \cdot R(c_1, c_1)
    - 2\alpha \beta \cdot (R(c_1, c_2) + R(c_2, c_1))
    + 2\alpha \beta \cdot R(c_2, c_2)                            \\
     & \leq 0                                                    \\
     & R(c_1, c_1) +  R(c_2, c_2) \leq R(c_1, c_2) + R(c_2, c_1)
\end{align*}
and again for the second case:
\begin{align*}
     & 2 \alpha \beta \cdot R(c_1, c_1)
    + (\alpha + \beta - 2\alpha \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))
    + (2 - 2\beta -2\alpha + 2\alpha \beta) \cdot R(c_2, c_2)                 \\
     & \leq 2 (\alpha + \beta - 1) \cdot R(c_1, c_1)
    + (2 - \alpha - \beta) \cdot (R(c_1, c_2) + R(c_2, c_1))                  \\
     & (2 - 2\alpha - 2\beta + 2 \alpha \beta) \cdot R(c_1, c_1)
    + (2 - 2\beta -2\alpha + 2\alpha \beta) \cdot R(c_2, c_2)                 \\
     & \leq
    (2 - 2\alpha - 2\beta + 2 \alpha \beta) \cdot (R(c_1, c_2) + R(c_2, c_1)) \\
     & R(c_1, c_1) + R(c_2, c_2) \leq R(c_1, c_2) + R(c_2, c_1)
\end{align*}
We observe that if the two plant types like
each at least as much as they like themselves,
then we can make one of the pixels contain only one
type of crop without worsening our overall score.
Importantly it also does not matter which crop
we make the pixel have. This means if we would
like to swap with two neighboring pixels and
our relationship matrix meets this condition,
then we can ignore the relationship
between these two pixels and only focus on
the other neighboring pixels. Then by the argument
in the previous section 2.1 we can again keep
our score at least as good and make a pixel have
only one type of crop.
\subsection{Repeated Application}
We now know that, given a field with at least
two pixels that have multiple crops planted
in them, we can find a new field that has one
pixel more with only one type of crop planted
and as least an equally good score. By repeatedly
applying this procedure we will eventually run out
of pixel pairs with fractional solutions that can
swap with each other. Further it is impossible
for only one pixel to have a fractional solution
without violating the constraints outlined in the
problem definition. Thus this procedure terminates
with an integral solution.
\section{Creating an Integer Solution ($\geq 2$ Crops)}
\subsection{The first $x \cdot y - \binom{\abs*{C}}{2}$ pixels}
We will first perform similar swapping as in the $\abs*{C} = 2$
case. This will lead to $\geq x \cdot y - \abs*{C}^2$ being
integer. In a second step we will make the entire field
integer.\\ \\
We first determine the delta score function for fields
with an arbitrary amount of crops (non neighboring pixels):
\begin{align*}
    \Delta Score
    &= \sum_{c \in C} -\lambda \cdot R(c_1, c) \cdot n_{x, c}
    + \sum_{c \in C} \lambda \cdot R(c_2, c) \cdot n_{x, c} \\
    &+ \sum_{c \in C} -\lambda \cdot  R(c, c_1) \cdot n_{x, c}
    + \sum_{c \in C} \lambda \cdot R(c, c_2) \cdot n_{x, c}\\
    &+ \sum_{c \in C} -\lambda \cdot  R(c_2, c) \cdot n_{y, c}
    + \sum_{c \in C} \lambda \cdot R(c_1, c) \cdot n_{y, c}\\
    &+ \sum_{c \in C} -\lambda \cdot R(c, c_2) \cdot n_{y, c}
    + \sum_{c \in C} \lambda \cdot  R(c, c_1) \cdot n_{y, c}\\
    &= \lambda \sum_{c \in C} (R(c, c_2) + R(c_2, c) - R(c_1, c) -R(c, c_1)) \cdot n_{x, c}\\
    &+ \lambda \sum_{c \in C} (R(c, c_1) + R(c_1, c) - R(c_2, c) - R(c, c_2)) \cdot n_{y, c}\\
    &= \lambda \sum_{c \in C} (R(c, c_2) + R(c_2, c) - R(c_1, c) -R(c, c_1)) \cdot (n_{x, c} - n_{y, c})
\end{align*}
And we also look again at the relationship neighboring two
pixels $x$ and $y$ have. Let $s_{p, c}$ denote how much of crop
$c$ is planted in pixel $p$:
\begin{align*}
    Score
    &= \sum_{c_1 \in C} \sum_{c_2 \in C} (R(c_1, c_2) \cdot s_{x, c_1} \cdot s_{y, c_2} + R(c_1, c_2) \cdot s_{y, c_1} \cdot s_{x, c_2})\\
    &= \sum_{c_1 \in C} \sum_{c_2 \in C} R(c_1, c_2) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})\\
\end{align*}
and again we ask what condition is implied by shifting
a crop and creating an integer solution. Suppose
w.l.o.g. that we shift $c_\alpha$ by $\lambda$ from $x$ to
$y$ in exchange for $c_\beta$. We use the prime $'$
to denote new crop arrangements. The score delta is:
\begin{align*}
    \Delta Score
    &= \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}}
    (R(c_1, c_2) + R(c_2, c_1)) \cdot (s'_{x, c_1} \cdot s'_{y, c_2} + s'_{y, c_1} \cdot s'_{x, c_2})\\
    &+ \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in \{c_\alpha, c_\beta\}}
    R(c_1, c_2) \cdot (s'_{x, c_1} \cdot s'_{y, c_2} + s'_{y, c_1} \cdot s'_{x, c_2})\\
    &-\sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}}
    (R(c_1, c_2) + R(c_2, c_1)) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})\\
    &- \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in \{c_\alpha, c_\beta\}}
    R(c_1, c_2) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})\\
    &= \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}}
    (R(c_1, c_2) + R(c_2, c_1)) \cdot (s'_{x, c_1} \cdot s'_{y, c_2} + s'_{y, c_1} \cdot s'_{x, c_2})\\
    &-\sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}}
    (R(c_1, c_2) + R(c_2, c_1)) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})\\
    &+ \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in \{c_\alpha, c_\beta\}}
    R(c_1, c_2) \cdot (s'_{x, c_1} \cdot s'_{y, c_2} + s'_{y, c_1} \cdot s'_{x, c_2})\\
    &- \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in \{c_\alpha, c_\beta\}}
    R(c_1, c_2) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})
\end{align*}
And by the way we shift we know:
\begin{align*}
    s'_{x, c_\alpha} &= s_{x, c_\alpha} - \lambda & & s'_{y, c_\alpha} = s_{y, c_\alpha} + \lambda\\
    s'_{y, c_\beta} &= s_{y, c_\beta} - \lambda & & s'_{x, c_\beta} = s_{x, c_\beta} + \lambda
\end{align*}
Where the first two terms simplify to:
\begin{align*}
    & \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}}
    (R(c_1, c_2) + R(c_2, c_1)) \cdot (s'_{x, c_1} \cdot s'_{y, c_2} + s'_{y, c_1} \cdot s'_{x, c_2})\\
    &-\sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}}
    (R(c_1, c_2) + R(c_2, c_1)) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})\\
    &= \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\alpha, c_2) + R(c_2, c_\alpha)) \cdot (s_{x, c_\alpha}' \cdot s_{y, c_2}' + s_{y, c_\alpha}' \cdot s_{x, c_2}')\\
    &+ \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\beta, c_2) + R(c_2, c_\beta)) \cdot (s_{x, c_\beta}' \cdot s_{y, c_2}' + s_{y, c_\beta}' \cdot s_{x, c_2}')\\
    &- \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\alpha, c_2) + R(c_2, c_\alpha)) \cdot (s_{x, c_\alpha} \cdot s_{y, c_2} + s_{y, c_\alpha} \cdot s_{x, c_2})\\
    &- \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\beta, c_2) + R(c_2, c_\beta)) \cdot (s_{x, c_\beta} \cdot s_{y, c_2} + s_{y, c_\beta} \cdot s_{x, c_2})\\
    &= \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\alpha, c_2) + R(c_2, c_\alpha)) \cdot (s_{x, c_\alpha}' \cdot s_{y, c_2} + s_{y, c_\alpha}' \cdot s_{x, c_2})\\
    &+ \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\beta, c_2) + R(c_2, c_\beta)) \cdot (s_{x, c_\beta}' \cdot s_{y, c_2} + s_{y, c_\beta}' \cdot s_{x, c_2})\\
    &- \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\alpha, c_2) + R(c_2, c_\alpha)) \cdot (s_{x, c_\alpha} \cdot s_{y, c_2} + s_{y, c_\alpha} \cdot s_{x, c_2})\\
    &- \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\beta, c_2) + R(c_2, c_\beta)) \cdot (s_{x, c_\beta} \cdot s_{y, c_2} + s_{y, c_\beta} \cdot s_{x, c_2})\\
    &= \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\alpha, c_2) + R(c_2, c_\alpha)) \cdot ((s'_{x, c_\alpha} - s_{x, c_\alpha}) \cdot s_{y, c_2} + (s'_{y, c_\alpha} - s_{y, c_\alpha}) \cdot s_{x, c_2})\\
    &+ \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\beta, c_2) + R(c_2, c_\beta)) \cdot ((s'_{x, c_\beta} - s_{x, c_\beta}) \cdot s_{y, c_2} + (s'_{y, c_\beta} - s_{y, c_\beta}) \cdot s_{x, c_2})\\
    &= \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\alpha, c_2) + R(c_2, c_\alpha)) \cdot (- \lambda \cdot s_{y, c_2} + \lambda \cdot s_{x, c_2})\\
    &+ \sum_{c_2 \in C \setminus \{c_\alpha, c_\beta\}} (R(c_\beta, c_2) + R(c_2, c_\beta)) \cdot (\lambda \cdot s_{y, c_2} - \lambda \cdot s_{x, c_2})
\end{align*}
Which is a linear function in $\lambda$.\\ \\
For the other two terms we can reason:
\begin{align*}
    & \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in \{c_\alpha, c_\beta\}}
    R(c_1, c_2) \cdot (s'_{x, c_1} \cdot s'_{y, c_2} + s'_{y, c_1} \cdot s'_{x, c_2})\\
    &- \sum_{c_1 \in \{c_\alpha, c_\beta\}} \; \sum_{c_2 \in \{c_\alpha, c_\beta\}}
    R(c_1, c_2) \cdot (s_{x, c_1} \cdot s_{y, c_2} + s_{y, c_1} \cdot s_{x, c_2})\\
    &= R(c_\alpha, c_\alpha) \cdot (s_{x, c_\alpha}' \cdot s_{y, c_\alpha}' + s_{y, c_\alpha}' \cdot s_{x, c_\alpha}')\\
    &+ R(c_\alpha, c_\beta) \cdot (s_{x, c_\alpha}' \cdot s_{y, c_\beta}' + s_{y, c_\alpha}' \cdot s_{x, c_\beta}')\\
    &+ R(c_\beta, c_\alpha) \cdot (s_{x, c_\beta}' \cdot s_{y, c_\alpha}' + s_{y, c_\beta}' \cdot s_{x, c_\alpha}')\\
    &+ R(c_\beta, c_\beta) \cdot (s_{x, c_\beta}' \cdot s_{y, c_\beta}' + s_{y, c_\beta}' \cdot s_{x, c_\beta}')\\
    &- R(c_\alpha, c_\alpha) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\alpha} + s_{y, c_\alpha} \cdot s_{x, c_\alpha})\\
    &- R(c_\alpha, c_\beta) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\beta} + s_{y, c_\alpha} \cdot s_{x, c_\beta})\\
    &- R(c_\beta, c_\alpha) \cdot (s_{x, c_\beta} \cdot s_{y, c_\alpha} + s_{y, c_\beta} \cdot s_{x, c_\alpha})\\
    &- R(c_\beta, c_\beta) \cdot (s_{x, c_\beta} \cdot s_{y, c_\beta} + s_{y, c_\beta} \cdot s_{x, c_\beta})\\
    &= R(c_\alpha, c_\alpha) \cdot ((s_{x, c_\alpha} - \lambda) \cdot (s_{y, c_\alpha} + \lambda) + (s_{y, c_\alpha} + \lambda) \cdot (s_{x, c_\alpha} - \lambda))\\
    &+ R(c_\alpha, c_\beta) \cdot ((s_{x, c_\alpha} - \lambda) \cdot (s_{y, c_\beta} - \lambda) + (s_{y, c_\alpha} + \lambda) \cdot (s_{x, c_\beta} + \lambda))\\
    &+ R(c_\beta, c_\alpha) \cdot ((s_{x, c_\beta} + \lambda) \cdot (s_{y, c_\alpha} + \lambda) + (s_{y, c_\beta} - \lambda) \cdot (s_{x, c_\alpha} - \lambda))\\
    &+ R(c_\beta, c_\beta) \cdot ((s_{x, c_\beta} + \lambda) \cdot (s_{y, c_\beta} - \lambda) + (s_{y, c_\beta} - \lambda) \cdot (s_{x, c_\beta} + \lambda))\\
    &- R(c_\alpha, c_\alpha) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\alpha} + s_{y, c_\alpha} \cdot s_{x, c_\alpha})\\
    &- R(c_\alpha, c_\beta) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\beta} + s_{y, c_\alpha} \cdot s_{x, c_\beta})\\
    &- R(c_\beta, c_\alpha) \cdot (s_{x, c_\beta} \cdot s_{y, c_\alpha} + s_{y, c_\beta} \cdot s_{x, c_\alpha})\\
    &- R(c_\beta, c_\beta) \cdot (s_{x, c_\beta} \cdot s_{y, c_\beta} + s_{y, c_\beta} \cdot s_{x, c_\beta})\\
    &= R(c_\alpha, c_\alpha) \cdot ( s_{x, c_\alpha} \cdot s_{y, c_\alpha} + s_{x, c_\alpha} \cdot \lambda - \lambda \cdot s_{y, c_\alpha} - \lambda \cdot \lambda + s_{y, c_\alpha} \cdot s_{x, c_\alpha} - s_{y, c_\alpha} \cdot \lambda + \lambda \cdot s_{x, c_\alpha} - \lambda \cdot \lambda)\\
    &+ R(c_\alpha, c_\beta) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\beta} - s_{x, c_\alpha} \cdot \lambda- \lambda \cdot s_{y, c_\beta} + \lambda \cdot \lambda+ s_{y, c_\alpha} \cdot s_{x, c_\beta} + s_{y, c_\alpha} \cdot \lambda+ \lambda \cdot s_{x, c_\beta} + \lambda \cdot \lambda)\\
    &+ R(c_\beta, c_\alpha) \cdot (s_{x, c_\beta} \cdot s_{y, c_\alpha} + s_{x, c_\beta} \cdot \lambda + \lambda \cdot s_{y, c_\alpha} + \lambda \cdot \lambda + s_{y, c_\beta} \cdot s_{x, c_\alpha} - s_{y, c_\beta} \cdot \lambda - \lambda \cdot s_{x, c_\alpha} + \lambda \cdot \lambda)\\
    &+ R(c_\beta, c_\beta) \cdot (s_{x, c_\beta} \cdot s_{y, c_\beta} - s_{x, c_\beta} \cdot \lambda + \lambda \cdot s_{y, c_\beta} - \lambda \cdot \lambda + s_{y, c_\beta} \cdot s_{x, c_\beta} + s_{y, c_\beta} \cdot \lambda - \lambda \cdot s_{x, c_\beta} - \lambda \cdot \lambda)\\
    &- R(c_\alpha, c_\alpha) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\alpha} + s_{y, c_\alpha} \cdot s_{x, c_\alpha})\\
    &- R(c_\alpha, c_\beta) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\beta} + s_{y, c_\alpha} \cdot s_{x, c_\beta})\\
    &- R(c_\beta, c_\alpha) \cdot (s_{x, c_\beta} \cdot s_{y, c_\alpha} + s_{y, c_\beta} \cdot s_{x, c_\alpha})\\
    &- R(c_\beta, c_\beta) \cdot (s_{x, c_\beta} \cdot s_{y, c_\beta} + s_{y, c_\beta} \cdot s_{x, c_\beta})\\
    &= R(c_\alpha, c_\alpha) \cdot ( 2 \cdot s_{x, c_\alpha} \cdot s_{y, c_\alpha} + 2 \cdot s_{x, c_\alpha} \cdot \lambda - 2 \cdot \lambda \cdot s_{y, c_\alpha} - 2 \cdot \lambda \cdot \lambda)\\
    &+ R(c_\alpha, c_\beta) \cdot (2 \cdot s_{x, c_\alpha} \cdot s_{y, c_\beta} - 2 \cdot s_{x, c_\alpha} \cdot \lambda - 2 \cdot \lambda \cdot s_{y, c_\beta} + 2 \cdot s_{y, c_\alpha} \cdot s_{x, c_\beta} + 2 \cdot s_{y, c_\alpha} \cdot \lambda + 2 \cdot \lambda \cdot s_{x, c_\beta} + 4 \cdot \lambda \cdot \lambda)\\
    &+ R(c_\beta, c_\beta) \cdot (2 \cdot s_{x, c_\beta} \cdot s_{y, c_\beta} - 2 \cdot s_{x, c_\beta} \cdot \lambda + 2 \cdot \lambda \cdot s_{y, c_\beta} - 2 \cdot \lambda \cdot \lambda)\\
    &- R(c_\alpha, c_\alpha) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\alpha} + s_{y, c_\alpha} \cdot s_{x, c_\alpha})\\
    &- R(c_\alpha, c_\beta) \cdot (s_{x, c_\alpha} \cdot s_{y, c_\beta} + s_{y, c_\alpha} \cdot s_{x, c_\beta})\\
    &- R(c_\beta, c_\alpha) \cdot (s_{x, c_\beta} \cdot s_{y, c_\alpha} + s_{y, c_\beta} \cdot s_{x, c_\alpha})\\
    &- R(c_\beta, c_\beta) \cdot (s_{x, c_\beta} \cdot s_{y, c_\beta} + s_{y, c_\beta} \cdot s_{x, c_\beta})\\
    &= R(c_\alpha, c_\alpha) \cdot (  2 \cdot s_{x, c_\alpha} \cdot \lambda - 2 \cdot \lambda \cdot s_{y, c_\alpha} - 2 \cdot \lambda \cdot \lambda)\\
    &+ R(c_\alpha, c_\beta) \cdot ( - 2 \cdot s_{x, c_\alpha} \cdot \lambda - 2 \cdot \lambda \cdot s_{y, c_\beta} + 2 \cdot s_{y, c_\alpha} \cdot \lambda + 2 \cdot \lambda \cdot s_{x, c_\beta} + 4 \cdot \lambda \cdot \lambda)\\
    &+ R(c_\beta, c_\beta) \cdot ( - 2 \cdot s_{x, c_\beta} \cdot \lambda + 2 \cdot \lambda \cdot s_{y, c_\beta} - 2 \cdot \lambda \cdot \lambda)\\
    &= R(c_\alpha, c_\beta) \cdot ( - 2 \cdot s_{x, c_\alpha} \cdot \lambda - 2 \cdot \lambda \cdot s_{y, c_\beta} + 2 \cdot s_{y, c_\alpha} \cdot \lambda + 2 \cdot \lambda \cdot s_{x, c_\beta} + 4 \cdot \lambda \cdot \lambda)\\
    &- R(c_\alpha, c_\alpha) \cdot (  - 2 \cdot s_{x, c_\alpha} \cdot \lambda + 2 \cdot \lambda \cdot s_{y, c_\alpha} + 2 \cdot \lambda \cdot \lambda)\\
    &- R(c_\beta, c_\beta) \cdot (- 2 \cdot \lambda \cdot s_{y, c_\beta} + 2 \cdot s_{x, c_\beta} \cdot \lambda + 2 \cdot \lambda \cdot \lambda)\\
\end{align*}
Which is the same condition as we had for the $\abs*{C} = 2$ case.
\subsection{The last $\binom{\abs*{C}}{2}$ pixels}
It turns out that we might end up with
up to $\binom{\abs*{C}}{2}$ pixels with fractional amounts
of crop planted in them that we can't make integer.
Consider the following example:\\
\begin{center}
    \begin{tikzpicture}[->,>=stealth',auto,node distance=3cm,
            thick,main node/.style={rectangle,draw,font=\sffamily\Large\bfseries}]
        \node[main node] (1) {$\textbf{X}: a, b$};
        \node[main node] (2) [right of=1] {$\textbf{Y}: a, c$};
        \node[main node] (3) [right of=2] {$\textbf{Z}: b, c$};
    \end{tikzpicture}
\end{center}
where three non-neighboring pixels each contain two different crops. Further
assume the following about their neighborhoods:
\begin{itemize}
    \item \textbf{X} likes $a$ and $b$, dislikes $c$
    \item \textbf{Y} likes $a$ and $c$, dislikes $b$
    \item \textbf{Z} likes $b$ and $c$, dislikes $a$
\end{itemize}
or in other words: each pixel only likes the crops currently planted
in itself. Then there exists no way to beneficially swap crops. However
a good solution still exists: $X: \alpha, Y: \gamma, Z: \beta$. Hence
the problem is that the two swap is not sufficiently powerful.\\ \\
Is there a remedy for this situation? One approach would
be to introduce more powerful shifts, we could then swap
like so:
\begin{center}
    \begin{tikzpicture}[->,>=stealth',auto,node distance=3cm,
            thick,main node/.style={rectangle,draw,font=\sffamily\Large\bfseries}]

        \node[main node] at (-3, 0) (1) {$a, b$};
        \node[main node] at (0, 3) (2) {$b, c$};
        \node[main node] at (3, 0) (3) {$a, c$};

        \path[every node/.style={font=\sffamily\small}]
        (1) edge[left] node [above] {$b$} (2)
        (2) edge[left] node [above] {$c$} (3)
        (3) edge[right] node [below] {$a$} (1);
    \end{tikzpicture}
\end{center}
However, we run into issues because we now no longer
have as specific information about our path. Further
there are paths were our usual condition does not guarantee
that the quadratic function remains positive:
\begin{center}
    \begin{tikzpicture}[->,>=stealth',auto,node distance=3cm,
            thick,main node/.style={rectangle,draw,font=\sffamily\Large\bfseries}]

        \node[main node] at (-3, 0) (1) {$a, b$};
        \node[main node] at (0, 0.35) (2) {$b, c$};
        \node[main node] at (3, 0) (3) {$c, d$};
        \node[main node] at (0, -0.35) (4) {$d, a$};

        \path[every node/.style={font=\sffamily\small}]
        (1) edge[left] node [above] {$b$} (2)
        (2) edge[left] node [above] {$c$} (3)
        (3) edge[right] node [below] {$d$} (4)
        (4) edge[right] node [below] {$a$} (1);
    \end{tikzpicture}
\end{center}
Here the quadratic part of the delta score function is (when $R$ symmetric):
\begin{align*}
    \Delta Score_{quad}
    &= \lambda^2 ( R(b, d) - R(b, a) - R(c, d) + R(c, a) )\\
    &+ \lambda^2 ( R(d, b) - R(d, c) - R(a, b) + R(a, c) ) \\
    &= 2 \lambda^2 \cdot (R(a, c) + R(b, d) - R(a, b) - R(c, d))
\end{align*}
\section{No restrictions on $R$}
If our R matrix does not fulfill the conditions
we can instead solve within the four subfields
shown below.
\begin{center}
    \begin{tikzpicture}[x=1cm]
        \foreach \x in {0,...,7} \foreach \y in {0,...,7}
        {
            \pgfmathparse{mod(\x+\y,2) ? (mod(\x,2) ? "red" : "green") : (mod(\x,2) ? "blue" : "gray")}
            \edef\colour{\pgfmathresult}
            \path[fill=\colour] (\x,\y) rectangle ++ (1,1);
        }
        \draw (0,0)--(0,8)--(8,8)--(8,0)--cycle;
    \end{tikzpicture}
\end{center}
Then we can be left with up to $4 \cdot \binom{\abs{C}}{2}$
fractional pixels.

\end{document}