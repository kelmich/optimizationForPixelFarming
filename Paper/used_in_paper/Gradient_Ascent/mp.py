import multiprocessing
import time
from gradient_ascent import gradient_ascent




for p_id in range(multiprocessing.cpu_count()):
    p = multiprocessing.Process(target=gradient_ascent)
    p.start()