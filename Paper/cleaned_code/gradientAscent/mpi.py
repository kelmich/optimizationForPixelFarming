from mpi4py import MPI
from gradient_ascent import gradient_ascent
import random

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
procname = MPI.Get_processor_name()

print("Hello world from processor {}, rank {:d} out of {:d} processors".format(procname, rank, size))
random.seed(rank)
gradient_ascent()
