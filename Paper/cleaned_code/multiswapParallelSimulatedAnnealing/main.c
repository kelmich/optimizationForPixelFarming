#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <glpk.h>            

// Global variables
int X = 0;
int Y = 0;
int C = 0;
int* dist;
double* R;

// specify parameters
int max_subset_size_climb = 225;
int max_subset_size_anneal = 2;
int max_same_iters = 10;
int outer_rounds = 450;
int inner_rounds = 105;
double P = 0.8;
double F = 0.85;



// Helper Variables
int* pixels;

// Some Helper Macros
#define R_ind(cA, cB) (((cA) + (cB) * C))
#define F_ind(x, y) (((x) + X * (y)))
#define F_ind_frac(p_id, c) (((p_id) * C + (c)))

// Some helper functions
void get_neighbors(int* res, int p) {
    int pX = p % X;
    int pY = p / X;
    int neighbors = 0;
    for(int i = -1; i < 2; i++) {
        for(int j = -1; j < 2; j++) {
            if(0 <= pX + i && pX + i < X) {
                if(0 <= pY + j && pY + j < Y) {
                    if(i == 0 && j == 0) {
                        continue;
                    }
                    neighbors++;
                    res[neighbors] = F_ind(pX + i, pY + j);
                }
            }
        }
    }
    res[0] = neighbors;
}
double energy(double* field) {
    double score = 0;
    int* neighbors = (int*) malloc(sizeof(int) * 9);
    for(int i = 0; i < X; i++) {
        for(int j = 0; j < Y; j++) {
            get_neighbors(neighbors, F_ind(i, j));
            for(int n = 1; n <= neighbors[0]; n++) {
                int p = F_ind(i, j);
                for(int cA = 0; cA < C; cA++) {
                    for(int cB = 0; cB < C; cB++) {
                        score += R[R_ind(cA, cB)] * field[F_ind_frac(p, cA)] * field[F_ind_frac(neighbors[n], cB)];
                    }
                }
                
            }
        }
    }
    free(neighbors);
    return score;
}

// A function to generate a random permutation of arr[] (https://www.geeksforgeeks.org/shuffle-a-given-array-using-fisher-yates-shuffle-algorithm/)
void randomize(int* arr, int n) {
    for (int i = n-1; i > 0; i--)
    {
        int j = rand() % (i+1);
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}

void get_non_neighboring_pixels(int* res, int max_size) {

    int* neighbors = malloc(sizeof(int) * 9);

    randomize(pixels, X * Y);
    int size = 1;
    res[size] = pixels[0];
    for(int i = 0; size < max_size && i < X * Y; i++) {
        get_neighbors(neighbors, pixels[i]);
        int is_non_neighboring = 1;
        for(int j = 1; j <= neighbors[0]; j++) {
            for(int k = 1; k <= size; k++) {
                // not a neighbor or a pixel already in the set
                if(neighbors[j] == res[k] || pixels[i] == res[k]) {
                    is_non_neighboring = 0;
                }
            }
        }
        if(is_non_neighboring == 1) {
            size++;
            res[size] = pixels[i];
        }
    }
    res[0] = size;
    free(neighbors);
}

void initialize_field(double* frac_field) {
    // initialize field
    int* field = (int*) malloc(sizeof(int) * X * Y);
    int pos = 0;
    for(int i = 0; i < C; i++) {
        for(int j = 0; j < dist[i]; j++) {
            field[pos] = i;
            pos++;
        }
    }
    randomize(field, X * Y);
    for(int i = 0; i < X; i++) {
        for(int j = 0; j < Y; j++) {
            for(int c = 0; c < C; c++) {
                int p = F_ind(i, j);
                if(field[p] == c) {
                    frac_field[F_ind_frac(p, c)] = 1.0;
                } else {
                    frac_field[F_ind_frac(p, c)] = 0.0;
                }
                
            }
        }
    }
    free(field);
}

void anneal(double* frac_field) {

    randomize(pixels, X * Y);

    // save the state of the pixels
    double* state = (double*) malloc(sizeof(double) * C * max_subset_size_anneal);
    for(int p = 0; p < max_subset_size_anneal; p++) {
        for(int c = 0; c < C; c++) {
            state[C * p + c] = frac_field[F_ind_frac(pixels[p], c)];
        }
    }

    // shuffle the first "max_subset_size_anneal" pixels
    randomize(pixels, max_subset_size_anneal);

    // write result back
    for(int p = 0; p < max_subset_size_anneal; p++) {
        for(int c = 0; c < C; c++) {
            frac_field[F_ind_frac(pixels[p], c)] = state[C * p + c];
        }
    }

    // cleanup
    free(state);
}

// The actual algorithm
void lp_iter(double* frac_field) {

    double p = P;
    double f = F;

    // Iteratively solve the LP
    for(int iterOur = 0; iterOur < outer_rounds; iterOur++) {
        for(int iterIn = 0; iterIn < inner_rounds; iterIn++) {


        // find the set of pixels we are swapping amongst
        int pixel_subset_size = max_subset_size_climb;
        int* p_set = (int*) malloc(sizeof(int) * (pixel_subset_size + 1));
        get_non_neighboring_pixels(p_set, pixel_subset_size);

        // setup the LP
        glp_prob* lp = glp_create_prob();
        glp_set_obj_dir(lp, GLP_MAX);
        glp_term_out(GLP_OFF);
        int vars = p_set[0] * C;
        int* ia = malloc(sizeof(int) * ((C + p_set[0]) * vars + 1));
        int* ja = malloc(sizeof(int) * ((C + p_set[0]) * vars + 1));
        double* ar = malloc(sizeof(double) * ((C + p_set[0]) * vars + 1));
        glp_add_rows(lp, C + p_set[0]);
        glp_add_cols(lp, vars);
        int constraint_counter = 1;

        // constrain that every pixel contains a total of 1 crops
        for(int p = 0; p < p_set[0]; p++) {
            glp_set_row_bnds(lp, p+1, GLP_FX, 1.0, 1.0);
            for(int i = 0; i < vars; i++) {
                ia[constraint_counter] = p + 1;
                ja[constraint_counter] = i + 1;
                if(i / C == p) {
                    ar[constraint_counter] = 1.0;
                } else {
                    ar[constraint_counter] = 0.0;
                }
                constraint_counter++;
            }
        }

        // constrain that the total crop amount remains the same
        for(int c = 0; c < C; c++) {
            double crop_amount_before = 0.0;
            for(int p = 1; p <= p_set[0]; p++) {
                crop_amount_before += frac_field[F_ind_frac(p_set[p], c)];
            }
            glp_set_row_bnds(lp, p_set[0] + c + 1, GLP_FX, crop_amount_before, crop_amount_before);
            for(int i = 0; i < vars; i++) {
                ia[constraint_counter] = p_set[0] + c + 1;
                ja[constraint_counter] = i + 1;
                if(i % C == c) {
                    ar[constraint_counter] = 1.0;
                } else {
                    ar[constraint_counter] = 0.0;
                }
                constraint_counter++;
            }
        }

        // contrain the vars to be 0 <= x <= 1 and set obj
        int* neighbors = malloc(sizeof(int) * 9);
        for(int i = 0; i < vars; i++) {
            glp_set_col_bnds(lp, i+1, GLP_DB, 0.0, 1.0);
            double score_delta = 0.0;
            get_neighbors(neighbors, p_set[(i / C) + 1]);
            for(int j = 1; j <= neighbors[0]; j++) {
                int n = neighbors[j];
                for(int c = 0; c < C; c++) {
                    score_delta += frac_field[F_ind_frac(n, c)] * ((R[R_ind(i % C, c)] + R[R_ind(c, i % C)]) / 2.0);
                }
            }
            glp_set_obj_coef(lp, i+1, score_delta);
        }
        free(neighbors);

        // solve the problem
        glp_load_matrix(lp, constraint_counter - 1, ia, ja, ar);
        glp_simplex(lp, NULL);

        // update the field
        int pos = 0;
        for(int pixel = 1; pixel <= p_set[0]; pixel++) {
            for(int c = 0; c < C; c++) {
                pos++;
                frac_field[F_ind_frac(p_set[pixel], c)] = glp_get_col_prim(lp, pos);
            }
        }

        // possibly anneal
        double r = ((double) rand()) / ((double) RAND_MAX);
        if(r < p) {
            anneal(frac_field);
        }

        // cleanup
        glp_delete_prob(lp);
        glp_free_env();
        free(p_set);
        free(ia);
        free(ja);
        free(ar);
        }
        // debug
        p *= f;
    }
}

// The driver for running it in parallel
int main(int argc, char** argv) {

        // MPI Initialization
        int world_size;
        int world_rank;
        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
        srand(world_rank);

        // load the pixel farming problem
        FILE* f = fopen("../problemInstances/1.txt", "r");
        int status = fscanf(f, "%i %i %i", &X, &Y, &C);
        dist = (int*) malloc(C * sizeof(int));
        R = (double*) malloc(X * Y * sizeof(double));
        for(int i = 0; i < C; i++) {
            status = fscanf(f, "%i", &dist[i]);
        }
        for(int i = 0; i < X * Y; i++) {
            status = fscanf(f, "%lf", &R[i]);
        }
        fclose(f);

        // variable initialization
        pixels = malloc(sizeof(int) * X * Y);
        for(int i = 0; i < X * Y; i++) {
            pixels[i] = i;
        }
        
        while(1) {

            // initialize a fractional field
            double* frac_field = (double*) malloc(sizeof(double) * X * Y * C);
            initialize_field(frac_field);

            // iterate on the best solution a node has until it converges
            int same_iters = 0;
            double* frac_field_prev = (double*) malloc(sizeof(double) * X * Y * C);
            double prev_score = - X * Y * C * 100;
            while(same_iters < max_same_iters) {

                // housekeeping
                same_iters++;
                memcpy(frac_field_prev, frac_field, sizeof(double) * X * Y * C);

                // iterate
                lp_iter(frac_field);

                // inform the other nodes of the best current solution
                double s = energy(frac_field);
                double s_prev = energy(frac_field_prev);
                double* best_sol;
                if(s > s_prev) {
                    best_sol = frac_field;
                } else {
                    best_sol = frac_field_prev;
                }
                MPI_Barrier(MPI_COMM_WORLD);
                
                // find the process with the max score
                double* rbuf;
                rbuf = (double *) malloc(world_size * X * Y * C * sizeof(double)); 
                MPI_Allgather( best_sol, X * Y * C, MPI_DOUBLE, rbuf, X * Y * C, MPI_DOUBLE, MPI_COMM_WORLD);
                MPI_Barrier(MPI_COMM_WORLD);
                double* best_round_sol = rbuf;
                double x = energy(best_round_sol);
                for(int i = 0; i < world_size; i++) {
                    double y = energy(rbuf + i * X * Y * C);
                    if(y > x) {
                        x = y;
                        best_round_sol = rbuf + i * X * Y * C;
                    }
                }
                memcpy(frac_field, best_round_sol, X * Y * C * sizeof(double));
                if(energy(frac_field) > prev_score) {
                    same_iters = 0;
                    prev_score = energy(frac_field);
                }
                
                free(rbuf);
            }


            // print the converged solution
            double s = energy(frac_field);
            if(world_rank == 0) {
                printf("trial-result: %f\n", s);
            }

            // cleanup
            free(frac_field);
            free(frac_field_prev);
        }

        // cleanup
        free(pixels);
        free(dist);
        free(R);

        // MPI Termination
        MPI_Finalize();
}