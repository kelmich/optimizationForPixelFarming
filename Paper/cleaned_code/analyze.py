import numpy as np
import sys


f = open(sys.argv[1])
trials = []

while True:
    line = f.readline()

    if not line:
        break

    if "trial-result:" in line:
        try:
            v = float(line.split(":")[1])
            trials.append(v)
        except:
            print("Parsing error")
            exit(1)

# crunch the numbers
print("Avg Score:", np.average(trials))
print("Std Devitaion: ", np.std(trials))
print("Number of Trials:", len(trials))

