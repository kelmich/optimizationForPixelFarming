#!/bin/bash

echo -e '\n submitted Open MPI job'
echo 'hostname'
hostname

# compile the C file
mpicc main.c -ldl -lm -O3 -o executable

# run compiled main.cpp file
mpirun ./executable