#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <signal.h>

// Global variables
int X = 0;
int Y = 0;
int C = 0;
int* dist;
double* R;

// specify the simulated annealing parameters
double T0 = 10.0;
double K = 0.995;
int NT = 3000;
int NG = 1400;

// Some Helper Macros
#define R_ind(cA, cB) ((cA) + (cB) * C)
#define F_ind(x, y) ((x) + X * (y))

// Some helper functions
void getNeighbors(int* res, int* field, int p) {
    int pX = p % X;
    int pY = p / X;
    int neighbors = 0;
    for(int i = -1; i < 2; i++) {
        for(int j = -1; j < 2; j++) {
            if(0 <= pX + i && pX + i < X) {
                if(0 <= pY + j && pY + j < Y) {
                    if(i == 0 && j == 0) {
                        continue;
                    }
                    neighbors++;
                    res[neighbors] = F_ind(pX + i, pY + j);
                }
            }
        }
    }
    res[0] = neighbors;
}
double energy(int* field) {
    double score = 0;
    int* neighbors = (int*) malloc(sizeof(int) * 10);
    for(int i = 0; i < X; i++) {
        for(int j = 0; j < Y; j++) {
            getNeighbors(neighbors, field, F_ind(i, j));
            for(int n = 1; n <= neighbors[0]; n++) {
                int cA = field[F_ind(i, j)];
                int cB = field[neighbors[n]];
                score -= R[R_ind(cA, cB)];
            }
        }
    }
    free(neighbors);
    return score;
}
double deltaEnergy(int* field, int u, int v) {

    double score = 0;
    int cU = field[u];
    int cV = field[v];

    int* neighbors = (int*) malloc(sizeof(int) * 10);
    getNeighbors(neighbors, field, u);
    for(int n = 1; n <= neighbors[0]; n++) {
        int cN = field[neighbors[n]];
        score += R[R_ind(cU, cN)];
        score += R[R_ind(cN, cU)];
        score -= R[R_ind(cV, cN)];
        score -= R[R_ind(cN, cV)];
    }
    getNeighbors(neighbors, field, v);
    for(int n = 1; n <= neighbors[0]; n++) {
        int cN = field[neighbors[n]];
        score += R[R_ind(cV, cN)];
        score += R[R_ind(cN, cV)];
        score -= R[R_ind(cU, cN)];
        score -= R[R_ind(cN, cU)];
    }
    free(neighbors);

    return score;
}

// A function to generate a random permutation of arr[] (https://www.geeksforgeeks.org/shuffle-a-given-array-using-fisher-yates-shuffle-algorithm/)
void randomize (int* arr) {
    int n = X * Y;
    for (int i = n-1; i > 0; i--)
    {
        int j = rand() % (i+1);
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}

// The actual simulated annealing
double simulatedAnnealing() {
    
    // initialize field
    int* field = malloc(sizeof(int) * X * Y);
    int pos = 0;
    for(int i = 0; i < C; i++) {
        for(int j = 0; j < dist[i]; j++) {
            field[pos] = i;
            pos++;
        }
    }
    randomize(field);

    // Setup Process
    double H = energy(field);
    double T = T0;
    for(int iT = 0; iT < NT; iT++) {
        for(int iG = 0; iG < NG; iG++) {

            // pick two random points
            int pA = rand() % (X * Y);
            int pB = rand() % (X * Y);
            while(field[pA] == field[pB]) {
                pB = rand() % (X * Y);
            }

            // Improve or Anneal
            double deltaH = deltaEnergy(field, pA, pB);
            double r = ((double) rand()) / ((double) RAND_MAX);
            double p = exp(-deltaH / T);
            if(deltaH <= 0 || r < p) {
                H += deltaH;
                int tmp = field[pA];
                field[pA] = field[pB];
                field[pB] = tmp;
            }
        }
        // Cool system down
        T *= K;
    }

    double score = energy(field);

    free(field);

    return score;
}

// The driver for running it in parallel
int main(int argc, char** argv) {

        // MPI Initialization
        int world_size;
        int world_rank;
        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
        srand(time(NULL) + world_rank);

        // load the pixel farming problem
        FILE* f = fopen("../problemInstances/1.txt", "r");
        int status = fscanf(f, "%i %i %i", &X, &Y, &C);
        dist = (int*) malloc(C * sizeof(int));
        R = (double*) malloc(X * Y * sizeof(double));
        for(int i = 0; i < C; i++) {
            status = fscanf(f, "%i", &dist[i]);
        }
        for(int i = 0; i < X * Y; i++) {
            status = fscanf(f, "%lf", &R[i]);
        }
        fclose(f);
        
        while(1) {
            double s = simulatedAnnealing();
            printf("trial-result: %f\n", s);
        }

        // cleanup
        free(dist);
        free(R);

        // MPI Termination
        MPI_Finalize();
}